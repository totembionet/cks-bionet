Readme.txt
----------

This example is taken from :
  H. Sun, M. Folschette, M. Magnin, Condition for periodic attractor
  in 4-dimensional repressilators, in: J. Pang, J. Niehren (Eds.), 
  Computational Methods in Systems Biology, Springer Nature Switzerland,
  Cham, 2023, pp. 184–201.

The reprissilator of size 4 is given in Figure 3-Left and the cycle we consider is given in figure 4.
reprisilator-IG-4.jpg is the influence graph for reprissilator of size 4 given in the article.

This reprissilator of size 4 is generalized in size n as shown in reprisilator-IG-n.jpg.

generate.py is a python3 script to generate the .smb input file for any reprissilator with n variables. For example:

      >> pythons3  generate.py 10

will generate the file reprissilator10.smb which contains the influence graph of a reprissilator with 10 variables.

The CKS approach is faster than the enumerative approach for n=3 to n=6. For n=7, the enumerative approach is faster. And beyong n=8 both approaches are TIMEOUT (>1h)
