# script to create a .smb file with a reprissilator of size n


# the number of variables should be given as main argument

import sys

# to write the VAR block
def writeVars(file,n,snoussi):
    file.write("VAR\n")
    snou=""
    if snoussi=="n":
        snou=" (NS)"
    for i in range(n-2):
        file.write("v"+str(i)+"=0..1"+snou+";\n")
    file.write("v"+str(n-2)+"=0..2"+snou+";\n")
    file.write("v"+str(n-1)+"=0..1"+snou+";\n")

# to build a string for the stable state 10000...0 
def stable(n):
    res = "v0=1&"
    for i in range(1,n-1):
        res+="v"+str(i)+"=0 & "
    res +="v"+str(n-1)+"=0"
    return res
    
def writeFormula(file,n):
    sta = stable(n)
    file.write("\nCTL\n")
    file.write("("+sta+")->EX(EF("+sta+"));\n")

def writeReg(file,n):
    file.write("\nREG\n")
    file.write("# each variable regulates the next one\n")
    file.write("# all regulations are inhibitions\n")
    # loop of inhibitions with thresholds 1
    i=1
    while i <= n-2:
        vi = "v"+ str(i)
        vi1 = "v"+str(i-1)
        file.write(vi1+"to"+vi+" [!("+vi1+">=1)] => "+vi + ";\n")
        i+=1
    # end of path with thresholds 1
    vi = "v"+ str(i-1)
    file.write(vi+"tov0 [!("+vi+">=1)] => v0;\n")
    # close the loop
    v1 = "v"+str(n-1)
    v2 = "v"+str(n-2)
    v3 = "v"+str(n-3)
    file.write(v2+"to" + v1 + "[!("+v2+">=2)] => " + v1 + ";\n")
    file.write(v1+"to" + v3 + "[!("+v1+">=1)] => " + v3 + ";\n")

def main(n):
    fileName = "reprissilator" + str(n) + ".smb"
    file = open(fileName, 'w')
    writeVars(file,n,"n")
    writeReg(file,n)
    writeFormula(file,n)
    file.write("\nEND")
    print("file has been generated in",fileName)
    file.close()


main(int(sys.argv[1]))
#main(8)
