Readme.txt
----------


negativeInfluenceGraph.jpg is a diagram of the influence graph.

There are (2²)^2 parameter settings (e.g. 1,048,576 for n=10).

generateNegative.py is a python3 script to generate the .smb input file for n variables. For example:

      >> pythons3  generateNegative.py 5

will generate the file negativeBool5.smb which contains the influence graph of a negative cycle with 5 variables.


