Readme.txt
----------

negativeBool5.smb is the influence graph of a negative loop with 5 variables

negativeBool5.csv has been obtained using the original approach implemented in TotemBioNet 

negativeBool5.out is a trace for the original approach implemented in TotemBioNet.

negativeBool5-cks.csv has been obtained using the new approach proposed in the paper.


