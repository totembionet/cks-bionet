# script to create a .smb file with a negative cycle

# the number of variables should be given as main argument

import sys

# to write the VAR block
def writeVars(file,n,snoussi):
    file.write("VAR\n")
    snou=""
    if snoussi=="n":
        snou=" (NS)"
    for i in range(n):
        file.write("v_"+str(i)+"=0..1"+snou+";\n")

# to build a string with stable state where all variables are equal to val 
def stable(n,val):
    res = ""
    for i in range(n-1):
        res+="v_"+str(i)+"="+val+"&"
    res +="v_"+str(n-1)+"="+val
    return res
    
def writeFormula(file,n):
    stable0="("+stable(n,"0")+")"
    file.write("\nCTL\n")
    file.write("# oscillation around state where v_i=0 for all i\n")
    file.write(stable0+"->AX(AF"+stable0+");\n")

def writeReg(file,n):
    file.write("\nREG\n")
    file.write("# each variable regulates the next one\n")
    file.write("# (n-1) regulations are activations\n")    
    for i in range(1,n):
        file.write("R_"+str(i)+"[(v_"+str(i-1)+">=1)] => v_"+ str(i) + ";\n")
    file.write("# the last regulation is an inhibition\n")
    file.write("R_0[!(v_"+str(n-1)+">=1)] => v_0;\n")

def main(n):
    fileName = "negativeBool" + str(n)  + ".smb"
    file = open(fileName, 'w')
    writeVars(file,n,"n")
    writeReg(file,n)
    writeFormula(file,n)
    print("file has been generated in",fileName)
    file.write("\nEND")
    file.close()



main(int(sys.argv[1]))
