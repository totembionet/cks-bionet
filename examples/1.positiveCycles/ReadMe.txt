Readme.txt
----------

positiveInfluenceGraph.jpg is a diagram of the influence graph.

There are (2²)^n parameter settings (e.g. 1,048,576 for n=10).

generatePositive.py is a python3 script to generate the .smb input file for n variables. For example:

      >> pythons3  generatePositive.py 10

will generate the file positiveBool10.smb which contains the influence graph of a positive cycle with 10 variables.

Generate the totem input file for the number of variables of your choice:
      if n=5, the enumeration takes a few seconds,
      if n=7, the enumeration takes a few minutes, and
      if n>=10, the enumeration could be too long for your patience...
New algorithm is applicable for n<=20"


