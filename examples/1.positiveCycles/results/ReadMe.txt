Readme.txt
----------

positiveBool10.smb is the TotemBioNet input for 10 variables which has been generated using generatePositive.py.

positiveBool10-cks.csv has been obtained using the new approach proposed in the paper on positiveBool10-.smb.

Previous model-checking implemented in TotemBioNet is too slow.
