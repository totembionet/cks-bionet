# CKS-BioNet


## Constrained Kripke Structure for Biological Networks


## Description
When modelling a complex biological system, the bottleneck of the process
is the determination of parameter values that lead to model dynamics that
are compatible with observations. Even for discrete modelling frameworks,
this step can be limiting. In this project we introduce a representation of the whole
family of discrete models that can be associated to a biological system using a Constrained Kripke Structure (CKS). In CKS states are shared by all models and transitions are labelled by constraints on
dynamical parameters. A model checking procedure is implemented to handle this
new representation. This procedure extracts the conditions on parameter
settings that are compatible with a given dynamical property expressed in a
temporal logic. 

Four different biological systems are proposed as application examples: Presence of two stable states, reprissilator, Pseudomonas æruginosa in presence of Calcium and Check Points in Cell Cycle.

## Installation
You need to have "make" and a jdk installed on your computer. Otherwise, uncomment lines 4 and 7 of the install.sh file

# in a terminal:

./install.sh
will first unzip the libraries, compile and install TotemBioNet in CKS-BioNet.

Tests
------
To run one example, we recommend to change to the directory example, and follow the instructions given in HowToRunExamples.txt


## Authors
Hélène Collavizza is the project manager, she implemented the approach and contributed to its formalization. Laetitia Gibart contributed with the examples. Jean-Paul Comet is at the origin of this model-checking using a parameterized Kripke structure. He proved the validity of the parameterized model-checking algorithm and the equivalence between this new approach and the enumerative approach that model-checks successive Kripke structures using classical model-checking (NuSMV).


## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
