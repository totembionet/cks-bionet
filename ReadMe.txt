Readme.txt
----------

Installation
---------------------

You need to have "make" and a jdk installed on your computer. Otherwise, uncomment lines 4 and 7 of the install.sh file

# in a terminal:

./install.sh
    
will first install unzip the libraries, compile and install TotemBioNet in CKS-BioNet.

Tests:
------
To run one example, we recommend to change to the directory example, and follow the instructions given in HowToRunExamples.txt
