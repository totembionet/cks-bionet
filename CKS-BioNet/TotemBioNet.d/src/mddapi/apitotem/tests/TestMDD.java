package mddapi.apitotem.tests;


import mddapi.mddlib.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


/**
 * Rough test case for MDD creation, duplicate detection and usage counter.
 *
 * @author Aurelien Naldi
 */
public class TestMDD {

	public void testConstruction() {
		MDDManager ddmanager = getSimpleManager(5);
		MDDVariable[] variables = ddmanager.getAllVariables();

		System.out.println(" manager node count");
		System.out.println("count " +ddmanager.getNodeCount());
		System.out.println("mdd : " + ddmanager.dumpMDD(1));

		variables[4].getNode(0, 1);
		System.out.println("count " + ddmanager.getNodeCount());

		int n1 = variables[4].getNode(0, 1);
		System.out.println("count " + ddmanager.getNodeCount());
		System.out.println("n " + n1 + " mdd : " + ddmanager.dumpMDD(n1));

		int n2 = variables[3].getNode(0, 0);
		System.out.println("count " + ddmanager.getNodeCount());

		int node = variables[4].getNode(1, 0);
		System.out.println("count " + ddmanager.getNodeCount());

		int newnode = variables[4].getNode(node, node);
		System.out.println("node " + node + " new node " +  newnode);
		System.out.println("count " + ddmanager.getNodeCount());

		newnode = variables[2].getNode(n1, n2);
		System.out.println("count " + ddmanager.getNodeCount());

		System.out.println("Paths ...");

		PathSearcher paths = new PathSearcher(ddmanager);
		int[] path = paths.setNode(newnode);
		int nbpaths = 0;
		for (int leaf : paths) {
			nbpaths++;
			System.out.println("path 0 ," + path[0]);
			System.out.println("path 1 ," + path[1]);
			System.out.println("path 3 ," + path[3]);
			switch (nbpaths) {
			case 1:
				System.out.println("path 0 ,"+ path[2]);
				System.out.println("path 0 ," +path[4]);
				break;

			case 2:
				System.out.println("path 0 ," +path[2]);
				System.out.println("path 0 ," +path[4]);
				break;
			case 3:
				System.out.println("path 0 ," +path[2]);
				System.out.println("path 0 ," +path[4]);
				break;
			default:
				throw new RuntimeException("bad number of paths");
			}
		}

        String dump = ddmanager.dumpMDD(newnode);
        try {
            int n = ddmanager.parseDump(dump);
            System.out.println("n après dump " + n);
        } catch (ParseException e) {
            e.printStackTrace();
         }
    }

	public void testOrderProxy() {
		MDDManager manager = getSimpleManager(5);
		MDDVariable[] variables = manager.getAllVariables();

		List<String> keys2 = new ArrayList<String>();
		int[] altOrder = {3,1,4,0,2};
		for (int i: altOrder) {
			keys2.add( (String)variables[i].key );
		}

		MDDManager pManager = manager.getManager(keys2);

		// build two simple logical functions:
		//   var0 OR ( (NOT var2) AND var4 )
		//   NOT var1 OR ( (NOT var2) AND var4 )
		int node = variables[4].getNode(0, 1);
		node = variables[2].getNode(node, 0);
		int n1 = variables[1].getNode(1, node);
		node = variables[0].getNode(node, 1);


		// check simple evaluations on the two functions with two orders
		byte[] values = {0,0,1,0,0};

		System.out.println(" Reach ...");

		System.out.println( manager.reach(node, values));
		System.out.println( pManager.reach(node, values));	// true as NOT var2 AND var4
		System.out.println(manager.reach(n1, values));		// true as NOT var1
		System.out.println(pManager.reach(n1, values));	// true as NOT var1

		values = new byte[] {0,1,0,0,1};
		System.out.println(manager.reach(node, values));	// true as NOT var2 AND var4
		System.out.println( pManager.reach(node, values));
		System.out.println( manager.reach(n1, values));		// true as NOT var2 AND var4
		System.out.println( pManager.reach(n1, values));

		values = new byte[] {1,1,1,0,0};
		System.out.println( manager.reach(node, values));	// true as NOT var0
		System.out.println( pManager.reach(node, values));	// true as NOT var2 AND var4
		System.out.println( manager.reach(n1, values));
		System.out.println( pManager.reach(n1, values));	// true as NOT var2 AND var4


		// check the paths provided by PathSearch
		PathSearcher ps = new PathSearcher(manager, 1);
		PathSearcher ps2 = new PathSearcher(pManager, 1);

		// {3,1,4,0,2}

		checkPath(ps, node, 	new int[][] { { 0, -1,  0, -1,  1},  { 1, -1, -1, -1, -1} });
		checkPath(ps2, node, 	new int[][] { {-1, -1,  1,  0,  0},  {-1, -1, -1,  1, -1} });

		checkPath(ps, n1, 		new int[][] { {-1,  0, -1, -1, -1},  {-1,  1,  0, -1,  1} });
		checkPath(ps2, n1, 		new int[][] { {-1,  0, -1, -1, -1},  {-1,  1,  1, -1,  0} });


		// stress path searcher
		ps.setNode(1);
		boolean first = true;
		for (int l: ps) {
			if (!first) {
				System.out.println("erreur");
			}
			System.out.println("vars in path");
			for (int v: ps.getPath()) {
				System.out.println( v);
			}
			first = false;
		}
	}

	private void checkPath(PathSearcher ps, int node, int[][] expected) {
		int[] path = ps.setNode(node);
		int n=0;
		for (int v: ps) {
			// check it
			int[] curExpected = expected[n];
			for (int i=0 ; i<path.length ; i++) {
				System.out.println("expected, path " + curExpected[i] + "," + path[i]);
			}

			n++;
		}
	}


	public void testInferSign() {
		MDDManager ddmanager = getSimpleManager(5);
		MDDVariable[] variables = ddmanager.getAllVariables();

		int n1 = variables[4].getNode(0, 1);
		int n2 = variables[4].getNode(1, 0);
		int n3 = variables[2].getNode(n1, n2);


		System.out.println( ddmanager.getVariableEffect(variables[0], 0));
		System.out.println( ddmanager.getVariableEffect(variables[0], n1));
		System.out.println( ddmanager.getVariableEffect(variables[0], n2));
		System.out.println( ddmanager.getVariableEffect(variables[0], n3));

		System.out.println( ddmanager.getVariableEffect(variables[4], n1));
		System.out.println( ddmanager.getVariableEffect(variables[4], n2));
		System.out.println( ddmanager.getVariableEffect(variables[4], n3));
		System.out.println( ddmanager.getVariableEffect(variables[2], n3));
		System.out.println( ddmanager.getVariableEffect(variables[3], n3));

	}


	public void testIntervalPathSearcher() {
		MDDVariableFactory varFactory = new MDDVariableFactory();
		for (int i = 0; i < 5; i++) {
			varFactory.add("var" + i, (byte)3);
		}
		MDDManager ddmanager = MDDManagerFactory.getManager( varFactory, 10);
		MDDVariable[] variables = ddmanager.getAllVariables();

		int n1 = variables[4].getNode(new int[]{0, 0, 1});
		int n2 = variables[4].getNode(new int[]{1, 0, 0});
		int n3 = variables[2].getNode(new int[]{1, n1, n2});

		PathSearcher ps = new PathSearcher(ddmanager, true);
		checkPath(ps, n3, 	new int[][] { { -1, -1,  0, -1,  -1},  { -1, -1,  1, -1,  0}, { -1, -1,  1, -1,  2}, { -1, -1,  2, -1,  0}, { -1, -1,  2, -1,  1},});
	}


	public void testMultivaluedNot() {
		MDDVariableFactory varFactory = new MDDVariableFactory();
		for (int i = 0; i < 5; i++) {
			varFactory.add("var" + i, (byte)3);
		}
		MDDManager ddmanager = MDDManagerFactory.getManager( varFactory, 10);
		MDDVariable[] variables = ddmanager.getAllVariables();

		int n1 = variables[4].getNode(new int[]{0, 0, 1});
		int n2 = variables[4].getNode(new int[]{1, 0, 0});
		int n3 = variables[2].getNode(new int[]{1, n1, n2});

		int n4 = ddmanager.not(n1);
		n4 = ddmanager.not(n4);
		System.out.println(" not " + n1 + "," + n4);

		n4 = ddmanager.not(n2);
		n4 = ddmanager.not(n4);
		System.out.println(" not " + n2 + "," + n4);

		n4 = ddmanager.not(n3);
		n4 = ddmanager.not(n4);
		System.out.println(" not " + n3+ "," + n4);
	}


	public static MDDManager getSimpleManager(int size) {
		List<String> keys = new ArrayList<String>();
		for (int i = 0; i < size; i++) {
			keys.add("var" + i);
		}
		return MDDManagerFactory.getManager( keys, 10);
	}

	public static void main(String[] s) {
		TestMDD tm = new TestMDD();
		tm.testConstruction();
	}
}

