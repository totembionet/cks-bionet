package mddapi.apitotem;

import util.TotemBionetException;

import java.io.*;
import java.util.*;

/**
 *
 * Class to read/write set of data from/to csv files
 *
 * @author Cyril Guillot
 * Polytech Sophia - SI4
 *
 * @author Hélène Collavizza
 *
 * NOTA: the two first colummns contain number and validity and are not useful in many cases
 *       the last column is the error explanation and is not useful
 *
 */

public class CSVParaReader {

    private BufferedReader br;
    private String valid;
    private int nbRows;

    CSVParaReader(String path,String valid) throws IOException {
        br = new BufferedReader(new FileReader(path));
        this.valid=valid;
    }

    void close() throws IOException {
        br.close();
    }

    /**
     *  Read the csv header with variable names
     * @return array of variables names
     *
     * NOTA: HashSet is not a good idea because order of variables in HashSet and in MDDVariableManager will differ
     * @throws IOException
     */
    HashMap<Integer,String> readVar() throws IOException{
        HashMap<Integer,String> indices = new HashMap<Integer,String>();
        String line = br.readLine();
        String[] rows = line.split(";");
        //if (rows[rows.length-1].equals())
        for (int i = 2 ; i < rows.length; i ++) {
            if (rows[i].startsWith("K_"))
                indices.put(i-2,rows[i]);
        }
        nbRows=indices.size();
        //System.out.println("vars " + indices);
        return indices;
    }

    /**
     * Read the second line of csv file. It contains the domains of the variables
     * @param  csvVars maps (indices,variable names)
     * @return map that associates to each variable its domain
     * @throws Exception
     *
     */
    HashMap<String,int[]> readDomains(HashMap<Integer,String> csvVars) throws Exception {
        String line = br.readLine();
        String[] rows = line.split(";");
        HashMap<String,int[]> domains = new HashMap<>();
        for (int i=2;i<csvVars.size()+2;i++) {
            String[] parts = rows[i].split("\\.\\.");
            int maxi = Integer.parseInt(parts[1]);
            int mini = Integer.parseInt(parts[0]);
            int[] domain = {i-2,mini, maxi};
            //System.out.println(name + " : " + mini + " " + maxi);
            domains.put(csvVars.get(i-2), domain);
        }
        return domains;
    }

    /**
     * Read all the values of the csv
     * @return a table with all the line translated as int (-1 if dont care (-)
     * or not depends on the variable (min..max))
     * @throws IOException
     */
    List<int[]> readValues() throws IOException {
        String line = "";
        List<int[]> records = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            // when there is an error explanation, must ignore it
            int crochet = line.indexOf('[');
            if (crochet !=-1) {
                line = line.substring(0, crochet-1);
            }
            String[] values = line.split(";");
            if (values[1].equals(valid))
                records.add(getIntValues(values));
        }
        return records;
    }

    /**
     * Translate String as int
     * @param values a line of values in the csv
     * @return values where:
     *            - dont care (-) is translated as -1,
     *            - all values are possible (min..max) is translated as -1,
     *            - others are int values
     */
    private int[] getIntValues(String[] values) {
        // must skip the 2 first columns (number of model and type of model)
        //int len =values.length-2;
        int len = nbRows;
        int[] val = new int[len];
        int v=2;
        for (int k = 0; k < len;k++) {
            String str = values[v];
             if (str.equals("-") || str.contains("..")) {
                val[k] = -1;
            } else {
                val[k] = Integer.parseInt(str);
            }
            v++;
        }
//        System.out.println("val " + Arrays.toString(val));
        return val;
    }
    
}
