package mddapi.apitotem;

import back.net.Gene;

import mddapi.mddlib.*;
import util.Out;
import util.TotemBionetException;
import util.jlist.OrderedArrayList;
import back.net.Para;

import mddapi.mddlib.operators.MDDBaseOperators;
import util.jlist.OrderedArrayListStringID;

import java.io.IOException;
import java.util.*;

/**
 *
 * Class to build a MDD from data in CSV
 *
 * @author Cyril Guillot
 * Polytech Sophia - SI4
 *
 * @author Hélène Collavizza
 */

public class MDDBuilder {

    // to manage variables
    MDDVariableFactory vbuilder;
    // to manage MDD
    MDDManager mddManager;
    // root of the MDD
    int root;

    // map index in csv to name of the variable
    private HashMap<Integer,String> indexToCsvVar;
    // map variable name to couple (index,domain)
    HashMap<String, int[]> csvVarToIndexAndDomain;
    // filename for intersection
    private String fileName;
    // to know if a variable contains a valuer after creating the MDD
    //    => if MDD does not depend on this variable, the variable is not written in the result
    HashMap<String,Boolean> containsAValue;
    // used in MDDFacade to know if this is the first file of the intersection
    private boolean isFirst;
    // the first map to be able to compare domains
    HashMap<String, int[]> firstCsvVarToIndexAndDomain;

    long timeToBuild=0;

    // used in AbstracSearchEngine to create an empty MDD
    public MDDBuilder() {
        this(null,new MDDVariableFactory(),new HashMap<String,Boolean>(),null);
        isFirst=true;
     }

    // used in MDDFacade to build the first MDD associated with the first file
    public MDDBuilder(String fn) {
        this(fn, new MDDVariableFactory(), new HashMap<String, Boolean>(),null);
        isFirst=true;
    }

     // used in MDDFacade to build the MDDs of other files
     public MDDBuilder(String fn,MDDVariableFactory vbuilder,HashMap<String,Boolean> hasValue,HashMap<String, int[]> map) {
        this.vbuilder = vbuilder;
        csvVarToIndexAndDomain = new HashMap<>();
        firstCsvVarToIndexAndDomain = map;
        isFirst=false;
        indexToCsvVar = null;
        fileName=fn;
        this.containsAValue=hasValue;
    }

    /**
     * Init the mddlib builder, specifying the range of each variable from set of genes
     * from the genes of a Net
     * NOTA: when parameters have fixed values in PARA bloc, need to put
     *       the domain of the gene to be able to handle ..
     */
    private void initVbuilder(OrderedArrayListStringID<Gene> genes) {
        indexToCsvVar = new HashMap<Integer, String>();
        int index =2;
        for (Gene g : genes) {
//            byte range = (byte) (g.max()-g.min +1);
            for (Para p : g.getParas()) {
                byte range = (byte) (p.maxLevel()+1);
                int[] dom = {index-2,p.minLevel(),p.maxLevel()};
                String n = p.getSMBname();
                vbuilder.add(n, range);
                csvVarToIndexAndDomain.put(n,dom);
                indexToCsvVar.put(index,n);
                index++;
            }
        }
    }

    /**
     * Init the mddlib builder, specifying the range of each variable
     *    NOTA: if the domain is mi..ma where mi!=0 then its domain in MDD is 0..ma
     *          and values between 0..(mi-1) throws an exception when writing the result
     */
    private void initVbuilder() throws TotemBionetException {
        for (String varName : indexToCsvVar.values()) {

            int[] domain = getDomainFromName(varName);
            byte nbVal = (byte) (domain[1] + 1);
            //vbuilder.add(varName, (byte) (domain[1]-domain[0]+1));
            if (isFirst) {
                vbuilder.add(varName, nbVal);
            } else {
                // if the domains are not compatible, throws an error
                int[] firstDomain = firstCsvVarToIndexAndDomain.get(varName);
                if (firstDomain==null)
                    throw new TotemBionetException("Variable " + varName + " not found. It is in "
                            + fileName + " but it is not in the other files");
                //System.out.println(Arrays.toString(firstDomain) + " " + Arrays.toString(domain));
                if ((domain[1] < firstDomain[1]) | (domain[0] > firstDomain[2])) {
                    throw new TotemBionetException("Domains of " + varName + " in file " + fileName + " is not compatible with other files : "
                            + domain[0] + ".." + domain[1] + " instead of " + firstDomain[1] + ".." + firstDomain[2]);
                }
                // TODO : on pourrait réduire le domaine initial mais
                // ça pose des problèmes car cela vire des variables ...
//                else {
//                    boolean changed = false;
//                    if (domain[0] > firstDomain[1]) {
//                        changed=true;
//                        firstDomain[1] = domain[0];
//                    }
//                    if (domain[1] < firstDomain[2]) {
//                        changed = true;
//                        firstDomain[2] = domain[1];
//                    }
//                    if (changed){
//                        vbuilder.remove(varName);
//                        byte size = (byte) (firstDomain[2]+1);
//                        vbuilder.add(varName,size);
//                    }
//                }

            }

//            if (vbuilder.contains(varName)){
//                if (vbuilder.getNbValue(varName)!= nbVal)
//                    throw new TotemBionetException("Domains of " + varName + " in file " + fileName + " differ from other files : it contains "
//                            + nbVal + " values instead of " + vbuilder.getNbValue(varName));
//            }
//            else
//                vbuilder.add(varName, nbVal);
        }

    }

    // to create an empty MDD from a gene list
    // used in AbstracSearchEngine
    public MDDParaSet createEmptyFromGene(String name, OrderedArrayListStringID<Gene> genes, String type) {
        initVbuilder(genes);
        mddManager = MDDManagerFactory.getManager( vbuilder, 2);
        root = -1;
//        System.out.println("manager " + Arrays.toString(mddManager.getAllVariables()));
        return new MDDParaSet(name, type, this, root);
    }

    /**
     * Tocreate a MDD from a CSV file
     * @param path to the file
     * @param type OK or KO
     * @return a MDD
     * @throws TotemBionetException
     */
    public MDDParaSet createFromCSV( String path, String type) throws TotemBionetException {
         CSVParaReader reader = null;
        // get csv data
        try {
            reader = new CSVParaReader(path,type);
        }catch (IOException e) {
            throw new TotemBionetException("Invalid path for intersection/union of models");
        }

        // read variables
        try {
            indexToCsvVar =reader.readVar();
        } catch (IOException e) {
            throw new TotemBionetException("Problem reading vars in csv");
        }
        // read domains
        try {
            csvVarToIndexAndDomain = reader.readDomains(indexToCsvVar);
        } catch (Exception e) {
            throw new TotemBionetException("Problem reading domains in csv");
        }
        initVbuilder();
        // read rows and build the MDD
        try{
            List<int[]> valid_rows= reader.readValues();
            reader.close();
            if (valid_rows.size() == 0) {
                MDDLogger.mddInfo("   No MDD created from " + path + ", 0 " + type + " models detected");
                return null;
            }
            return getMDD(valid_rows, vbuilder, type);

        } catch (IOException e) {
            throw new TotemBionetException("Problem reading values in csv");
        }
    }


    /**
     * Build MDD using mddManager from mddlib
     * @param valid_rows: the rows corresponding to type (OK or KO)
     * @param vbuilder: MDDlib manager for variables
     * @param type: OK or KO
     * @return
     */
    MDDParaSet getMDD(List<int[]> valid_rows, MDDVariableFactory vbuilder, String type) throws TotemBionetException{
        long t1= System.currentTimeMillis();
        mddManager = MDDManagerFactory.getManager( vbuilder, 2);
        root = addAllPathsFromRows(valid_rows,mddManager.getAllVariables());
        long t2= System.currentTimeMillis();
        timeToBuild+=(t2-t1);
        return new MDDParaSet("", type, csvVarToIndexAndDomain,mddManager, vbuilder, root);
    }


    /**
     *    Add all paths to the MDD from all rows
     *
     * @param rows : a set of lines. Each line gives a value to each variables
     * @return : the node of the MDD which is the union of all lines
     */
    private int addAllPathsFromRows(List<int[]> rows,MDDVariable[] mddVariables) throws TotemBionetException{
        int[] start_nodes = new int[rows.size()];
        for (int i = 0 ; i < rows.size() ; i++) {
            start_nodes[i] = addPathFromRow(rows.get(i),mddVariables);
        }
        // combine all starts node of each path
        return MDDBaseOperators.OR.combine(mddManager, start_nodes);

    }


    /**
     *    Build a path to the MDD from a row
     *    The path contains one assignment to each variable except when
     *    the variable value is -1 (means no value)
     *
     *    Leaf node has number 1
     *
     * @param row : a line where row[i] is the value of variable #i
     * @return : a node associated to the first variable in row
     *
     */
    int addPathFromRow(int[] row,MDDVariable[] mddVariables) throws TotemBionetException{
        int n_var = mddVariables.length;
//        System.out.println("nb var " + n_var + "size row " + row.length);
        int last_node= 1;
         for (int i = n_var-1 ; i >= 0; i--) {
            MDDVariable variable = mddVariables[i];
            String name = variable.toString();
            int[] values = new int[variable.nbval];
            int csvVarIndex = getIndexFromName(name);
//            System.out.println("row " + Arrays.toString(row));
            if (row[csvVarIndex] != -1) {
                // the domains of the variables doesn't start with 0
                if (row[csvVarIndex]>=variable.nbval) {
                    throw new TotemBionetException("Value " + row[csvVarIndex] + " is impossible for variable " + variable +
                            ". Its domain is " + Arrays.toString(getDomainFromName(name)));
                }
                Arrays.fill(values, 0);
                values[row[csvVarIndex]] = last_node;
                last_node = variable.getNode(values);
                containsAValue.put(name,true);
            }
        }
        return last_node;
    }

    // to get the index of a variable in the csv file (i.e. its column number)
    private int getIndexFromName(String n) throws TotemBionetException{
        if (! csvVarToIndexAndDomain.containsKey(n))
            throw new TotemBionetException("Variable " + n + " not found. It is in " + fileName +
                    " but is not in the other files");
        return csvVarToIndexAndDomain.get(n)[0];
    }

    // to get the triple (index,min,max) of a variable
    private int[] getDomainFromName(String n) throws TotemBionetException{
        if (! csvVarToIndexAndDomain.containsKey(n))
            throw new TotemBionetException("Variable " + n + " not found. It is in " + fileName +
                    " but is not in the other files");
        int[] val = csvVarToIndexAndDomain.get(n);
        return new int[]{val[1],val[2]};
    }


    // compute the set of variables which are not used
    // these variables are in the set of variables read from a csv file or a set of models
    // (AbstracSearchEngine) but their are not used (i.e. their values are the whole domain of the variable)
    public HashSet<String> unusedVariables(){
        HashSet<String> unused = new HashSet<String>();
        //System.out.println(" has " + firstMddBuilder.containsAValue);
        int i= 0;
        while (i<vbuilder.size()) {
            String name = vbuilder.get(i).toString();
            if (!containsAValue.containsKey(name))
                unused.add((String) vbuilder.get(i));
            i++;
        }
        System.out.println("***    Unused variables are : " + unused);
        if (unused.size()>0) {
            System.out.println("***    Warning    *** \n***    " + unused.size() + " variables are unused in the intersection (use -verbose 2 to know which ones)");
            if (Out.verb()>=1)
                System.out.println("***    Unused variables are : " + unused);
            System.out.println("***    End warning    ***");
        }
        return unused;
    }

    ///////////////////////////////////////////////////////////////////////
    ///
    /// Everything below is about creating a MDD with every variable values
    ///
    ///////////////////////////////////////////////////////////////////////

//    public MDD createAllParameterizations(List<Gene> genes) {
//
//        variablesNames = new ArrayList<>();
//        variablesRanges = new HashMap<>();
//        for (Gene gene : genes) {
//            for (Para para : gene.getParas()) {
//                variablesNames.add(para.getSMBname());
//                variablesRanges.put(para.getSMBname(),gene.max());
//            }
//        }
//        vbuilder = createVariableFactoryAllParameterizations(variablesNames, variablesRanges);
//
//        mddManager = MDDManagerFactory.getManager( vbuilder, 2);
//        mddVariables = mddManager.getAllVariables();
//        int[] array = new int[variablesNames.size()];
//        Arrays.fill(array, -1);
//        paths = new ArrayList<>();
//        recursive(array, 0);
//        root = addAllPathsFromRows(paths);
//        return new MDD("", mddManager, vbuilder, root);
//    }
//
//    private void recursive(int[] current_path, int current_var_index) {
//        if (current_var_index  == variablesNames.size()) {
//            paths.add(current_path);
//            return;
//        }
//        int index = contains(variablesNames.get(current_var_index), variablesNames, current_var_index);
//        int min = 0;
//        int max = variablesRanges.get(variablesNames.get(current_var_index));
//        if (index != -1) {
//            min = current_path[index];
//        }
//        for( int i = min; i <= max; i++) {
//            int[] newpath = current_path.clone();
//            newpath[current_var_index] = i;
//            recursive(newpath, current_var_index + 1);
//        }
//    }
//
//    private int contains(String value, List<String> array, int max_index) {
//        if (!value.contains(":")) return -1;
//        int index = value.lastIndexOf(":");
//        String str_to_check = value.substring(0,index-1);
//        for (int i = 0 ; i < max_index; i++) {
//            String v = array.get(i);
//            if (v.contains(str_to_check) && ! v.equals(value)) {
//                return i;
//            }
//        }
//        return -1;
//    }
//
//    private MDDVariableFactory createVariableFactoryAllParameterizations(List<String> variables, HashMap<String, Integer> variables_ranges) {
//        MDDVariableFactory vbuilder = new MDDVariableFactory();
//        for (int i = 0 ; i < variables.size(); i++) {
//            String var_name = variables.get(i);
//            vbuilder.add(var_name, variables_ranges.get(var_name).byteValue());
//        }
//        return vbuilder;
//    }
//


}