package mddapi.apimodelcheck;


import mddapi.apitotem.MDDBuilder;
import mddapi.apitotem.MDDParaSet;
import mddapi.mddlib.*;
import mddapi.mddlib.operators.MDDBaseOperators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class Test {

    public static void testConstraint(int nbVar, byte nbVal){
        // build the MDD manager
        MDDVariableFactory vbuilder = new MDDVariableFactory();
        for (int i=0;i<nbVar;i++)
            vbuilder.add("K_"+i,nbVal);
        MDDManager mddManager= MDDManagerFactory.getManager( vbuilder,nbVal);
        MDDVariable[] vars = mddManager.getAllVariables();
        int[] nodes = makeConstraint(nbVal,vars);
        int done = combine(nodes,mddManager);
        printNbPaths(mddManager,done);

        int l = nodes.length;
        System.out.println(mddManager.dumpMDD(nodes[l-2]));

        long before = System.currentTimeMillis();
        int or = MDDBaseOperators.OR.combine(mddManager, new int[] {done,nodes[l-2]});
        long after = System.currentTimeMillis();
        System.out.println("last or " + (after-before) + "ms");
        printNbPaths(mddManager,or);

        // and avec la dernière contrainte
        int and = MDDBaseOperators.AND.combine(mddManager, new int[] {done,nodes[l-2]});
        long after2 = System.currentTimeMillis();
        System.out.println("last and " + (after2-after) + "ms");
        printNbPaths(mddManager,and);

        long stop = System.currentTimeMillis();
        int neg = mddManager.not(nodes[l-1]);
        int inter = MDDBaseOperators.AND.combine(mddManager, new int[] {neg,or});
        long stop2 = System.currentTimeMillis();
        System.out.println("time for stop condition " + ((after-before) + (stop2-stop)));
        printNbPaths(mddManager,inter);

        //System.out.println("result " + pathsToString(mddManager,vars,done));

    }
    // build nbVar constraints, one for each variable
    // constraint is either v=n or v<n or v>n or no constraint
    public static int[] makeConstraint(byte nbVal,MDDVariable[] vars) {

        int nbVar= vars.length;

        Random random = new Random();

        // to add values
        int[] values = new int[nbVal];

        // the nodes of MDD of constraints
        int[] nodes = new int[nbVar];

        // init the domains
        for (int i = 0; i < nbVar; i++) {
            //  0 <, 1 >, 2 =, 3 à nbCst : no constraint
            int nbCst = 8;
            int choice = random.nextInt(nbCst);
            int n = random.nextInt(nbVal);
            Arrays.fill(values, 0);
            switch (choice) {
                case 0: { // init vi with constraint vi<nb
                    for (int j = 0; j < n; j++) {
                        values[j] = 1;
                    }
                    System.out.println(vars[i] + " < " + n + " " + Arrays.toString(values));
                    break;
                }
                case 1: { // init vi with constraint vi>nb
                    for (int j = n; j < nbVal; j++) {
                        values[j] = 1;
                    }
                    System.out.println(vars[i] + " > " + n + " " + Arrays.toString(values));
                    break;
                }
                case 2: { // init vi with constraint vi=nb
                    values[n] = 1;
                    System.out.println(vars[i] + " = " + n +  " " + Arrays.toString(values));
                    }
                    break;
                }
            nodes[i] = vars[i].getNode(values);
        }
        return nodes;
    }

    public static int combine(int[] nodes, MDDManager mdd) {
        Random random = new Random();
        int root = nodes[0];
        long total = 0;
        for (int i=1;i<nodes.length-2;i++) {
            boolean or = random.nextInt(11)<9;
            long before = System.currentTimeMillis();
            if (or) {
                root = MDDBaseOperators.OR.combine(mdd, new int[] {root,nodes[i]});
            }
            else {
                root = MDDBaseOperators.AND.combine(mdd, new int[] {root,nodes[i]});
            }
            long after = System.currentTimeMillis();
            System.out.println(or + " " + i + " " + (after-before) + "ms");
            total += (after-before);
        }
        System.out.println("tps total " + total);
        return root;
    }

    public static void testStopCond() {
        MDDVariableFactory varFactory = new MDDVariableFactory();
        for (int i = 0; i < 5; i++) {
            varFactory.add("var" + i, (byte) 3);
        }
        MDDManager ddmanager = MDDManagerFactory.getManager(varFactory, 10);
        MDDVariable[] variables = ddmanager.getAllVariables();

        int[] nodes = new int[10];
        nodes[0] = variables[4].getNode(new int[]{0, 0, 1});
        nodes[1] = variables[4].getNode(new int[]{1, 0, 0});
        nodes[2] = variables[2].getNode(new int[]{1, nodes[0], nodes[1]});
        nodes[3] = variables[2].getNode(new int[]{0, nodes[0], 1});
        System.out.println("n2 "  + pathsToString(ddmanager,variables,nodes[2]));
        System.out.println("n3 "  + pathsToString(ddmanager,variables,nodes[3]));

        int val = MDDBaseOperators.OR.combine(ddmanager, new int[] {nodes[0],nodes[1],nodes[2]});
        System.out.println("val "  + pathsToString(ddmanager,variables,val));

        int or = MDDBaseOperators.OR.combine(ddmanager, new int[] {val,val});
        System.out.println("or "  + pathsToString(ddmanager,variables,or));

        // teste si (c ou nc) est équivalent à nc
        // si l'intersection de (c ou nc) et negation nc est vide
        int neg = ddmanager.not(val);
        System.out.println("neg n3"  + pathsToString(ddmanager,variables,neg));
        int inter = MDDBaseOperators.AND.combine(ddmanager, new int[] {neg,or});
        System.out.println("stop "  + pathsToString(ddmanager,variables,inter));

    }

    public static void go(){
        System.out.println("Test of MDDs");
//        testMultivaluedNot();
//        testStopCond();
        testConstraint(24,(byte)3);
    }

    public static void printNbPaths(MDDManager mdd, int root) {
        PathSearcher ps = new PathSearcher(mdd, 1);
        ps.setNode(root);
        System.out.println("MDD has "  + ps.countPaths() + " paths");
    }


    public static String pathsToString(MDDManager mdd, MDDVariable[] vars,int root) {
        PathSearcher ps = new PathSearcher(mdd, 1);

        ps.setNode(root);
        String res="MDD has "  + ps.countPaths() + " paths\n";
        int k= 0;
        for (int p : ps) {
            int[] path = ps.getPath();
            String str = "Path " + k + "::: ";
            for (int i = 0; i < path.length; i++) {
                str += vars[i] + " : " + path[i] + ", ";
            }
            k++;
            res+=str+"\n";
        }
        return res;
    }

    public static  void testMultivaluedNot() {
        MDDVariableFactory varFactory = new MDDVariableFactory();
        for (int i = 0; i < 5; i++) {
            varFactory.add("var" + i, (byte) 3);
        }
        MDDManager ddmanager = MDDManagerFactory.getManager(varFactory, 10);
        MDDVariable[] variables = ddmanager.getAllVariables();

        int n1 = variables[4].getNode(new int[]{0, 0, 1});
        int n2 = variables[4].getNode(new int[]{1, 0, 0});
        int n3 = variables[2].getNode(new int[]{1, n1, n2});

        System.out.println("dump n1 avant not " + ddmanager.dumpMDD(n1));
        int n4 = ddmanager.not(n1);
       System.out.println("dump n4 " + ddmanager.dumpMDD(n4));


        n4 = ddmanager.not(n4);
        System.out.println(" not " + n1 + "," + n4);
       System.out.println("dump not n4 = n1 " + ddmanager.dumpMDD(n4));



//
//        n4 = ddmanager.not(n2);
//        n4 = ddmanager.not(n4);
//        System.out.println(" not " + n2 + "," + n4);
//
//        n4 = ddmanager.not(n3);
//        n4 = ddmanager.not(n4);
//        System.out.println(" not " + n3 + "," + n4);
    }
}
