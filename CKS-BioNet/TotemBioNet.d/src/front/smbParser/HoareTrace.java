package front.smbParser;

import java.util.*;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

enum HoareTraceType {
    SKIP, AFFECT, OPER, IF, WHILE, FORALL, EXISTS, ASSERT, SEQ
}

public class HoareTrace {

    private HoareTraceType type;

    private List<HoareTrace> traces;
    private List<HoareAssert> assertions;

    private String id;
    private String op;
    private Integer constant;

    public HoareTrace() {

        this.type = null;

        this.traces = new ArrayList<HoareTrace>();
        this.assertions = new ArrayList<HoareAssert>();

        this.id=null;
        this.op=null;
        this.constant=null;

    }

    public HoareTraceType getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getOp() {
        return op;
    }

    public Integer getConstant() {
        return constant;
    }

    public List<HoareTrace> getTraces() {
        return traces;
    }

    public HoareTrace getTrace(int i){
        return traces.get(i);
    }

    public List<HoareAssert> getAssertions() {
        return assertions;
    }

    public HoareAssert getAssertion(int i) {
        return assertions.get(i);
    }

    public void addTrace(HoareTrace trace){
        this.traces.add(trace);
    }

    public void addAssert(HoareAssert assertion){
        this.assertions.add(assertion);
    }

    public void setType(HoareTraceType type) {
        this.type = type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public void setConstant(Integer constant) {
        this.constant = constant;
    }
}