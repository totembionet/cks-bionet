package front.smbParser;

import java.io.FileOutputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 * @author helen
 *
 * class for building ocaml file associated with a Hoare triple
 */

public class OneHoareBlock {
    private List<HoareExpr> preExprList;
    private HoareTrace trace;
    private List<HoareAssert> postExprList;
    private String id;  // identifier possibly empty
    private HashSet<String> preVars;


    public OneHoareBlock() {
        this.preVars = new HashSet<String>();
        this.preExprList = new ArrayList<HoareExpr>();
        this.trace = new HoareTrace();
        this.postExprList = new ArrayList<HoareAssert>();
    }

    void setId(String id) {
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void addPreVar(String id){
        preVars.add(id);
    }

    public boolean allVarInPreHoare(VarBlock vars) {
        for (Var v:vars.getData()) {
            if ((v.getVarType() != VarType.ENV_VAR) &&!preVars.contains(v.getId()))
                return false;
        }
        return true;
    }

    public void addPreExpression(String id, String operation, Integer constant){
        this.preExprList.add(new HoareExpr(id, operation, constant));
    }

    public void setTraceExpression(HoareTrace trace){
        this.trace = trace;
    }

    public void addPostExpression(HoareAssert assertion){
        this.postExprList.add(assertion);
    }

    public String toTripletOcaml() {
        String preDecl = "let wps = ";
        preDecl += mlForPre();
        preDecl += ";;";

        String progDecl = "let prog = ";
        progDecl += mlForTrace(this.trace);
        progDecl += ";;";

        String postDecl = "let post = ";
        postDecl += mlForPost();    
        postDecl += ";;";

        String printResult = "print_endline (string_of_formula wps) ;;";

        return  progDecl + "\n\n" + postDecl + " \n\n" + preDecl + " \n\n" + printResult + " \n\n";
    }

    private String mlForTrace(HoareTrace root){
        String trace = "";
        
        switch(root.getType().name()){
            
            case "SKIP" : {
                trace = "Skip";
            }break;
            
            case "AFFECT" : {
                trace = "Set( ExprVar \"" + root.getId() + "\", ExprConst" + root.getConstant() + " )";
            }break;
            
            case "OPER" : {
                trace = HoareBlocks.FOL_SYMBOL.get(root.getOp()) + " \"" + root.getId() + "\"";
            }break;
            
            case "IF" : {
                trace = "If( " + mlForAssert(root.getAssertion(0)) + ", " 
                              + mlForTrace(root.getTrace(0))
                              + ((root.getTraces().size()>1) ?  
                                                ", " +  mlForTrace(root.getTrace(1))
                                                : "")
                              + " )";
            }break;
            
            case "WHILE" : {
                trace = "While( " + mlForAssert(root.getAssertion(0)) + ", " 
                              + ((root.getAssertions().size()>1) ?  
                                                ", " +  mlForAssert(root.getAssertion(1))
                                                : "")
                              + " )";
            }break;
            
            case "FORALL" : {
                int size = root.getTraces().size();
                StringBuffer tmp = new StringBuffer();
                if(size>0)
                    tmp = tmp.append(mlForTrace(root.getTrace(0)));
                
                if(size>1){
                    for(int i=1; i<size; i++){
                       tmp = new StringBuffer("Forall(" + tmp + " , " + mlForTrace(root.getTrace(i)) + ")");
                    } 
                } else {
                    tmp = new StringBuffer("Forall( " + tmp + ")");
                }

                trace = tmp.toString();

            }break;
            
            case "EXISTS" : {
                int size = root.getTraces().size();
                StringBuffer tmp = new StringBuffer();
                if(size>0)
                    tmp = tmp.append(mlForTrace(root.getTrace(0)));
                
                if(size>1){
                    for(int i=1; i<size; i++){
                       tmp = new StringBuffer("Exists(" + tmp + " , " + mlForTrace(root.getTrace(i)) + ")");
                    } 
                } else {
                    tmp = new StringBuffer("Exists( " + tmp + ")");
                }

                trace = tmp.toString();
            }break;
            
            case "ASSERT" : {
                trace = "Assert( " + mlForAssert(root.getAssertion(0)) + " )";
            }break;
            
            case "SEQ" : {
                trace = "Seq( " +  mlForTrace(root.getTrace(0)) + ", " 
                                +  mlForTrace(root.getTrace(1))
                                +  " )";
            }break;
            
            default : {
                System.err.println("Error in ml file generation  : unknown Trace type " + root.getType().name());
            }break;
        }

        return trace;
    }

    private String mlForAssert(HoareAssert assertion){
        String formula = "";

        switch(assertion.getType().name()){
            
            case "NEG" : {
                formula = "PropUn( " + HoareBlocks.FOL_SYMBOL.get(assertion.getOp()) + ", "
                                    + mlForAssert(assertion.getExpr1())
                                    + " )";
            }break;
            
            case "BOOL_OP" : {
                formula = "PropBin( " + HoareBlocks.FOL_SYMBOL.get(assertion.getOp()) + ", "
                                    + mlForAssert(assertion.getExpr1()) + ", "
                                    + mlForAssert(assertion.getExpr2())
                                    + " )";
            }break;
            
            case "COMP" : {
                formula = "Rel( " + HoareBlocks.FOL_SYMBOL.get(assertion.getOp()) + ", "
                                  + mlForAssert(assertion.getExpr1()) + ", "
                                  + mlForAssert(assertion.getExpr2())
                                  + " )";
            }break;
            
            case "SIMPLE_TERM_ID" : {
                formula = "ExprVar \"" + assertion.getTerm() + "\""; 
            }break;
            
             case "SIMPLE_TERM_NUM" : {
                formula = "ExprConst " + assertion.getTerm(); 
            }break;
            
             case "SIMPLE_TERM_KID" : {
                String[] parts = null;
                String id = assertion.getTerm();
                if(id.contains(":")){
                    parts = id.split("\\:");
                }else {
                    parts = new String[]{id};
                }
                String gene = parts[0].split("K")[1];

                formula = "ExprParam \"" + gene + "\"";

                if(parts.length>1){
                    formula += ", (";
                    for(int i=1; i<parts.length; i++){
                        formula += "\"" + parts[i] + "\", ";
                    }
                    formula = formula.substring(0, formula.length() - 2);
                    formula += " )";
                }

            }break;

            case "OPER_TERM" : {
                formula = "ExprBin( " + ((assertion.getOp()=="+") ? "Plus" : "Minus") + ", "
                                      + mlForAssert(assertion.getExpr1()) + ", "
                                      + mlForAssert(assertion.getExpr2())
                                      + " )";
            }break;
            default : {
                System.err.println("Error in ml file generation  : unknown Assert type ");
            }break;
        }

        return formula;
    }

    private String mlForPost(){
        int size = this.postExprList.size();
        StringBuffer tmp = new StringBuffer(mlForAssert(postExprList.get(0)));
        if(size > 1){
            for (int i=1; i<size ; i++ ) {
                tmp = new StringBuffer("PropBin(And, " + tmp + ", " + mlForAssert(postExprList.get(i)) + ")");
            }  
        }
        return tmp.toString();
        
    }

    private String mlForPre(){
        int size = this.preExprList.size();
        //simplify (wp prog1 post1) [("operon",1);("mucuB",1)] [];;
        StringBuffer tmp = new StringBuffer(" simplify (wp prog post) [");
        if(size > 0){
            String details = "";
            for (HoareExpr expr : preExprList) {
                details += "( \"" + expr.getId() + "\", " + expr.getConstant() + ");";
            }
            details = details.substring(0, details.length() - 1);
            tmp.append(details);
        }

        tmp.append("] []");
        return tmp.toString();
    }

    private String mlForExpr(HoareExpr expr){
        return "Rel(" + HoareBlocks.FOL_SYMBOL.get(expr.getOperation())
                      + ", ExprVar \"" + expr.getId() + "\""
                      + ", ExprConst " + expr.getConstant()
                      + ")";
    }


    public void generateTripletOCaml(FileOutputStream outstream) {
        try {
            outstream.write(toTripletOcaml().getBytes());
            System.out.println(" *** Success - Hoare input generated *** ");

        } catch (Exception e) {
            System.out.println(e);
            System.exit(2);
        }
    }
}