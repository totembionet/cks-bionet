#MyVisitor.java
La classe MyVisitor étend la classe RRGBaseVisitor automatiquement générée
par antler après compilation de la grammaire.


## @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]


##Table des données
```typescript
[{blocktype : "/*Block SMBname */", value: {/*Blocks below*/}}, {}]


ENV_VAR =   {
               count: Integer,
               data: [{
                        SMBid: String,
                        val: Integer
                      }]
            };
  


    VAR =   {
                count: Integer,
                 data: [{
                          SMBid: String,
                          min: Number,
                          max: Number
                        }]
            };
   
    
    
    enum OPERATEUR {'&', '|', '!'}
    REG_EXPR = {
                    var: String, //gene
                    seuil: Number
                }
                //[var, seuil]
    interface OPERATION {
                    operateur: OPERATEUR,
                    expr1: OPERATION | REG_EXPR,
                    expr2: OPERATION | REG_EXPR
                }
    REG =   [{  
                cible : String, //gene
                regs: [{
                    SMBid: String, //reg SMBname
                    formula_string: String,
                    formula_tree: OPERATION
                }] //Toutes les regulations de la cible
            }];
                      

    PARA = [{
             gene: String,
             paras: [{
                      SMBid: String, //K_gene+regs => Kg_r
                      min: Number,
                      max: Number //Min == max si pas d'intervalle
                    }]
          }];
   
    
    
    CTL =  {
             count: Integer,
             properties: [{
                      type: String, //bool-expr, ctl_prop, ctl_prop_until
                      operateur: String,
                      expr1: String,
                      expr2: String,
                    }]
          };
          

          
```

Erreurs 
Check to do lists
