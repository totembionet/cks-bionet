package front.smbParser;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class ParaBlock {

    private List<Param> paras;
    private Set<String> genes;

  public ParaBlock(){
      this.paras = new ArrayList<Param>();
      this.genes = new HashSet<String>();
  }

  public void addPara(String gene, String id,String finalId, List<Regul> regs, Integer valMin, Integer valMax){
      this.genes.add(gene);
      this.paras.add(new Param(gene, id,finalId, regs, valMin, valMax));
  }

  public String toJSON() {
      String paraBlock = "{\"blocktype\" : \"para\",\n \"value\" : [";
      for(String gene: genes){
          paraBlock += "{\"gene\" : \"" + gene + "\",\n \"paras\" : [";
          List<Param> geneParaS = paras.stream().filter(para -> para.getGene().contains(gene)).collect(Collectors.toList());
          for(Param para : geneParaS){
          paraBlock += "{ \"id\": \"" + para.getSMBid() +
                      "\",\n \"min\" : " + para.getValMin() +
                      ",\n \"max\" : " + para.getValMax() + "},";
          }
          if(geneParaS.size()>0)
            paraBlock = paraBlock.substring(0, paraBlock.length() - 1);
          paraBlock += "]},";
      }
      if(genes.size()>0)
        paraBlock = paraBlock.substring(0, paraBlock.length() - 1);
      paraBlock += "]}";
      return paraBlock;
  }

//    public List<Para> getBackFormat() throws  Exception{
//        List<Para> paraList = new ArrayList<>();
//        for(Param param : this.paras){
//            paraList.add(param.toBack());
//        }
//        return paraList;
//    }

    public List<Param> getParas() { return paras;}

}