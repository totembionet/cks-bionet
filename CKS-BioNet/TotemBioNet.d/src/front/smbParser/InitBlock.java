package front.smbParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to represent the initial conditions of the FSM (i.e. INIT block in NuSMV)
 * @author helen
 *
 * TODO: conditions are String that are directly written in the .smv file
 *       if one wants to do symbolic operations, need to store back.Formula instead of String
 *
 * The only verification that has been done is that atoms contains known variables
 * Satisfiability of the init constraints are delegated to NuSMV => warning in back.Search
 */

public class InitBlock {
    List<String> initConditions;

    public InitBlock() {
        initConditions = new ArrayList<String>();
    }

    public boolean isEmpty() {
        return initConditions.isEmpty();
    }

    public void add(String exp) {
        initConditions.add(exp);
    }
}
