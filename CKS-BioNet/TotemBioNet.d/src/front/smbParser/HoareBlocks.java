package front.smbParser;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to represent the sets of Hoare blocks
 *
 * Also contains methods for generating OCAML of BRN sine it is the same for all triples
 */
public class HoareBlocks {

    List<OneHoareBlock> blocks;
    static final HashMap<String, String> FOL_SYMBOL = new HashMap<String, String>()
    {{
        put("!", "Neg");
        put("&", "And");
        put("|", "Or");
        put("=>", "Impl");
        put("+", "Incr");
        put("-", "Decr");
        put(">", "GT");
        put("<", "LT");
        put(">=", "GE");
        put("<=", "LE");
        put("=", "Eq");
        put("!=", "NEq");
    }};
    boolean allVarsAreInPre;
    int currentId;

    public HoareBlocks(){
        this.allVarsAreInPre=true;
        this.blocks = new ArrayList<OneHoareBlock>();
        this.currentId=1;
    }

    public void add(OneHoareBlock block){
        blocks.add(block);
    }

    public ArrayList<OneHoareBlock> getBlocks() {
        return (ArrayList<OneHoareBlock>) blocks;
    }

    public String newId() {
        return "" + currentId++;
    }

    public String toBRNOcaml(List<Var> vars, List<Regul> regs){
        String brnVarBlock = "let vars = \n[";
        List<Var> targets = vars.stream().filter(el -> el.getVarType() == VarType.VAR).collect(Collectors.toList());
        for(Var target: targets){
            brnVarBlock += "(\"" + target.getId() + "\" ,( " + target.getMax() + ", [ " ;
            List<Regul> targetRegS = regs.stream().filter(reg -> reg.getTargets().contains(target.getId())).collect(Collectors.toList());
            for(Regul reg: targetRegS){
                brnVarBlock += "\"" + reg.getId() + "\" ;";
            }
            brnVarBlock = brnVarBlock.substring(0, brnVarBlock.length() - 1);
            brnVarBlock += "]));\n";
        }
        if(targets.size()>0)
            brnVarBlock = brnVarBlock.substring(0, brnVarBlock.length() - 2);
        brnVarBlock += "];; \n\n";

        String brnMultsBlock = "let mults = \n[";
        for(Regul reg: regs){
            brnMultsBlock += " (\"" + reg.getId() + "\", " ;
            brnMultsBlock += mlForBRN(reg.getFormulaTree());
            brnMultsBlock += " );\n";
        }
        if(regs.size()>0)
            brnMultsBlock = brnMultsBlock.substring(0, brnMultsBlock.length() - 2);
        brnMultsBlock += "];; \n";

        return brnVarBlock + brnMultsBlock;
    }

    private String mlForBRN(RegOp root){

        String mlFormula = "";

        if(root.getType() == RegType.ATOME) {
            mlFormula += "MAtom(\"" + root.getVar().getId() + "\", "+ root.getSeuil() + " )";
        }else{
            if(root.getExpr2()==null){
                mlFormula += "MPropUn(" + this.FOL_SYMBOL.get(root.getOperateur()) +
                        ", " + mlForBRN(root.getExpr1()) + " )";

            }else{
                mlFormula += "MPropBin(" + this.FOL_SYMBOL.get(root.getOperateur()) +
                        ", " + mlForBRN(root.getExpr1()) + ", " + mlForBRN(root.getExpr2()) + " )";
            }
        }

        return mlFormula;
    }

    public void generateBRNOCaml(FileOutputStream outstream,List<Var> vars, List<Regul> regs ) {
        try {
            String s = this.toBRNOcaml(vars,regs);
            outstream.write(s.getBytes());
            System.out.println(" *** Success - BRN generated *** ");
        } catch (Exception e) {
            System.out.println(e);
            System.exit(2);
        }
    }

    public boolean isEmpty() {
        return blocks.isEmpty();
    }

    public String toString() {
        return blocks.toString();
    }
}
