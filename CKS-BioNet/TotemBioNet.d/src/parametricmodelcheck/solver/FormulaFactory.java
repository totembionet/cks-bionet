package parametricmodelcheck.solver;

import back.logic.Formula;

import java.util.ArrayList;

/**
 * Class to handle only one instance of a formula.
 * Useful to share subformulas
 *
 * @author Helene Collavizza
 *
 */
public class FormulaFactory {

    private static final ArrayList<Formula> FORMULA_SET=new ArrayList<Formula>();

    public static int ID=1;

    // if f already exist, it is labeled with the label in FORMULA_SET, it is not added => return true
    // otherwise, is of f is a new id, f is added to FORMULA_SET => return true
    public static Formula addFormula(Formula f) {
        for (Formula fs : FORMULA_SET) {
            if (fs.toString().equals(f.toString())) {
                f.setId(fs.id);
                return fs;
            }
        }
        f.setId(ID++);
        FORMULA_SET.add(f);
        return null;
    }

    public static Formula getFormula(int id) {
        for (Formula f : FORMULA_SET) {
            if (f.id==id)
                return f;
        }
        return null;
    }

    public static Formula getFormula(Formula f) {
        for (Formula fs : FORMULA_SET) {
            if (fs.toString().equals(f.toString())) {
                return fs;
            }
        }
        return null;
    }

}
