package parametricmodelcheck.solver;

public class Main {

    public static void main(String[] args) {
//        Random rand = new Random();
//        // variables
//        Variable[] vars = new Variable[10];
//        for (int i=0;i<10;i++) {
//            byte max = (byte)(rand.nextInt(5)+1);
//            vars[i] = new Variable("K_"+i, (byte) 0,max);
//        }
//
//        System.out.println(Arrays.toString(vars));
//
//        // atomes
//        Atom[] atoms = new Atom[10];
//        for (int i=0;i<10;i++) {
//            int op = rand.nextInt(6);
//            byte val = (byte)rand.nextInt(vars[i].max);
//            atoms[i]=(op==0)?AtomFactory.makeEqual(vars[i],val):
//                    (op==1|op==2|op==3)?AtomFactory.makeLess(vars[i],val):AtomFactory.makeGreat(vars[i],val);
//        }
//
//        System.out.println(Arrays.toString(atoms));

        // contraintes à la main
        byte x = VariableFactory.createVar("x",(byte)0,(byte)2);
        byte y = VariableFactory.createVar("y",(byte)0,(byte)1);
        byte yy = VariableFactory.createVar("yy",(byte)0,(byte)2);
        byte z = VariableFactory.createVar("z",(byte)0,(byte)3);


        Atom a = AtomFactory.makeEqual(yy,(byte)2); // yy=2
        Atom b = AtomFactory.makeGreat(y,(byte)0); // y>0
        Atom c = AtomFactory.makeEqual(x,(byte)0); // x=0
        Atom d = AtomFactory.makeLess(yy,(byte)1); // yy<1 ==> transformé en yy=0
        Atom e = AtomFactory.makeLess(z,(byte)3); // z<3
        Atom f = AtomFactory.makeEqual(z,(byte)1); // z=1
        Atom g = AtomFactory.makeLess(z,(byte)2); // z<2
//        Atom neq = AtomFactory.makeNotEqual(z,(byte)2); // z/=2
//       Atom neq3 = AtomFactory.makeNotEqual(z,(byte)3); // z/=3

        Disjunction dt = Disjunction.TRUE;
        Conjunction ct = Conjunction.TRUE;
        Disjunction di = Disjunction.INCONSISTENT;
        Conjunction ci = Conjunction.INCONSISTENT;


        System.out.println("a " + a + " b " + b + " c " + c + " d " + d + " e " + e + " f " + f + " g " + g);

        Conjunction ce = new Conjunction(e);
        Conjunction cge = ce.andWithAtomClone(c);
        Conjunction cf = new Conjunction(f);

        System.out.println("ce " + ce + " cge " + cge); //[and:[[4]z<3]]
//        System.out.println("uncompatible 1" + ce.uncompatible(neq));
//         System.out.println("uncompatible 2" + eq.uncompatible(neq));
//         System.out.println("uncompatible 3" + cge.uncompatible(neq));
//        System.out.println("uncompatible 4" + cf.uncompatible(neq));

        Conjunction gce = ce.andWithAtomClone(g);
        gce = ce.makeAndConjunctionWithClone(cge);
        System.out.println(gce); // [and:[[2]x=0, [4]z<3]]
//        Conjunction gcen = gce.andWithAtomClone(neq); //[and:[[2]x=0, [4]z<3, [7]z/=2]]
//        System.out.println(gcen.makeNegation());
//        System.out.println(gcen.andWithAtomClone(neq3));


//        System.out.println("makeAndAtom clone" + ce.makeAndAtomWithClone(g));
//        System.out.println("ce " + ce);
//        System.out.println("makeAndAtom " + ce.makeAndAtom(g));
//        System.out.println("ce " + ce);
//        System.out.println("makeAndConj clone true" + ce.makeAndConjunctionWithClone(ct));
//        System.out.println("makeAndConj clone inconsistent" + ce.makeAndConjunctionWithClone(ci));
        Disjunction dce = new Disjunction(ce);
//        System.out.println("dce " + dce);
//        System.out.println("makeAndDisj clone true" + dce.makeAndDisjunctionWithClone(dt));
//        System.out.println("makeAndConj clone inconsistent" + dce.makeAndDisjunctionWithClone(di));



//        Conjunction cegc = ce.makeAndConjunctionWithClone(cg);
//        System.out.println("cegc clone " + cegc);//cegc clone [and:[[6]z<2]]
//        System.out.println("ce " + ce);//ce [and:[[4]z<3]]
//        Disjunction deg =  new Disjunction(cegc);
//        System.out.println("deg " + deg);
////        Disjunction dtrue = new Disjunction(Conjunction.TRUE);
//        Disjunction dtrue = Disjunction.TRUE;
//        System.out.println("dtrue " + dtrue);
//        Disjunction degTrue = deg.makeOrDisjunctionWithClone(dtrue);
//        System.out.println("degtrue " + degTrue + " deg " + deg);
//        System.out.println("conj with true " + deg.makeAndConjunctionWithClone(ct) + " deg " + deg);
////        System.out.println("neg degtrue " + degTrue.makeNegation());
//        Conjunction ca = new Conjunction(a);
//        Conjunction cega = cegc.makeAndConjunctionWithClone(ca);
//        System.out.println("cega " + cega);//cega [and:[[0]yy=2, [6]z<2]]
//        System.out.println("true cega " + degTrue.makeAndConjunctionWithClone(cega));//true cega [or:[and:[[0]yy=2, [6]z<2]]]
//        System.out.println("d " + d);
//        System.out.println("true and d "+ degTrue.makeAndAtomWithClone(d));//true and d [or:[and:[[3]yy=0]]]
//        System.out.println(degTrue.makeOrAtom(d));
//        System.out.println(degTrue);
//
//        Conjunction cb = new Conjunction(b);
//        Conjunction cba = cb.makeAndAtomWithClone(a);
//        System.out.println("cba " + cba + " cega " + cega + " uncompatible " + cba.uncompatible(cega)); // false
//        Disjunction dis = new Disjunction(cba);
//        System.out.println("dis 1 " + dis);//dis 1 [or:[and:[[0]yy=2, [1]y=1]]]
//        dis = dis.makeAndDisjunctionWithClone(degTrue);
//        System.out.println("dis 2" + dis);//dis 2[or:[and:[[0]yy=2, [1]y=1]]]
//        Disjunction nd = dis.makeNegation();
//        System.out.println("nd " + nd);//nd [or:[and:[[7]yy<2]],[and:[[8]y=0]]]
//        System.out.println(nd.makeAndDisjunctionWithClone(dis));// [or: INCONSISTENT]
//        System.out.println("not nd " + nd.makeNegation());// [or:[and:[[0]yy=2, [1]y=1]]]
//

//        Conjunction cf = new Conjunction(f);
//        System.out.println("cf " + cf + " cegc " + cegc); //cf [and:[[5]z=1]] cegc [and:[[6]z<2]]
//        System.out.println("cf and cegc " + cf.makeAndConjunctionWithClone(cegc)); // cf and cegc [and:[[5]z=1]]
//        System.out.println("ceg and cf " + cegc.makeAndConjunctionWithClone(cf)); // ceg and cf [and:[[5]z=1]]
//        Conjunction cfega = cf.makeAndConjunctionWithClone(cega);
//        System.out.println("cega " + cega);
//        System.out.println("cf and cega " + cfega);//cf and cega [and:[[0]yy=2, [5]z=1]]
//        System.out.println("cega and cf " + cega.makeAndConjunctionWithClone(cf));// cega and cf [and:[[0]yy=2, [5]z=1]]
//        System.out.println("cf and cega and e "  + cfega.makeAndConjunctionWithClone(ce));//cf and cega and e [and:[[0]yy=2, [5]z=1]]
//        Atom zz = AtomFactory.makeLess(z,(byte)1);
//        System.out.println("cfega and z <1 " + zz.uncompatible(cfega));//true
//        System.out.println("cfega and z <1 " + cfega.makeAndAtom(zz));//[ERROR]

//        Conjunction ca = new Conjunction(a);
//        System.out.println("ca " + ca);
////        System.out.println("ca " + ca + " start " + ca.areaStart + " size " + ca.areaSize);
////        System.out.println(GlobalVariableStore.toString(ca.areaStart,ca.areaSize));
////        System.out.println("uncompatible " + GlobalVariableStore.uncompatible(d,ca.areaStart,ca.areaSize));
//        ca = ca.makeAndAtom(a);
//        System.out.println("caa " + ca);
//        Conjunction cd = new Conjunction(d);
//        cd = ca.makeAndConjunction(cd);
//        System.out.println("cd " + cd);
//
//        Conjunction cb = new Conjunction(b);
//        Conjunction cba = cb.makeAndAtom(a);
//        System.out.println("b and a "+ cba );
//        Conjunction cbab = cba.makeAndAtom(b);
////        System.out.println("bef " + GlobalVariableStore.toString(cbab.areaStart, cbab.areaSize));
//        System.out.println("(b and a) and b " + cbab);
////        System.out.println("after " + GlobalVariableStore.toString(cbab.areaStart, cbab.areaSize));
////        int start = GlobalVariableStore.clone(cbab.areaStart, cbab.areaSize);
////        System.out.println("after clone " + GlobalVariableStore.toString(start, cbab.areaSize));
//        Conjunction cbac = cba.makeAndAtom(c);
//        System.out.println("(b and a) and c " + cbac);
//
//        Conjunction bac = cba.makeAndAtom(c);
//        Conjunction ce = bac.makeAndAtom(e);
//        System.out.println("((b and a) and c) and e " + ce);
//        Conjunction baccbac = bac.makeAndConjunction(cbac);
//        System.out.println("bac and cbac " + baccbac);
//        Conjunction cee = ca.makeAndAtom(f);
//
//        Disjunction dis = new Disjunction(ca);
//        System.out.println("dis 1 " + dis);
//        dis = dis.makeOrConjunction(bac);
//        System.out.println("dis 2" + dis);
//        Disjunction nd = dis.makeNegation();
//        System.out.println("nd " + nd);
//        System.out.println(nd.makeAndDisjunction(dis));
//
//        System.out.println(ce + " " + cd + " " + ce.getAtoms().isEqual(cd.getAtoms()));
//        System.out.println(ce + " " + cbac + " " + ce.getAtoms().isEqual(cbac.getAtoms()));
//        Conjunction ccc = cee.makeAndConjunction(cb);
//        cee.addAndAtom(c);
//        System.out.println(ccc + " " + cee + " " + ccc.getAtoms().isEqual(cee.getAtoms()));
//
//        System.out.println(ccc + " " + cee + " " + ccc.equals(cee));

//        Conjunction tr = Conjunction.TRUE;
//        System.out.println(tr.isTrue() + " " + tr.isNull());

  //      [or:[and:[[1]y>0],[and:[[0]x=0, [2]yy=0],

//        co = co.makeAndAtom(c);
//        System.out.println(co);
//        Atom d = AtomFactory.makeGreatEq(t,(byte)1);
//        Disjunction dis = new Disjunction(d);
//        dis = dis.makeAndConjunction(co);
//        dis = dis.makeOrDisjunction(new Disjunction(d));
//        System.out.println(dis);
//        System.out.println(dis.makeNegation());


//        [or:[and:[[1]y>0], vars:[y[1:1]]],[and:[[0]x=0, [2]y=0], vars:[x[0:0], y[0:0]]]]


        // contraintes
//        Conjunction c = new Conjunction();
//        c.addAndAtom(atoms[2]);
//        Conjunction c2 =  c.makeAndAtom(atoms[5]);
//        Conjunction c3 = c2.makeAndAtom(atoms[1]);
//        System.out.println(c +  " " + c2 + " " +c3);
//        Disjunction d = new Disjunction();
//        Disjunction dd = d.makeAndAtom(atoms[3]);
//        System.out.println(dd);
//        Disjunction dd1 = d.makeAndConjunction(c);
//        Conjunction cc2 = new Conjunction(atoms[0]);
//        Conjunction cc3 = c2.makeAndAtom(atoms[4]);
//        Disjunction ddd =d.makeOrConjunction(cc3);
//        ddd=ddd.makeAndAtom(AtomFactory.makeEqual(vars[6], (byte) 1));
//        System.out.println(ddd);
////        Disjunction d2 = new Disjunction();
////        d2.makeAndAtom(atoms[6]);
//////        d2.makeOrAtom(atoms[7]);
////        System.out.println(d2);
//        System.out.println(ddd.makeNegation());
////        d.makeOrDisjunction(d2);
////        System.out.println(d);
////        d.makeAndDisjunction(d2);
//        System.out.println(d);

//        System.out.println(d.toYices());
//        System.out.println(d2.toYices());
    }
}
