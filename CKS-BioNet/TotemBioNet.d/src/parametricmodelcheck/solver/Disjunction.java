package parametricmodelcheck.solver;


import parametricmodelcheck.mdd.MDDSolver;
import util.jclock.Clock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

/**
 * to represent disjunctions in DNS form
 *
 * this is a list of conjunctions
 *
 * @author Helene Collavizza
 */
public class Disjunction {

//    // disjunction used during the initialization phase for EG
//    // states labeled with child are labeled with (EG(child),INIT_EG)
//    // it corresponds to a label temporary true, which is removed if
//    public final static Disjunction EG_CST_INIT = new Disjunction();

    // disjunction to represent true
    public final static Disjunction TRUE = new Disjunction();

    // disjunction to represent an inconsistent disjunction
    public final static Disjunction INCONSISTENT = new Disjunction();

    // list of conjunctions
    ArrayList<Conjunction> conjunct;

    public static long product_time = 0;
    public static long product_nb = 0;

    public Disjunction() {
        conjunct = new ArrayList<>();
    }

    public Disjunction(Atom a) {
        this();
        conjunct.add(new Conjunction(a));
    }

    private Disjunction(ArrayList<Conjunction> c) {
        conjunct = c;
    }

    public Disjunction(Conjunction c) {
        this();
        conjunct.add(c);
    }

    // create the disjunction which contains the conjunct in conjuncts.
    // return the constant TRUE or INCONSISTENT when needed
    // this method must be used each time a new disjunction is built
    //    starting from an empty disjunstion and adding conjunctions in it
    private Disjunction buildDisjunction(ArrayList<Conjunction> conjuncts) {
        if (conjuncts.size() == 0)
            return INCONSISTENT;
        for (Conjunction c : conjuncts) {
            if (c == Conjunction.INCONSISTENT)
                throw new RuntimeException("     should not happen INCONSISTENT");
            if (c == Conjunction.TRUE)
                throw new RuntimeException("     should not happen TRUE");
       }
        return new Disjunction(conjuncts);
    }

    // return a new disjunction which is the conjunction of this and the simple atom a
    // the atom ais distributed on each conjunction of this
    public Disjunction andWithAtomClone(Atom a) {
        // false & x = false
        if (a.isInconsistent() || this == INCONSISTENT)
            return INCONSISTENT;
        // true & x = x
        if (a.isTrue())
            return (Disjunction) this.clone();
        // true & x = x, empty & x = x
        if (this == TRUE || this.isEmpty())
            return new Disjunction(a);

        // distribute a on each conjunction in this
        ArrayList<Conjunction> resConj = new ArrayList<>();
        for (Conjunction conj : conjunct) {
//          TODO: test if cc is TRUE (if more simplifications are added in filterAndAdd in conjunction)
            Conjunction cc = conj.andWithAtomClone(a);
            if (cc == Conjunction.TRUE)
                throw new RuntimeException("TRUE in Disjunction makeAndAtomWithClone");
            if (!cc.isInconsistent() & !cc.isEmpty() & !resConj.contains(cc))
                resConj.add(cc);
        }
        return buildDisjunction(resConj);
    }

    // add a conjunction into the disjunction
    // only a distribution of conj over this
    public Disjunction andWithConjunctionClone(Conjunction conj) {
        // false & x = false
        if (conj.isInconsistent() || this == INCONSISTENT)
            return INCONSISTENT;
        // true & x = x, empty & x = x
        if (conj.isTrue() || conj.isEmpty())
            return (Disjunction) this.clone();
        // true & x = x, empty & x = x
        if (this.isTrue() || this.isEmpty())
            return new Disjunction((Conjunction) conj.clone());
        // distribute conj on each conjunction in conjunct
        ArrayList<Conjunction> resConj = new ArrayList<Conjunction>();
        for (Conjunction c : conjunct) {
            Conjunction cand = c.makeAndConjunctionWithClone(conj);
            if (cand == Conjunction.TRUE)
                throw new RuntimeException("TRUE in Disjunction makeAndConjunctionWithClone");

//          TODO: test if cand is TRUE (if more simplifications are added in filterAndAdd in conjunction)
             if (!cand.isInconsistent() && !cand.isEmpty() && !resConj.contains(cand)) {
                resConj.add(cand);
            }
        }
        return buildDisjunction(resConj);
    }


    // add the disjunction disj to this disjonction
    // clone the conjunct in disj but do not clone the conjunct in this
    public Disjunction orWithDisjunction(Disjunction disj) {
        // TRUE or x = TRUE
        if (disj.isTrue() || this.isTrue())
            return TRUE;
        // if disj is inconsistent, don't use it
        // FALSE or x = x
        if (disj.isInconsistent()) {
            return this;
        }
        // if this is empty, return disj
        if (this.isEmpty())
            return disj;
        // clone and add the conjunctions in disj which are useful
        for (Conjunction c : disj.conjunct) {
            // if one conjunct is TRUE, the result is true
            if (c.isTrue())
                throw new RuntimeException("    Should not happen Disjunction.makeOrDisjunction: true conjunction");
            // add useful conjunct
            if (c.isInconsistent())
                throw new RuntimeException("    Should not happen Disjunction.makeOrDisjunction: inconsistent conjunction " + disj);
            if (!this.conjunct.contains(c)) {
                conjunct.add(c);
            }
        }
        // here, we started with this not empty, nor inconsistent nor true and added useful
        // conjuncts => return this
        return this;
    }

    // add the disjunction disj to this disjonction
    public Disjunction orWithDisjunctionClone(Disjunction disj) {
        // TRUE or x = TRUE
        if (disj.isTrue() || this.isTrue())
            return TRUE;
        // if disj is inconsistent, don't use it
        // FALSE or x = x
        if (disj.isInconsistent()) {
            return (Disjunction) this.clone();
        }
        // if true or empty, return the clone of disj
        if (this.isEmpty())
            return new Disjunction((Conjunction) disj.clone());
        // clone the conjunct in this
        ArrayList<Conjunction> resConj = cloneConjunct(conjunct);
        // clone and add the conjunctions in disj which are useful
        for (Conjunction c : disj.conjunct) {
            // if one conjunct is TRUE, the result is true
            if (c.isTrue())
                throw new RuntimeException("    Should not happen Disjunction.makeOrDisjunction: true conjunction");
            // add useful conjunct
            if (c.isInconsistent())
                throw new RuntimeException("    Should not happen Disjunction.makeOrDisjunction: inconsistent conjunction");
            if (!this.conjunct.contains(c)) {
                resConj.add((Conjunction) c.clone()

                );
            }
        }

        return buildDisjunction((resConj));
    }

    // to add an atom
    // useful to negate a conjunction
    public Disjunction orWithAtom(Atom a) {
        // if this or a if TRUE, return TRUE
        //TRUE or x = TRUE
        if (this == TRUE | a == AtomFactory.TRUE)
            return TRUE;

        // if a is impossible, don't use it
        // FALSE or x = x
        if (a.isInconsistent()) {
            return this;
        }
        // else add the atom as a conjunction
        conjunct.add(new Conjunction(a));
        return this;
    }

    public boolean containsTrue() {
        for (Conjunction c : conjunct) {
            if (c.isTrue())
                return true;
        }
        return false;
    }

    // this is the development of disj on this
    public Disjunction andWithDisjunctionClone(Disjunction disj) {
//		System.out.println("makeAndDisjunction bef " + this.conjunct + " " + disj.conjunct + this.isTrue() + disj.isTrue());
        // false & x = false
        if (this.isInconsistent())
            return INCONSISTENT;
        // false & x = false
        if (disj.isInconsistent())
            return INCONSISTENT;
        // true & x = x, empty & x = x
        if (this.isTrue() || this.isEmpty())
            return (Disjunction) disj.clone();
        // true & x = x, empty & x = x
        if (disj.isTrue() || disj.isEmpty())
            return (Disjunction) this.clone();

        product_nb++;
        Disjunction res;
        int nbAtoms = this.nbAtoms() + disj.nbAtoms();
        long t1 = System.currentTimeMillis();
//        if (nbAtoms>1000) {
//            res = productWithMDD(disj);
//            //System.out.println("product MDD " + (System.currentTimeMillis() -t1) + "ms");
//        }
//        else {
        res = productWithDNF(disj);
        //System.out.println("product DNF " + (System.currentTimeMillis() -t1) + "ms");
//        }
        product_time += System.currentTimeMillis() - t1;
        return res;

    }

    // this is the development of disj on this
    public Disjunction productWithMDD(Disjunction disj) {
        ArrayList<Disjunction> thisDisj = new ArrayList<>();
        thisDisj.add(this);
        thisDisj.add(disj);
        MDDSolver mddSolver = new MDDSolver(usedVarsAndDomains());
        return mddSolver.getMDDDisjunctionProduct(this, disj);
    }


    // this is the development of disj on this
    public Disjunction productWithDNF(Disjunction disj) {
        // this and disj are not inconsistent nor true, make the development
        // this.c1 & disj.c1 | this.c1 & disj.c2 | ... | this.cn & disj.c1 | this.cn & disj.c2 ...|this.cn & disj.cp
        ArrayList<Conjunction> res = new ArrayList<>();
        for (Conjunction c1 : conjunct) {
            for (Conjunction c2 : disj.conjunct) {
                Conjunction resAnd = c1.makeAndConjunctionWithClone(c2);
               // if one of the product is true, the overall disjunction is also true
                if (resAnd.isTrue()) {
                   return TRUE;
                }
                // only add the product which are not inconsistent and not empty
                if (!resAnd.isInconsistent() && !resAnd.isEmpty()) {
                    Conjunction resAndClone = (Conjunction) resAnd.clone();
                    if (!res.contains(resAndClone))
                        res.add(resAndClone);
                }
            }
        }
        return buildDisjunction((res));
    }

    // negation
    public Disjunction negation() {
        if (this.isTrue())
            return INCONSISTENT;
        if (this.isEmpty()) {
            throw new RuntimeException("SHOULD NOT HAPPEN: Disjunction.makeNegation, this is empty");
        }
        // negationWithDNF uses andWithDisjunctionClone
        // andWithDisjunctionClone is the critical point and is the only one that must use MDDs
        long t1 = System.currentTimeMillis();
        Disjunction res;
//        if (this.nbAtoms()>1000) {
        res = negationWithMDD();
//            System.out.println("negation MDD " + (System.currentTimeMillis() -t1) + "ms");
//        }
//        else {
//            res = negationWithDNF();
//            System.out.println("negation DNF " + (System.currentTimeMillis() -t1) + "ms");
//        }
        return res;

    }

    private Disjunction negationWithMDD() {
        ArrayList<Disjunction> thisDisj = new ArrayList<>();
        thisDisj.add(this);
        MDDSolver mddSolver = new MDDSolver(usedVarsAndDomains());
//        System.out.println(mddSolver.mddBuilder.pathsToString(not));
        Disjunction res = mddSolver.getMDDNot(this);
//        System.out.println(mddSolver.mddBuilder.pathsToDisjunction(not));
        return res;
    }

    private Disjunction negationWithDNF() {
        // init the disjunction with the first negated Conjunction
        Disjunction res = new Disjunction();
        res = res.orWithDisjunction(conjunct.get(0).makeNegation());
        // make the and of the disjunctions obtained for each conjunction in conjunct
        for (int i = 1; i < conjunct.size(); i++) {
            Disjunction c = conjunct.get(i).makeNegation();
            res = res.andWithDisjunctionClone(c);
            if (res.isTrue() | res.isInconsistent())
                break;
        }
        return res;
    }

    // build a map name -> domains of the variables used in this disjunction
    public HashMap<String, int[]> usedVarsAndDomains() {
        HashMap<String, int[]> usedVarsAndDomains = new HashMap<>();
        HashSet<String> usedVars = this.varNames();
        for (String n : usedVars) {
            byte id = VariableFactory.getID(n);
            usedVarsAndDomains.put(n, new int[]{VariableFactory.getMin(id), VariableFactory.getMax(id)});
        }
        return usedVarsAndDomains;
    }

    // equal to constant INCONSISTENT
    public boolean isInconsistent() {
        return this == INCONSISTENT;
    }

    // equal to constant TRUE
    public boolean isTrue() {
        return this == TRUE;
    }

    // equal to a disjunction with only one conjunct equal to TRUE
    public boolean isTrueConjunct() {
        return conjunct.size() == 1 & conjunct.get(0) == Conjunction.TRUE;
    }

    public HashSet<String> varNames() {
        HashSet<String> varNames = new HashSet<>();
        for (Conjunction c : conjunct) {
            varNames.addAll(c.varNames());
        }
        return varNames;
    }

    ////////////////////////////////////////////////////
    ///// getters, setters and overrided methods
    ////////////////////////////////////////////////////

    public ArrayList<Conjunction> getConjunctions() {
        return conjunct;
    }

    public int nbAtoms() {
        int nb = 0;
        for (Conjunction c : conjunct) {
            nb += c.size;
        }
        return nb;
    }

    public HashSet<Byte> setOfVarId() {
        HashSet<Byte> vars = new HashSet<>();
        for (Conjunction c : conjunct) {
            vars.addAll(c.setOfVarId());
        }
        return vars;
    }

    public boolean containsAllVars(Disjunction newf) {
        HashSet<Byte> varsOld = setOfVarId();
        HashSet<Byte> varsNew = newf.setOfVarId();
        return varsOld.containsAll(varsNew);
    }


    public boolean isEmpty() {
        return this!=TRUE & this!=INCONSISTENT & conjunct.isEmpty();
    }

//    public boolean isInitEG() {
//        return this== EG_CST_INIT;
//    }

    public String toString() {
//        if (isInitEG())
//            return "[or: BOTTOM]";
        if (isTrue())
            return "[or: TRUE]";
        if (isInconsistent())
            return "[or: INCONSISTENT]";
        if (conjunct.isEmpty())
            return "[or:]";
        if (conjunct.size()==1)
            return "[or:" + conjunct.get(0)+ "]";
        StringBuilder res = new StringBuilder("[or:");
        int i=0;
        for (i=0;i<conjunct.size()-1;i++) {
            Conjunction c = conjunct.get(i);
            res.append(c +",");//+ "\n";
        }
        res.append(conjunct.get(i)+"]");
        return res.toString();
    }

    public String toYices() {
        if (isTrue())
            return "true";
        if (conjunct.size() == 0)
            return "";
        if (conjunct.size() == 1)
            return conjunct.get(0).toYices();

        StringBuilder res = new StringBuilder("");
        int i;
        if (conjunct.size() % 2 == 0) {
            res.append("(or " + conjunct.get(0).toYices() + " " + conjunct.get(1).toYices() + ")");
            i = 2;
        } else {
            res.append(conjunct.get(0).toYices());
            i = 1;
        }
        while (i < conjunct.size()) {
            res.append(conjunct.get(i).toYices() + ")");
            res.insert(0,"(or ");
            i++;
        }
        return res.toString();
    }


    static ArrayList<Conjunction> cloneConjunct(ArrayList<Conjunction> conj) {
        ArrayList<Conjunction> res = new ArrayList<>();
        for (Conjunction c : conj) {
            res.add((Conjunction) c.clone());
        }
        return res;
    }

    @Override
    public Object clone() {
        if (this==TRUE)
            return TRUE;
        if (this==INCONSISTENT)
            return INCONSISTENT;
        return new Disjunction((ArrayList<Conjunction>) conjunct.clone());
    }

    @Override
    // complexity : at most that.conjunct.size()*this.conjunct.size()
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disjunction that = (Disjunction) o;
        if (isEmpty())
            return that.isEmpty();
        if (isTrue())
            return that.isTrue();
        if (isInconsistent())
            return that.isInconsistent();
        // to be synctatically equals, this and that must have the same number of conjuncts
        if (conjunct.size()!= that.conjunct.size())
            return false;
        // if one conjunct is in this and not in that, their are not equals
        for (Conjunction c : that.conjunct) {
            if (!this.conjunct.contains(c))
                return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(conjunct);
    }
}
