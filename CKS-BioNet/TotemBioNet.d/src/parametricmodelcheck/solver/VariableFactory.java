package parametricmodelcheck.solver;

import parametricmodelcheck.modelcheck.YicesLauncher;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * class to store the variables
 *
 * NOTA on variable names:
 *    - SMB names are used in ParametricModelChecker, VariableFactory and CSVParaWriter
 *    - in Yices String the ":" of SMBNames are replace with "-"
 *
 *    @author Helene Collavizza
 */

public class VariableFactory {

    // associate an ID to a name
    private static final HashMap<String,Byte> VAR_ID =new HashMap<>();
    // gives the minimum and maximum of the domain of a variable (from its index)
    private static final HashMap<Byte,Byte[]> DOMAIN =new HashMap<>();

    private static byte ID=0;


    public static byte createVar(String name, byte min, byte max){
        if (VAR_ID.containsKey(name))
            return VAR_ID.get(name);
        VAR_ID.put(name,ID);
        Byte[] dom = {min,max};
        DOMAIN.put(ID,dom);
        ID++;
        return (byte) (ID-1);
    }

    public static byte getID(String name) {
        return VAR_ID.get(name);
    }

    public static String getName(byte id) {
        Set<Map.Entry<String,Byte>> set = VAR_ID.entrySet();
        for (Map.Entry<String,Byte> entry : set) {
            if (entry.getValue()==id)
                return entry.getKey();
        }
        return "UNKNOWN";
    }

    public static String getYicesName(byte id) {
        return YicesLauncher.toYicesIdent(getName(id));
    }

    public static Byte[] getDomain(byte id) {
        return DOMAIN.get(id);
    }

    public static byte getMin(byte id){
        return DOMAIN.get(id)[0];
    }

    public static byte getMax(byte id){
        return DOMAIN.get(id)[1];
    }

    public static String print() {
        String res = "Vars: " + VAR_ID;
        res+="\nDomains: [";
        Set<Map.Entry<Byte,Byte[]>> set = DOMAIN.entrySet();
        for (Map.Entry<Byte,Byte[]> entry : set){
            res+= entry.getKey() + ":" + entry.getValue()[0] + ".." + entry.getValue()[1] + " ";
        }
        res+="]";
        return res;
    }

    public static HashMap<String,Byte> getVariables() {
        return VAR_ID;
    }
}
