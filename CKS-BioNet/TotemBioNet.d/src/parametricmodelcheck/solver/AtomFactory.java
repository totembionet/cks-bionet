package parametricmodelcheck.solver;

import java.util.HashMap;

/**
 * class to store atoms and have only one reference to one atom
 *
 * Nota: during the CKS model-checking, the same atom may be found many times => use only one reference
 *
 * @author Helene Collavizza
 */

public class AtomFactory {

    public static final String EQUAL="=";
    public static final String LESS="<";
    public static final String GREAT=">";
    // not used operators
//    public static final String NEQ="/=";  // according to yices syntax
//    public static final String LESSEQ="<=";
//    public static final String GREATEQ=">=";

    // used when the constraint for this atom is always true according to the domain of the variable
    public static final Atom TRUE=new Atom((byte) -1, (byte)0,(byte)0,(byte) -1,"TRUE",-1);
    // used when the constraint for this atom is always flase according to the domain of the variable
    public static final Atom INCONSISTENT =new Atom((byte) -2, (byte)0,(byte)0,(byte) -1,"INCONSISTENT",-2);

    // the map of the atom
    private static final HashMap<String,Atom> ATOM_SET=new HashMap<>();

    public static int ID=0;

    //    idVar = value
    public static Atom makeEqual(byte idVar, byte value) {
        if (value < 0)
            throw new IllegalStateException("Unexpected negative value for atom ");
        // search if the atom already exists
        String key = makeKey(idVar, value, EQUAL);
        if (ATOM_SET.containsKey(key))
            return ATOM_SET.get(key);

        // if not, make it
        byte min = VariableFactory.getMin(idVar);
        byte max = VariableFactory.getMax(idVar);

        // atom is impossible according to the initial domain
        if (value > max | value < min)
            return INCONSISTENT;

        // this atom is always true according to the initial domain
        if (value == min & value == max) {
            return TRUE;
        }

        // store the atom with the right domain for its variable
        Atom a = new Atom(idVar, value, value, value, EQUAL, ID++);
        a.setIsBool();
        ATOM_SET.put(key, a);
        return a;
    }


    //  idVar < value
    public static Atom makeLess(byte idVar, byte value) {
//        System.out.println("less " + VariableFactory.getName(idVar) + " value " + value);
        if (value < 0)
            throw new IllegalStateException("Unexpected negative value for atom ");
        // search if the atom already exists
        String key = makeKey(idVar, value, LESS);
        if (ATOM_SET.containsKey(key))
            return ATOM_SET.get(key);

        byte min = VariableFactory.getMin(idVar);
        byte max = VariableFactory.getMax(idVar);

        // value<=[min,max] => impossible
        if (value <= min)
            return INCONSISTENT;
        // if domain is [v,v] and v<value => always true
        // possible when K is fixed
        if (min == max & min < value)
            return TRUE;
        if (value==min+1) {
            return makeEqual(idVar,min);
       }

        // general case
        Atom a = new Atom(idVar, min, (byte) (value - 1), value, LESS, ID++);
        a.setIsBool();
        ATOM_SET.put(key, a);
        return a;
    }


    public static Atom makeGreat(byte idVar, byte value) {
//        System.out.println("great " + VariableFactory.getName(idVar) + " value " + value);

        if (value < 0)
            throw new IllegalStateException("Unexpected negative value for atom ");

        // search if the atom already exists
        String key = makeKey(idVar,value,GREAT);
        if (ATOM_SET.containsKey(key))
            return ATOM_SET.get(key);
        byte min = VariableFactory.getMin(idVar);
        byte max = VariableFactory.getMax(idVar);

        // [min,max]<=value => impossible
        if (max <= value)
            return INCONSISTENT;
        // if domain is [v,v] and v>value => always true
        // possible when K is fixed
        if (min==max && value<max)
            return TRUE;

       // value<[value,value+1] => idVar must be equal to value+1
        if (value==max-1) {
            return makeEqual(idVar,max);
        }
        // general case
        Atom a = new Atom(idVar, (byte) (value + 1), max, value, GREAT, ID++);
        a.setIsBool();
        ATOM_SET.put(key,a);
        return a;
    }


    private static String makeKey(byte idVar,byte value,String op){
        return idVar+ "" + value+op;
    }

     //////////////////////////////////////
    // not used
    ///////////////////////////////////////


    //    //    idVar != value
//    // uses to take the negation of (idVar = value)
//    public static Atom makeNotEqual(byte idVar, byte value) {
//        if (value < 0)
//            throw new IllegalStateException("Unexpected negative value for atom ");
//        // search if the atom already exists
//        String key = makeKey(idVar, value, NEQ);
//        if (ATOM_SET.containsKey(key))
//            return ATOM_SET.get(key);
//
//        // if not, make it
//        byte min = VariableFactory.getMin(idVar);
//        byte max = VariableFactory.getMax(idVar);
//
//        // if v is outside the domain of idVar, NEQ is TRUE
//        if (value > max | value < min)
//            return TRUE;
//
//        // this atom is always true according to the initial domain
//        if (value == min & value == max) {
//            return INCONSISTENT;
//        }
//
//        // if value is on the bounds
//        if (value == min)
//            return makeGreat(idVar, value);
//        if (value == max)
//            return makeLess(idVar, value);
//
//        // store the atom with the right domain for its variable
//        Atom a = new Atom(idVar, (byte)-1, (byte)-1,value,  NEQ, ID++);
//        System.out.println("    $$$$$$ NEQ created " + a);
//        a.setIsBool();
//        ATOM_SET.put(key, a);
//        return a;
//    }

//    public static Atom makeLessEq(byte idVar, byte value) {
//        if (value < 0)
//            throw new IllegalStateException("Unexpected negative value for atom ");
//        String key = idVar+"" + value+LESSEQ;
//        if (ATOM_SET.containsKey(key))
//            return ATOM_SET.get(key);
//        byte min = VariableFactory.getMin(idVar);
//        byte max = VariableFactory.getMax(idVar);
//        Atom a;
//        if (value < min)
//            a= IMPOSSIBLE;
//        else if (min==max && min==value)
//            a=TRUE;
//        else
//            a = new Atom(idVar,min,value,value,LESSEQ, ID++);
////        System.out.println("filter " + v);
//        ATOM_SET.put(key,a);
//        return a;
//    }
//
//    public static Atom makeGreatEq(byte idVar, byte value) {
//        if (value  < 0)
//            throw new IllegalStateException("Unexpected negative value for atom ");;
//        String key = idVar+"" + value+GREATEQ;
//        if (ATOM_SET.containsKey(key))
//            return ATOM_SET.get(key);
//        byte max = VariableFactory.getMax(idVar);
//        byte min = VariableFactory.getMin(idVar);
//        Atom a;
//        if (value  > max)
//            a = IMPOSSIBLE;
//        else if (min==max && max==value)
//            a = TRUE;
//        else
//            a = new Atom(idVar,value,max,value,GREATEQ, ID++);
////        System.out.println("filter " + v);
//        ATOM_SET.put(key,a);
//        return a;
//    }

}
