package parametricmodelcheck.solver;

import util.jlist.OrderedArrayList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


/**
 * to represent conjunctions
 *
 * conjunctions are ordered list of atoms
 * or, and, not are applied to conjunctions
 *
 * @author Helene Collavizza
 */
public class Conjunction {

	//final static Conjunction EMPTY=new Conjunction();
	public final static Conjunction TRUE=new Conjunction(AtomFactory.TRUE);
	public final static Conjunction INCONSISTENT=new Conjunction(AtomFactory.INCONSISTENT);
	//final static Conjunction FALSE=new Conjunction(null,null);

	// list of atoms in the conjunction
	// [a1,a2,...,an] represents a1 & a2 & ... & an
	// atoms are ordered by they id
	OrderedArrayList<Atom> atoms;

	// number of atoms
	int size=0;

	////////////////////////////////////
	// constructors
	private Conjunction(OrderedArrayList<Atom> atoms){
		this.atoms = atoms;
		if (atoms!=null)
			this.size = atoms.size();
	}

	public Conjunction(Atom a){
		this();
		atoms.add(a);
		this.size=1;
	}

	public Conjunction(){
		this(new OrderedArrayList<>());
	}

	@Override
	public Object clone() {
		if (isTrue())
			return TRUE;
		if (isInconsistent())
			return INCONSISTENT;
		return new Conjunction((OrderedArrayList<Atom>)atoms.clone());
	}

	// create the conjunction which contains the atoms in atoms.
    // return the constant TRUE or INCONSISTENT when needed
    // this method must be used each time a new conjunction is built
    //    starting from an empty conjunction and adding atoms in it
	private Conjunction buildConjunction(OrderedArrayList<Atom> atoms){
		if (atoms.isEmpty())
			return INCONSISTENT;
		if (atoms.size()==1 & atoms.get(0)==AtomFactory.TRUE)
			return TRUE;
		if (atoms.size()==1 & atoms.get(0)==AtomFactory.INCONSISTENT)
			return INCONSISTENT;
		return new Conjunction(atoms);
	}

	// conjunction of this and atom a
	// if not uncompatible, modify the list of atoms
	// NOTA: used for selfloop. Start with an empty conjunction and modify it
	public Conjunction andWithAtom(Atom a) {
//		System.out.println("addAndAtom bef " + a + " " + this);
		// false & x = false
		if (a.isInconsistent()|this.isInconsistent())
			return INCONSISTENT;
		// true & x = x
		if (a.isTrue())
			return this;
		if (this.isTrue() || this.isEmpty())
			return new Conjunction(a);
		if (this.uncompatible(a))
			return INCONSISTENT;
		filterAndAdd(atoms, a);
		if (this.atoms.size()<=1)
			throw new RuntimeException("   Should not happen: atoms.size <= 1 in Conjunction.makeAndAtom " + atoms);
		return this;
	}

	// add atom a and reduce domain of variable in a accordingly
	public Conjunction andWithAtomClone(Atom a) {
		// false & x = false
		if (a.isInconsistent()||this.isInconsistent())
			return INCONSISTENT;
		// true & x = x
		if (a.isTrue())
			return (Conjunction) this.clone();
		if (this.isTrue() || this.isEmpty())
			return new Conjunction(a);
		if (this.uncompatible(a))
			return INCONSISTENT;

		//////////////
		OrderedArrayList<Atom> resAtoms = new OrderedArrayList<>();
		resAtoms.addAll(atoms);
		filterAndAdd(resAtoms,a);
		return buildConjunction(resAtoms);
	}

	// this and the atom are uncompatible if the intersection of one atom of this is uncompatible with the atom
	boolean uncompatible(Atom a) {
		for (Atom thisAtom : atoms)
			if (thisAtom.uncompatible(a))
				return true;
		return false;
	}


	// to filter the conjunction of atoms in theAtoms when adding an atom A
	//    remove redundant atoms
	//    PRECOND: A is compatible with all the atoms in this
	//    POST: this.conjunct has been modified
	// Filtering: only when thisAtom and A concern the same variable
	// if A = x<a
	//       if thisAtom is x<b and a<b => remove x<b
	//       if thisAtom is x=a-1 => don't add A
	// if A = x>a
	//       if thisAtom is x>b and b<a => remove x>b
	//       if thisAtom is x=a+1 => don't add A
	// if A = x=a
	//       remove all atoms which involve x (redundant)
	private void filterAndAdd(OrderedArrayList<Atom> theAtoms, Atom a) {
		Iterator<Atom> iter = theAtoms.iterator();
		boolean existEqual = false;
		boolean notFound=true;
		while (iter.hasNext()) {
			Atom thisAtom = iter.next();
			// thisAtom is interesting only if it has the same variable than a
			if (thisAtom.variableId == a.variableId) {
				notFound=false;
				// a is an equality, more interesting than an inequality
				if (a.isEqual()) {
					iter.remove();
				} else if (a.isLess()) {
					// remove because information in a more interesting
					if (thisAtom.isLess() && thisAtom.value > a.value) {
						iter.remove();
					}
					if (thisAtom.isEqual() && thisAtom.value < a.value)
						existEqual = true;

				} else if (a.isGreat()) {
					// remove because information in a more interesting
					if (thisAtom.isGreat() && thisAtom.value < a.value) {
//						System.out.println("remove great");
						iter.remove();
					}
					if (thisAtom.isEqual() && thisAtom.value > a.value)
						existEqual = true;
				}
			}
		}
		// if a is equal it more interesting than inequalities, add it
		if (notFound | a.isEqual())
			theAtoms.add(a);
		else
			// if existEqual, equal is more interesting so don't add a
			// otherwise, add it
			if (!existEqual)
				theAtoms.add(a);
	}



	// add the atoms of conj
	public Conjunction makeAndConjunctionWithClone(Conjunction conj) {
		if (this.isInconsistent() | conj.isInconsistent())
			return INCONSISTENT;
		if (conj.isTrue() || conj.isEmpty()) {
			return (Conjunction) this.clone();
		}
		if (this.isTrue() ||this.isEmpty() || this.equals(conj)) {
			return (Conjunction) conj.clone();
		}
		// check if domain of one variable of this is incompatible with
		// domain of conj
		if (this.uncompatible(conj)) {
			return INCONSISTENT;
		}
		// add the atoms
		OrderedArrayList<Atom> resAtoms = new OrderedArrayList<Atom>();
		resAtoms.addAll(atoms);
		for (Atom a : conj.atoms) {
			filterAndAdd(resAtoms,a);
		}
		return buildConjunction(resAtoms);
	}

	// this and conj are uncompatible if the intersection of one atom of this is uncompatible with one atom of that
	boolean uncompatible(Conjunction conj) {
		for (Atom thisAtom : atoms)
			if (thisAtom.uncompatible(conj))
				return true;
		return false;
	}


	// negation
	// make a disjunstion which contains each atom after negation
	// if the atom is (x = val) then add 2 atoms in the disjunction : (x<val) , (x>val)
	public Disjunction makeNegation() {
		if (isTrue()) {
			return Disjunction.INCONSISTENT;
		}
		if (isEmpty())
			throw new RuntimeException("  Should not happen : Conjunction.makeNegation EMPTY");
		Disjunction res= new Disjunction();
		for (Atom a : atoms) {
			if (a.operator!=AtomFactory.EQUAL)
				res = res.orWithAtom(a.negation());
				// here we add two atoms
			else {
				ArrayList<Atom> neq = makeNegEq(a);
				for (Atom n : neq) {
					res = res.orWithAtom(n);
				}
			}
		}
		return res;
	}

	// PRE: a is an equality (x = val)
	// return the 2 atoms : (x<val) , (x>val)
	private ArrayList<Atom> makeNegEq(Atom a) {
		// create a new variable with initial domain of the parameter
		byte min = VariableFactory.getMin(a.variableId);
		byte max = VariableFactory.getMax(a.variableId);
		ArrayList<Atom>  res = new ArrayList<>();
		if (a.value!=min) {
			res.add(AtomFactory.makeLess(a.variableId, a.value));
		}
		if (a.value!=max) {
			res.add(AtomFactory.makeGreat(a.variableId, a.value));
		}
		return res;
	}


	public boolean isInconsistent() {
		return this==INCONSISTENT ;
	}

	public boolean isTrue() {
		return this==TRUE;
	}

	public boolean isEmpty() {
		return atoms==null || atoms.isEmpty();
	}

	public HashSet<String> varNames(){
		HashSet<String> varNames = new HashSet();
		for (Atom a : atoms) {
			varNames.add(VariableFactory.getName(a.variableId));
		}
		return varNames;
	}

	public HashSet<Byte> setOfVarId() {
		HashSet<Byte> vars = new HashSet<>();
		for (Atom a : atoms){
			vars.add(a.variableId);
		}
		return vars;
	}

	public String toYices() {
		if (isTrue())
			return "true";
		return atomsToYices(atoms);
	}

	private String atomsToYices(ArrayList<Atom> atoms) {
		if (atoms.size()==1)
			return atoms.get(0).toYices();

		StringBuilder res = new StringBuilder("");
		int i;
		if (atoms.size()%2==0) {
			res.append("(and " + atoms.get(0).toYices() + " " + atoms.get(1).toYices() + ")");
			i=2;
		}
		else {
			res.append(atoms.get(0).toYices());
			i = 1;
		}
		while (i<atoms.size()) {
			res.append(atoms.get(i).toYices()+ ")");
			res.insert(0,"(and ") ;
			i++;
		}
		return res.toString();
	}


	public String toString() {
		if (this.isInconsistent())
			return "[INCONSISTENT]";
		if (this==TRUE) return "TRUE";
		if (this.isEmpty()) return "[]]";
		//if (this==FALSE) return "FALSE";
		return "[and:" + atoms + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Conjunction that = (Conjunction) o;
		if (this==that)
			return true;
		if (this.isEmpty())
			return that.isEmpty();
		if (that.isEmpty())
			return this.isEmpty();
		return atoms.isEqual(that.atoms);
		}

	public OrderedArrayList<Atom> getAtoms() {
		return atoms;
	}
}

