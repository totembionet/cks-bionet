package parametricmodelcheck.solver;

import java.util.Objects;

import static parametricmodelcheck.solver.AtomFactory.*;

/**
 * class to represent an atom of form (x op n) with x a Variable, op in =, <, >, n a value in domain of x
 *
 * PRECOND : the atom is consistent (i.e. no check between the atom expression and the variable domain)
 *
 * @author Helene Collavizza
 */
public class Atom implements Comparable{

    byte variableId;
    byte value;
    String operator;
    // domain associated with this atom after filtering according to operator
    byte min;
    byte max;
    int id; // from AtomFactory
    boolean isBool;


    public Atom(byte variableId,byte min, byte max,byte value,String operator,int id){
        this.variableId=variableId;
        this.value=value;
        this.operator=operator;
        this.id = id;
        this.min=min;
        this.max = max;
        // this doesn't work =>  java.lang.ExceptionInInitializerError
        // need to use setIsBool instead
//        this.isBool = VariableFactory.getMin(variableId)==0 & VariableFactory.getMax(variableId)==1;
    }

    private boolean isBoolDomain(byte min, byte max) {
        return min==0 & max==1;
    }

    void setIsBool() {
        isBool=VariableFactory.getMin(variableId)==0 & VariableFactory.getMax(variableId)==1;
    }

    Atom negation() {
        // create the atom which corresponds to the negation
        switch (operator){
            case GREAT: return AtomFactory.makeLess(variableId, (byte) (value+1));
            case LESS: return AtomFactory.makeGreat(variableId,(byte) (value-1));
            // EQUAL is never found because in Conjunction, ! (x=v) is translated as the disjunction of
            // (x<v) | (x>v)
//            case EQUAL: return AtomFactory.makeNotEqual(variableId,value);
//            case NEQ: return AtomFactory.makeEqual(variableId,value);
//            case GREATEQ: return AtomFactory.makeLess(variableId,value);
//            case LESSEQ: return AtomFactory.makeGreat(variableId,value);
            default: throw new IllegalStateException("Bad operator in Atom.negate()");
        }
    }

    // 2 atoms are uncompatibles if they use the same variable and the intersection of their domains is empty
    public boolean uncompatible(Atom that) {
        if (this.variableId!=that.variableId)
            return false;
        return this.min > that.max | that.min > this.max;
    }

    // this atom is uncompatible with the conjunction conj if one of the atoms of conj is uncompatible with this
    public boolean uncompatible(Conjunction conj) {
        for (Atom that : conj.atoms) {
            if (this.uncompatible(that))
                return true;
        }
        return false;
    }


    public String toYices() {
//        if (isBool)
//            return toYicesBool();
        return "(" + operator + " " + VariableFactory.getYicesName(variableId) + " " + value + ")";
    }

    private String toYicesBool() {
        String var= VariableFactory.getYicesName(variableId) ;
//        System.out.println(operator + " " +var + " val " + value);
        if (operator.equals(GREAT)) return "(= " + var + " true)";
        if (operator.equals(LESS))  return "(= " + var + " false)";
        if (operator.equals(EQUAL)) {
            String val = (value == 0) ? " false" : " true";
            return "(= " + var + val + ")";
        }
        throw new IllegalStateException("unknown atom operator");
    }

    public String toString() {
        if (this==TRUE)
            return "TRUE_ATOM";
         if (this==INCONSISTENT)
            return "FALSE_ATOM";
        return "["+id+"]"+name(variableId) + operator +value;
    }

    public boolean isInconsistent() { return this== INCONSISTENT; }

    public boolean isTrue() {
        return this==TRUE;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Atom atom = (Atom) o;
        return id == atom.id ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    // getters
    public String name(byte id) {
        return VariableFactory.getName(id);
    }

    public String nuSMVname(byte id) {
        String name = VariableFactory.getName(id);
        name = name.replace("_", "");
        name = name.replace(":","_");
        return name;
    }

    public String op() {return operator;}

    public boolean isEqual() {return operator.equals(AtomFactory.EQUAL);}
//    public boolean isNotEqual() {return operator.equals(AtomFactory.NEQ);}
    public boolean isLess() {return operator.equals(AtomFactory.LESS);}
    public boolean isGreat() {return operator.equals(AtomFactory.GREAT);}


    public byte value() {return value;}

    public byte varId() {
        return variableId;
    }

    @Override
    public int compareTo(Object o) {
        return id - ((Atom)o).id;
    }
}
