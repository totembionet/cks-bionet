package parametricmodelcheck.mdd;

import parametricmodelcheck.solver.Atom;
import parametricmodelcheck.solver.Conjunction;
import parametricmodelcheck.solver.Disjunction;
import run.Main;
import util.TotemBionetException;
import util.jclock.Clock;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static parametricmodelcheck.modelcheck.YicesLauncher.*;

/**
 * class to build MDDs associated with constraints (Disjunction and Conjunction) and get all the solutions
 *
 * @author Helene Collavizza
 */
public class MDDSolver {

    public static long MDD_BUILD_TIME = 0;
    public static long MDD_ENUMERATE_TIME = 0;

    // to build the MDD
    MDDBuilder mddBuilder;

    // usedVarsAndDomains are variable names and domains of variables to use
    // all variables domains contain at least 2 values
    public MDDSolver(HashMap<String,int[]> usedVarsAndDomains) {
        mddBuilder = new MDDBuilder(usedVarsAndDomains);
     }

     // test if oldd is semantically equal to newd or different or one included in the other
    public int testInclusion(Disjunction oldd, Disjunction newd) {
        int oldf = getMDDRootFromDisjunction(oldd);
        int newf = getMDDRootFromDisjunction(newd);
        int notNewf = mddBuilder.mddManager.not(newf);
        int notOldf = mddBuilder.mddManager.not(oldf);
        int conj1 = mddBuilder.getConjunction(new int[]{oldf, notNewf});
        int conj2 = mddBuilder.getConjunction(new int[]{notOldf, newf});
        boolean empty1 = mddBuilder.isEmptyMDD(conj1);
        boolean empty2 = mddBuilder.isEmptyMDD(conj2);
        // old and new are equals (new is in old and old is in new)
        if (empty1 & empty2)
            return NEW_EQUAL_OLD;
        if (empty1 & ! empty2)
            return OLD_IN_NEW;
        if (!empty1 & empty2)
            return NEW_IN_OLD;
        return NEW_DIFF_OLD;
    }


    // test if oldd is included in newd
    // used when newd has some variables which are not in oldd
    public boolean isIncluded(Disjunction oldd, Disjunction newd) {
        int oldf = getMDDRootFromDisjunction(oldd);
        int newf = getMDDRootFromDisjunction(newd);
        int notnewf = mddBuilder.mddManager.not(newf);
        int conj = mddBuilder.getConjunction(new int[]{notnewf, oldf});
        return mddBuilder.isEmptyMDD(conj);
    }


    // get the MDD which contains the solutions of disjunction disj
    public int getMDDRootFromDisjunction(Disjunction disj) {
        if (disj.isTrue())
            return -1;
        ArrayList<Integer> nodes=new ArrayList<>();
        for (Conjunction c : disj.getConjunctions()){
            int n= getMDDRootConjunction(c);
            if (n!=-1)
                nodes.add(n);
        }
        int[] nodesTab = new int[nodes.size()];
        int i=0;
        for (int n : nodes)
            nodesTab[i++]=n;
        if (i==0)
            return -1;
        return mddBuilder.getDisjunction(nodesTab);
    }


    // return the negation of disj
    public Disjunction getMDDNot(Disjunction disj) {
        int d = getMDDRootFromDisjunction(disj);
        return MDDRootToDisjunction(mddBuilder.mddManager.not(d));
    }

    // return the disjunction which correspond to the values in the MDD root
    // disjunction such that : (x1=v1 or x3 = v3 or ... or xn=vn)
    // NOTA: by convention, -1 is the root of the empty MDD => contains all values
    private Disjunction MDDRootToDisjunction(int root) {
        if (root == -1)
            return Disjunction.TRUE;
        return mddBuilder.pathsToDisjunction(root);
    }

       // get the MDD which contains the solutions of the conjunction c
    public int getMDDRootConjunction(Conjunction c) {
         if (c.isTrue())
               throw new RuntimeException("   *** Should not happen MDDSolver.getMDDRootConjunction. true Conjunction");
        ArrayList<Integer> nodes=new ArrayList<>();
        for (Atom a : c.getAtoms()) {
            if (a.isTrue())
                throw new RuntimeException("   *** Should not happen MDDSolver.getMDDRootConjunction. true or fixed atom " +a);
            nodes.add(mddBuilder.getNodeAtom(a));
        }
        int[] nodesTab = new int[nodes.size()];
        int i=0;
        for (int n : nodes)
            nodesTab[i++]=n;

        if (i==0)
            return -1;
        return mddBuilder.getConjunction(nodesTab);
    }


    // return the root of the MDD which is the conjunction of the contraints for all states
   private int getMDDRootSolution(ArrayList<Disjunction> constraints) {
         // ArrayList needed because only not 0 nodes can be managed in the AND
       int[] roots = getMDDRoots(constraints);
       // all K are models
       if (roots.length==0)
           return -1;
       System.out.println("\n   The set of useful MDDs for all states has been built ("+ roots.length + " MDDs). Building the intersection... ");
       return mddBuilder.getConjunction(roots);
    }

    // return the product of conjunctions d1 and d2
    public Disjunction getMDDDisjunctionProduct(Disjunction d1, Disjunction d2) {
        ArrayList<Disjunction> lesDisj = new ArrayList<>();
        lesDisj.add(d1);
        lesDisj.add(d2);
//        System.out.print("MDDDisjProd   ");
        int[] roots = getMDDRoots(lesDisj);
        int root;
        if (roots.length==0)
            // all values are solutions
            root = -1;
        else
            root =mddBuilder.getConjunction(roots);
        return MDDRootToDisjunction(root);
    }

    // return the roots of all disjunctions in lesDisj
    private int[] getMDDRoots(ArrayList<Disjunction> lesDisj) {
        // ArrayList needed because only not 0 nodes must be managed in the AND
        ArrayList<Integer> nodes=new ArrayList<>();
        // get the roots of the MDD of the disjunction of all states which are labeled
        // with ctl
        for  (Disjunction disj : lesDisj) {
            int n = getMDDRootFromDisjunction(disj);
            if (n != -1)
                nodes.add(n);
        }
        // make the corresponding array
        int[] anodes = new int[nodes.size()];
        int i=0;
        for (int n : nodes)
            anodes[i++]=n;
        return anodes;
    }

    // to print the solutions of a disjunction using MDDs
    public void printMDDSolutionOfDisjunction(Disjunction d) {
        System.out.println("Solution of disjunction " + d);
        System.out.println(mddBuilder.pathsToString(getMDDRootFromDisjunction(d)));
    }


    // to print the global solution
    public void printSolutions(ArrayList<Disjunction> constraints) {
        System.out.println("\nStarting MDD construction...");
        long t1 = System.currentTimeMillis();
        int root = getMDDRootSolution(constraints);
        long t2 = System.currentTimeMillis();
        MDD_BUILD_TIME=t2-t1;
        System.out.println("--- Time for MDD construction and intersection " + MDD_BUILD_TIME + "ms");
        if (root==-1)
            System.out.println("The property is valid for all parameter settings");
        else {
            System.out.println("\nSet of solutions:\n*********************");
//            System.out.println("Fixed PARAS : " + mddBuilder.oneValueVars);
//            mddBuilder.printDomains();
            t1 = System.currentTimeMillis();
            System.out.println(mddBuilder.pathsToString(root));
            t2 = System.currentTimeMillis();
            MDD_ENUMERATE_TIME=t2-t1;
            System.out.println("--- Time for MDD solutions enumeration " + MDD_ENUMERATE_TIME + "ms");
        }
        System.out.println("\n--- Total time for MDD " + Clock.print(MDD_BUILD_TIME+MDD_ENUMERATE_TIME));
    }


    // to print the global solution in a .csv file
    public void printSolutionsInCsv(HashMap<String,int[]> allParaDomains,ArrayList<Disjunction> constraints,HashSet<String> unUsedVars) throws IOException, TotemBionetException {
        int root;
        // no solution
        if (constraints == null)
            root = -2;
        else
            // all solutions
            if (constraints.size() == 0)
                root = -1;
            // any solution
            else {
                long t1 = System.currentTimeMillis();
                root = getMDDRootSolution(constraints);
                long t2 = System.currentTimeMillis();
                MDD_BUILD_TIME = t2 - t1;
                System.out.println("--- Time for MDD construction and intersection " + MDD_BUILD_TIME + "ms");
                if (root == -1) {
                    System.out.println("The property is valid for all parameter settings");
                }
            }
        String csvFile = Main.getPath() + "/" + Main.getInputRoot() + "-cks.csv";
        CSVParaWriter writer = new CSVParaWriter(csvFile, allParaDomains, unUsedVars, mddBuilder);
        writer.writeCSV(root);
        if (root == -1) {
            System.out.println("--- Time for MDD solutions enumeration " + MDD_ENUMERATE_TIME + "ms");
        }
        System.out.println("--- Total time for MDD " + Clock.print(MDD_BUILD_TIME + MDD_ENUMERATE_TIME));
        System.out.println("\n*** Solutions have been printed in " + csvFile);
    }
}
