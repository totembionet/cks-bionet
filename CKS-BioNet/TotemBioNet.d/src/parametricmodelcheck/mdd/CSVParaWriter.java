package parametricmodelcheck.mdd;


import mddapi.mddlib.MDDManager;
import mddapi.mddlib.MDDVariable;
import mddapi.mddlib.PathSearcher;
import util.TotemBionetException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 *
 * Class to write set of data to csv files
 *
 *
 * @author Hélène Collavizza
 */

public class CSVParaWriter {

    // where to write the MDD
    private BufferedWriter writer;

    // the MDDbuilder whose values must be written
    MDDBuilder mddBuilder;

    // to complete csv with paras which are not in the MDD
    // the model-checking doesn't depend on these variables
    // useful to compare results between CKS and TotemBioNet
    HashMap<String, int[]> unUsedVars;

    public CSVParaWriter(String path, HashMap<String, int[]> allParaDomains, HashSet<String> unUsedVars, MDDBuilder mddBuilder) throws IOException {
        writer = new BufferedWriter(new FileWriter(new File(path)));
        this.mddBuilder = mddBuilder;
        initUnused(allParaDomains, unUsedVars);
    }


    // to build the set of paras which are not used in MDDs
    private void initUnused(HashMap<String, int[]> allParaDomains, HashSet<String> unUsedVars) {
        this.unUsedVars = new HashMap<String, int[]>();
        Collection<String> allParaNames = allParaDomains.keySet();
        for (String s : allParaNames) {
            if (unUsedVars.contains(s)) {
                this.unUsedVars.put(s, allParaDomains.get(s));
            }
        }
    }

    /**
     * Write a MDD into a csv file
     *
     * @throws IOException
     */
    public void writeCSV(int root) throws IOException, TotemBionetException {
//        System.out.println("unused " + Arrays.toString(new HashMap[]{unUsedParas}));
//        System.out.println("one value " + mddBuilder.oneValueVars.keySet());
//        System.out.println("key set " + mddBuilder.nameToIndex.keySet());
        String head = getCSVHeader();
        String domains = getCSVDomains();
        writer.write(head);
        writer.write(domains);
        // all solutions
        if (root==-1)
            writeFullCSV(mddBuilder.nameToDomain);
        else
            // if root = -2 there is no solution
            if (root!=-2)
                writePaths(root);
        writer.close();
    }

    /**
     * Write all the paths of the MDD (i.e. all possible values)
     * fixed then unused then other variable are printed
     *
     * @return
     * @throws IOException
     */
    private void writePaths(int root) throws IOException, TotemBionetException {
        HashMap<String, int[]> namesToDomain = mddBuilder.nameToDomain;
        MDDManager mddManager = mddBuilder.mddManager;
        MDDVariable[] vars = mddManager.getAllVariables();
//        System.out.println("vars " + Arrays.toString(vars));
//        System.out.print("domains "); mddBuilder.printDomains();
        PathSearcher ps = new PathSearcher(mddManager, 1);
        ps.setNode(root);
        int nbPaths = ps.countPaths();
        if (nbPaths == 0)
            System.out.println("MDD is empty, there is no solution");
        else {
            // pre-processing to build values for fixed and unused variables
            // which have the same values for each line
            int i = 0;
            // unUsed vars => the constraints do not depend on these variables
            // either fixed paras or variables which do not occur in the contraints to solve
            String unUsed = "";
            for (int[] domain : unUsedVars.values()) {
                if (domain[0] == domain[1])
                    unUsed += domain[0] + ";";
                else
                    unUsed += domain[0] + ".." + domain[1] + ";";
            }
            //other vars
            // each iteration builds a solution <=> a line in the csv
            for (int p : ps) {
                int[] path = ps.getPath();
                StringBuilder res = new StringBuilder(i + ";OK;" + unUsed);
                for (int j = 0; j < path.length; j++) {
                    // don't print the unused variables
                    // here, path[j] can be equal to -1 because the variable was not used in the MDD
                    // event if it is not a fixed para
                    int[] domain = namesToDomain.get(vars[j]);
                    // -1 means that all values are OK
                    if (path[j] == -1) {
                        res.append(domain[0] + ".." + domain[1]);
                    } else {
                        // write the value
                        // here one checks if the value is in the initial domain of the variable
                        if (path[j] > domain[1] | path[j] < domain[0])
                            throw new TotemBionetException("Value " + path[j] + " of " + vars[j].toString() + " in MDD is out of bounds " + domain[0] + ".." + domain[1]);
                        res.append(path[j] + "");
                    }
                    res.append(';');
                }
                writer.write(res.toString() + '\n');
                i++;
            }
        }
    }

    private void writeFullCSV(HashMap<String, int[]> namesToDomain) throws IOException, TotemBionetException {
//        System.out.println("vars " + Arrays.toString(vars));
//        System.out.print("domains "); mddBuilder.printDomains();

        // pre-processing to build values for fixed and unused variables
        // which have the same values for each line
        int i = 0;
        // unUsed vars => the constraints do not depend on these variables
        // either fixed paras or variables which do not occur in the contraints to solve
        String unUsed = "";
        for (int[] domain : unUsedVars.values()) {
            if (domain[0] == domain[1])
                unUsed += domain[0] + ";";
            else
                unUsed += domain[0] + ".." + domain[1] + ";";
        }
        StringBuilder res = new StringBuilder(0 + ";OK;" + unUsed);
        for (int[] domain : namesToDomain.values()) {
            res.append(domain[0] + ".." + domain[1]);
        }
        writer.write(res.toString() + '\n');
    }




    /**
     * Write the first line according Totem syntax
     * @return
     */
    private String getCSVHeader() {
        Set<String> others=mddBuilder.nameToDomain.keySet();
        StringBuilder header =new StringBuilder("n;valid;");
        // unUsed var names
        for (String n : unUsedVars.keySet())
            header.append(n+";");
         // other var
        // NOTA: order of variables in keySet is the same as in ps.getPath()
        for (String n : others) {
            assert (!unUsedVars.containsKey(n));
            header.append(n + ";");
        }
        return header+"\n";
    }


    /**
     * Write the second line with domains
     * @return
     */
    private String getCSVDomains() {
        Collection<int[]> others = mddBuilder.nameToDomain.values();
        StringBuilder header =new StringBuilder(";;");
         // unUsed vars
        for (int[] domain : unUsedVars.values())
            header.append(domain[0] + ".." + domain[1]+";");
        // other var domains
        for (int[] n : others) {
            assert (!unUsedVars.containsKey(n));
            header.append(n[0] + ".." + n[1] + ";");
        }
        return header+"\n";
    }

}
