package parametricmodelcheck.mdd;

import mddapi.mddlib.*;
import mddapi.mddlib.operators.MDDBaseOperators;
import parametricmodelcheck.solver.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/**
 * class to encapsulate a MDD
 * @author Helene Collavizza
 */

public class MDDBuilder {

    MDDManager mddManager;
    // variables handled by the MDD manager. Their domains contains at least 2 values
    MDDVariable[] vars;
    // the root of the MDD which contains all values
    // created only if needed. =-1 if not yet created
    int rootTrue=-1;


    // map name of the MDD variable to its index in vars
    // name of the MDD variable is the name of a parameter of the net
    // used in getNodeAtom
    HashMap<String,Integer> nameToIndex;

    // map name of the MDD variable to its domain
    HashMap<String,int[]> nameToDomain;


    // build the MDDManager from variables in usedVarsAndDomains
    // only useful variables are used
    // all variables domains contain at least 2 values
    public MDDBuilder(HashMap<String,int[]> usedVarsAndDomains){
        this.nameToIndex = new HashMap<>();
        this.nameToDomain = new HashMap<>();
        MDDVariableFactory vbuilder = new MDDVariableFactory();
        initVars(usedVarsAndDomains,vbuilder);
        //TODO: nbleaves = "number of values that can be reached"
        //      ne marche pas si on met 1
        this.mddManager = MDDManagerFactory.getManager( vbuilder,2);
        this.vars = this.mddManager.getAllVariables();
    }


    // init the variables of vbuilder => put the used variables in usedVarsAndDomains in the MDDManager
    // also manage the map for domains and indexes
    private void initVars(HashMap<String,int[]> usedVarsAndDomains,MDDVariableFactory vbuilder) {
        int i = 0;
        for (String name : usedVarsAndDomains.keySet()) {
            int[] levels = usedVarsAndDomains.get(name);
            // to have the correct solutions, all values between 0 and max must be considered
            byte nbVal = (byte) (levels[1] + 1);
            vbuilder.add(name, nbVal);
            nameToIndex.put(name, i);
            nameToDomain.put(name, levels);
            i++;
        }
    }

    // get a node in the global MDDManager for atom a
    // PRE : a is either name>n, name<n, name=n where name is the name of a variable in
    //       vars and n is a value in the domain of name
    public int getNodeAtom(Atom a) {
        int index = nameToIndex.get(VariableFactory.getName(a.varId()));
        MDDVariable mddVar = vars[index];
        byte nbVal = mddVar.nbval;
        int n = a.value();
        // to add values
        int[] values = new int[nbVal];
        switch (a.op()) {
            // init values with constraint v<n
            // each digit j<n is 1
            case "<": {
                for (int j = 0; j < n; j++) {
                    values[j] = 1;
                }
                break;
            }
            // init values with constraint v>n
            // each digit j>n is 1
            case ">": { // init vi with constraint vi>nb
                for (int j = n + 1; j < nbVal; j++) {
                    values[j] = 1;
                }
                break;
            }
            // init values with constraint v=n
            // digit n is 1
            case "=": { // init vi with constraint vi=nb
                values[n] = 1;
            }
            break;
            // not used
            case "<=": {
                for (int j = 0; j <= n; j++) {
                    values[j] = 1;
                }
                break;
            }
            // not used
            case ">=": { // init vi with constraint vi>nb
                for (int j = n; j < nbVal; j++) {
                    values[j] = 1;
                }
                break;
            }
        }
        return mddVar.getNode(values);
    }

    // build the MDD for TRUE if needed
    // NOTA: should not happen
    public int getMDDTrue() {
        if (rootTrue==-1){
            rootTrue= getAllValues();
        }
        return rootTrue;
    }


    // when all values are OK => build the MDD with all possible values
    private int getAllValues() {
        System.out.println("... ALL values used in MDD");
        int[] nodes = new int[vars.length];
        int i=0;
        for (MDDVariable mddVar: vars) {
            byte nbVal = mddVar.nbval;
            int[] values = new int[nbVal];
            for (int j = 0; j < nbVal; j++) {
                values[j] = 1;
            }
            nodes[i++] = mddVar.getNode(values);
        }
        return MDDBaseOperators.OR.combine(mddManager,nodes);
    }

///////////////////////////////////////////////:
    // methods to combine MDDs

    int getNegation(int root) {
        return mddManager.not(root);
    }

    int getConjunction(int[] nodes) {
        return MDDBaseOperators.AND.combine(mddManager,nodes);
    }

    int getDisjunction(int[] nodes) {
        return MDDBaseOperators.OR.combine(mddManager,nodes);
    }

    // build the string with all solutions
    public String pathsToString(int root) {
        PathSearcher ps = new PathSearcher(mddManager, 1);
        ps.setNode(root);
        StringBuilder res=new StringBuilder("MDD has "  + ps.countPaths() + " condensed solutions");
        int k= 1;
        String var="";
        for (int p : ps) {
            int[] path = ps.getPath();
            StringBuilder str = new StringBuilder("\nSolution " + k + ": ");
            for (int i = 0; i < path.length; i++) {
                var+=vars[i] + ",";
                // display only solution in the initial domain of the variable
                if (path[i] == -1)
                    str.append(vars[i] + ":" + path[i] + ", ");
                else {
                    if (path[i] >= nameToDomain.get(vars[i].toString())[0])
                        str.append(vars[i] + "=" + path[i] + ", ");
                    else
                        str.append("WARNING: " + vars[i] + ":" + path[i] + "is a MDD solution but is not in the initial domain, ");
                }
            }
            k++;
            res.append(str+"\n");
        }
//        System.out.println("keySet " + nameToIndex.keySet() + " " + var);

        return res.toString();
    }

    // return the disjunction that corresponds to the values in the MDD root
    // disjunction such that : (x1=v1 or x3 = v3 or ... or xn=vn)
    // for each varaible vi where the value of vi is not -1
    public Disjunction pathsToDisjunction(int root) {
        PathSearcher ps = new PathSearcher(mddManager, 1);
        ps.setNode(root);
        int k= 1;
        String var="";
        Disjunction res = new Disjunction();
        for (int p : ps) {
            int[] path = ps.getPath();
            Conjunction conj = new Conjunction();
            for (int i = 0; i < path.length; i++) {
                var+=vars[i] + ",";
                // display only solution in the initial domain of the variable
                if (path[i]!=-1) {
                    if (path[i] >= nameToDomain.get(vars[i].toString())[0]) {
                        byte id = VariableFactory.getID(vars[i].toString());
                        Atom eq = AtomFactory.makeEqual(id, (byte) path[i]);
                        conj = conj.andWithAtom(eq);
                    } else
                        throw new RuntimeException(vars[i] + ":" + path[i] + "is a MDD solution but is not in the initial domain, ");
                }
            }
            res = res.orWithDisjunction(new Disjunction(conj));
            k++;

        }
        return res;
    }

    // to know if the MDD of root "root" is empty
    public boolean isEmptyMDD(int root) {
        PathSearcher ps = new PathSearcher(mddManager, 1);
        ps.setNode(root);
        return ps.countPaths()==0;
    }

    void printDomains() {
        for (String s : nameToDomain.keySet()) {
            System.out.print(s + ":" + Arrays.toString(nameToDomain.get(s)) + " ");
        }
        System.out.println();
    }
}
