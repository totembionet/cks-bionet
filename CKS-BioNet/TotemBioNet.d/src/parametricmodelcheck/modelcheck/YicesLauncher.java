package parametricmodelcheck.modelcheck;

import back.net.Gene;
import back.net.Net;
import back.net.Para;
import parametricmodelcheck.solver.Disjunction;
import util.TotemBionetException;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * class to launch yices to check formulas built during model checking
 *
 * @author Helene Collavizza
 */
public class YicesLauncher {

    public final static int NEW_EQUAL_OLD=0;
    public final static int NEW_DIFF_OLD=1;
    public final static int NEW_IN_OLD=2;
    public final static int OLD_IN_NEW=3;
    
        // path to yices
    public static String YICESPATH;

    public static void setYicesPath() {
        String env = System.getenv("YICESPATH");
        YICESPATH = (env != null) ? env : "lib/yices-2.6/yices";
    }

    // the definition of variables and assertion on their domains
    private ArrayList<String> varDeclaration;
    // file to write yices statements
    private String yicesFile;
    // the file
    private FileWriter yicesFileWriter;

    private boolean withBool;

    public YicesLauncher(String path, Net net) {
        this(path,net,true,true);
    }

    public YicesLauncher(String path, Net net,boolean withBool,boolean withDomain) {
        this.yicesFile = path + "yicesConstraint.ys";
        try {
            yicesFileWriter = new FileWriter(yicesFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.varDeclaration = new ArrayList<>();
        buildVarDeclaration(net,withDomain);
        this.withBool = withBool;
    }

    // build the string for defining variables and domains
    // used only once
    private void buildVarDeclaration(Net net) {
        buildVarDeclaration(net,false);
    }

    // build the string for defining variables and domains
    // used only once
    // if variable is 0..1 it is translated as a boolean
    private void buildVarDeclaration(Net net,boolean withDomain) {
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g)) {
                for (Para p : g.getParas()) {
                    String name = toYicesIdent(p.getSMBname());
                    if (!p.fixe) {
                        // bool var
                        if (withBool& (p.minLevel() == 0) & p.maxLevel() == 1) {
                            varDeclaration.add("(define " + name + "::bool)");
                        }
                        // integer var
                        else {
                            varDeclaration.add("(define " + name + "::int)");
                            if (withDomain) {
                                varDeclaration.add("(assert (<= " + name + " " + p.maxLevel() + "))");
                                varDeclaration.add("(assert (>= " + name + " " + p.minLevel() + "))");
                            }
                        }
                    }
                }
            }
        }
    }

    // change the : of SMBname into - because : is not admissible in yices
    public static String toYicesIdent(String name) {
        return name.replace(":","-");
    }

    // to write and solve a formula
    public void writeYices(Disjunction f, String comment) throws IOException {
        yicesFileWriter.write("; File generated from model-checker\n");
        yicesFileWriter.write("; Formula " + f + "\n\n");
        writeVariables(yicesFileWriter);
        yicesFileWriter.write("\n;Formula for " + comment + "\n");
        yicesFileWriter.write("(assert " + f.toYices() + ")");
        yicesFileWriter.write("\n(check)");
        yicesFileWriter.close();
    }

    // to write and solve a formula
    public void writeOneAssert(Disjunction f) throws IOException {
        yicesFileWriter.write("(assert " + f.toYices() + ")\n");
    }

    public void writeHeader() throws IOException {
        yicesFileWriter.write("; File generated from model-checker\n");
        writeVariables(yicesFileWriter);
    }

    public void close() throws IOException {
        yicesFileWriter.write("\n(check)");
        yicesFileWriter.close();
    }

    // to write and solve a formula
    public void writeYices(String s, String comment) throws IOException {
        FileWriter yicesFileWriter = new FileWriter(yicesFile);
//        System.out.println("yices formula " + s);
        yicesFileWriter.write("; File generated from model-checker\n");
        writeVariables(yicesFileWriter);
        yicesFileWriter.write("\n;Constraints for " + comment + "\n");
        yicesFileWriter.write("(assert " + s + ")");
        yicesFileWriter.write("\n(check)");
        yicesFileWriter.close();
    }


    // write the variables
    // TODO : only useful variables => only variables which are in the formula to check
    private void writeVariables(FileWriter fw) throws IOException {
        for (String line : varDeclaration)
            fw.write(line + "\n");
    }

    // launch yices and return true if sat
    public boolean yicesIsSAT() throws Exception {
        Process p;
        p = Runtime.getRuntime().exec(YICESPATH + " " + yicesFile);
        p.waitFor();
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line = "";
        if ((line = reader.readLine()) != null) {
//            System.out.println(line);
            if (line.equals("sat"))
                return true;
            if (line.equals("unsat"))
                return false;
            if (line.contains("unsat")) {
                System.out.println("*** WARNING sat answer :" + line);
                return false;
            }
        }
        throw new TotemBionetException("Result of yices is not SAT nor UNSAT. Check the yices input file: " + yicesFile);
    }

    //////////////////////////////////////////////
    // to test equality of 2 disjunctions
    // if (not (<=> old new )) is SAT then old and new differ
    public boolean testEquality(Disjunction oldf, Disjunction newf) {
        String o = oldf.toYices();
        String n = newf.toYices();
        String cst = "(not (<=> " + o + " " + n + "))";
        try {
            writeYices(cst, "for testing equality");
            return ! yicesIsSAT();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("YICES input error");
        }
    }

    // to know if a Disjunction is satisfiable
    public boolean testSatisfiability(Disjunction disj) {
        String cst = disj.toYices();
        try {
            writeYices(cst, "for testing satisfiability");
            return yicesIsSAT();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("YICES input error");
        }
    }


    // to know if the solutions of oldf is included into the solutions of newf
    // require Yices 2.6.2 to use push and pop operation
    public int testInclusion(Disjunction oldf, Disjunction newf) {
        String o = oldf.toYices();
        String n = newf.toYices();
        String imply1 = "(and " + o + " " + "(not " + n + "))";
        String imply2 = "(and " + n + " " + "(not " + o + "))";
        try {
            writeInclusions(imply1,imply2);
            return executeInclusionCommand();

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("YICES input error");
        }
    }

    // to write and solve a formula
    public void writeInclusions(String s1,String s2) throws IOException {
        FileWriter yicesFileWriter = new FileWriter(yicesFile);
        yicesFileWriter.write("; File generated from model-checker\n");
        writeVariables(yicesFileWriter);
        yicesFileWriter.write("\n;First inclusion \n");
        yicesFileWriter.write("(push)\n");
        yicesFileWriter.write("(assert " + s1 + ")");
        yicesFileWriter.write("\n(check)\n");
        yicesFileWriter.write("(pop)\n");
        yicesFileWriter.write("\n;Second inclusion \n");
        yicesFileWriter.write("(assert " + s2 + ")");
        yicesFileWriter.write("\n(check)");
        yicesFileWriter.close();
    }

    // check first if old & !new is SAT and then if new & !old is SAT
    // NOTA: when the answer is UNSAT, it is not clear why and when yices returns UNSAT 2 times
    public int executeInclusionCommand() throws Exception {
        Process p;
        p = Runtime.getRuntime().exec(YICESPATH + " " + yicesFile);
        p.waitFor();
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        ArrayList<String> lines = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.equals("unsat") & line.contains("unsat"))
                throw new TotemBionetException(" Yices error : sat answer :" + line);
            lines.add(line);
        }
//        System.out.println("   ****  Test inclusion : Yices answer   ****        " + lines);
        if (lines.size()>2 && lines.get(0).equals("sat") && lines.get(1).equals("sat") ) {
            throw new TotemBionetException("Result of yices for inclusion is not fair " + lines);
        }
        int first = -1;
        int second = -1;
        if (lines.get(0).equals("sat"))
            first = 1;
        else if (lines.get(0).equals("unsat"))
            first = 0;
        int next=1;
        if (lines.size() > 2 & first==0)
            next=2;
        if (lines.get(next).equals("sat"))
            second = 1;
        else if (lines.get(next).equals("unsat"))
            second = 0;
        if (first == -1 | second == -1)
            throw new TotemBionetException("Result of yices is not SAT nor UNSAT. Check the yices input file: " + yicesFile);
        // the two inclusions are true, the sets are equals
        if (first == 0 && second == 0)
            return NEW_EQUAL_OLD;
        // old is included in new
        if (first == 0 && second == 1)
            return OLD_IN_NEW;
        // new is included in old
        if (first == 0 && second == 1)
            return NEW_IN_OLD;
        return NEW_DIFF_OLD;
    }

}





