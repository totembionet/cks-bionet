package parametricmodelcheck.modelcheck;

import back.logic.CTLlogic.ExistGenerally;
import back.logic.CTLlogic.ExistNeXt;
import back.logic.CTLlogic.ExistUntil;
import back.logic.Formula;
import back.logic.Var;
import back.logic.blogic.*;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import back.net.Reg;
import parametricmodelcheck.mdd.MDDSolver;
import parametricmodelcheck.solver.*;
import run.Main;
import util.Out;
import util.TotemBionetException;
import util.jclock.Clock;
import util.jlist.OrderedArrayListStringID;

import java.util.*;

import static parametricmodelcheck.modelcheck.YicesLauncher.*;
import static parametricmodelcheck.solver.Disjunction.TRUE;

/**
 * Class for parametric model-checking
 *
 * state st is labeled with (phi, cstPhi) when CTL formula phi is valid on st under constraint cstPhi
 *
 *     - if the constraint required to obtain phi on state st is INCONSISTENT, st is not labeled with phi
 *     - if phi is always true on state st, st is labeled with (phi,TRUE)
 *     - empty constraint (i.e. d = new Disjunction()) is used to build new possibilities
 *
 * @author Helene Collavizza
 */
public class ParametricModelChecker {


    // static fields for time and steps information
    private static int nbStepsEU = 1;
    private static int nbStepsEG = 1;

    private static int NB_EQ_YICES = 0;
    private static int NB_REMOVE_YICES = 0;
    private static int NB_REMOVE_EQUALS = 0;
    private static int NB_REMOVE_TRUE = 0;

    private static int TIME_EQ_YICES = 0;
    public static long MCC_TIME = 0;


    // yices total time to test loop stop conditions (i.e. to obtain a fix-point)
    private long YICES_SEMANTIC_EQ_TOTAL_TIME = 0;
    // yices time for testing loop condition => for each individual test
    private long YICES_SEMANTIC_EQ_TIME = 0;
    // yices time for testing semantic equality of formulas
    // to remove equals disjunctions before building the MDDs
    private int YICES_EQ_TEST = 0;

    // path to file that contains the Net and the CTL formula
    private String path;
    // set of states with their labels
    private LabeledStates states;
    // the regulation network built from .smb file
    private Net net;
    // the ctl formula in the .smb file
    private Formula ctl;
    // the ctl formula to model check
    private Formula ctlToModelCheck;
    // HashMap to get sub-formulas by set of same heights
    private HashMap<Integer, HashSet<Formula>> formulaByHeight;
    // the list of formulas which used a level
    // when this list becomes empty, the formulas of this level can be removed from states
    private HashMap<Integer, HashSet<Integer>> levelUsedBy;

    // map gene names to indexes to be able to get the values of state variables
    private HashMap<Integer, String> mapIndexVar;
    // list of effective genes (i.e. VAR, not ENV_VAR because ENV_VAR have no associated parameters)
    ArrayList<Gene> genes;
    // list of state names
    private Set<String> statesName;
    // the yices launcher to test semantic equality of formulas
    private YicesLauncher yices;

    // variables which appear in the contraints to solve
    // required to build the MDDs only using these variables
    private HashSet<String> usedVarsToSolve;

    // path is the path to the input file
    // n is the Net built from the .smb input file
    public ParametricModelChecker(String path, Net n) throws TotemBionetException {
        this.path = path;
        // data structure to label states with couple of formulas and constraints on parameters
        this.states = new LabeledStates();
        this.net = n;
        this.genes = new ArrayList<>();
        // to make link between name of variables
        this.mapIndexVar = new HashMap<>();
        this.usedVarsToSolve = new HashSet<>();
        initGenes(net.genes());
        long t1 = System.currentTimeMillis();
        if (Out.verb() >= 1)
            System.out.println("Starting init state ...");
        initStates();
        long t2 = System.currentTimeMillis();
        if (Out.verb() >= 1)
            System.out.println("    ... time for init state " + Clock.print(t2 - t1));
        this.statesName = states.getStates();
        yices = new YicesLauncher(path, net, false, false);
        int nbParas = net.nbParams();
        // formulas
        // fair ctl translation
        List<Formula> fairForm = getFairBio(net.getFairCtl());
        // conjunction of fair and ctl formulas
        this.ctl = buildSpec(net.getCtl(), fairForm);
        // translation into operators admitted in the CKS mc algorithm
        t1 = System.currentTimeMillis();
        if (Out.verb() >= 1)
            System.out.println("Starting translation ...");
        this.ctlToModelCheck = this.ctl.translateCTL();
        t2 = System.currentTimeMillis();
        if (Out.verb() >= 1)
            System.out.println("    ... end translation " + Clock.print(t2 - t1));
        printNetInformation(nbParas, fairForm);
        formulaByHeight = new HashMap<Integer, HashSet<Formula>>();
        levelUsedBy = new HashMap<Integer, HashSet<Integer>>();
    }

    // to print some information about the regulation network
    private void printNetInformation(int nbParas, List<Formula> fairForm) {
        System.out.println("\n################ Net information ################");
        System.out.println("This net has " + genes.size() + " genes governed by " + nbParas + " parameters.");
        System.out.println("   There are " + statesName.size() + " states in the transition graph.");
        System.out.println("   There are " + net.NB_PARAMETERIZATIONS + " parameter settings.");
        System.out.println("################     Formula     ################");
        System.out.println("List of CTL formulas: " + net.getCtl());
        if (fairForm.size() != 0) {
            System.out.println("List of FAIR CTL formulas: " + net.getFairCtl());
            System.out.println("      Translated as: " + fairForm);
        }
        System.out.println("   Translation to apply CKS model-checking : " + ctlToModelCheck);
        System.out.println("#################################################");
    }

    //////////////////////////////////////////////////
    ////// INIT methods
    //////////////////////////////////////////////////

    // to store the useful genes => variables which are not environment variables
    private void initGenes(OrderedArrayListStringID<Gene> genes) {
        int i = 0;
        for (Gene g : genes) {
            if (!net.isEnvVar(g)) {
                mapIndexVar.put(i, g.name);
                this.genes.add(g);
                i++;
            }
        }
    }

    // to make the conjunction of ctl formulas from CTL block and FAIRCTL block
    private Formula buildSpec(List<Formula> ctl, List<Formula> lff) throws TotemBionetException {
        if (ctl.size() == 0 & lff.size() == 0)
            throw new TotemBionetException("No CTL formula to model-check");
        if (lff.size() == 0)
            return buildConjunction(ctl);
        if (ctl.size() == 0)
            return buildConjunction(lff);
        return new And(buildConjunction(ctl), buildConjunction(lff));
    }

    // get the fairbio version of ctl formulas in fctl
    private List<Formula> getFairBio(List<Formula> fctl) {
        List<Formula> lff = new ArrayList<>();
        for (Formula f : fctl)
            lff.add(f.fairBio());
        return lff;
    }

    // return the conjunction of a list of formula
    private Formula buildConjunction(List<Formula> lf) {
        if (lf.size() == 1)
            return lf.get(0);
        if (lf.size() % 2 == 0)
            return buildConjunction(lf, 0);
        return new And(lf.get(0), buildConjunction(lf, 1));
    }

    // auxiliary function for recursion
    private Formula buildConjunction(List<Formula> lf, int i) {
        if (i == lf.size() - 2)
            return new And(lf.get(i), lf.get(i + 1));
        return new And(lf.get(i), buildConjunction(lf, i + 1));
    }

    // init the state with empty list of labels
    private void initStates() {
        firstState();
        do {
            states.init(vectorToString(net.currentState()));
        } while (nextState());
    }

    ///////////////////////////////////////////////////////
    // methods to handle levels of sub-formulas
    //////////////////////////////////////////////////

    // to add a formula to the HashSet of formula of height h
    private void addFormulaByHeight(int h, Formula f) {
        if (formulaByHeight.get(h) == null) {
            HashSet<Formula> hs = new HashSet<>();
            hs.add(f);
            formulaByHeight.put(h, hs);
        } else {
            formulaByHeight.get(h).add(f);
        }
        f.setHeight(h);
    }

    // h is a height, id the id of a formula
    // add id in the set of forumlas which are used by the level h
    private void addUsedByLevel(int h, int id) {
        if (levelUsedBy.get(h) == null) {
            HashSet<Integer> hs = new HashSet<>();
            hs.add(id);
            levelUsedBy.put(h, hs);
        } else {
            levelUsedBy.get(h).add(id);
        }
    }

    // to init the HashMap which associates its height to a formula
    private int initFormulaHeights(Formula f) {
        Formula fFact = FormulaFactory.addFormula(f);
        if (fFact == null) {
            // atom and True are level 0
            if (f.isAtom() | f.isTrueFormula()) {
                addFormulaByHeight(0, f);
                return 0;
            }
            // not, or, and
            if (!f.isCTL()) {
                // not(f) is level(f) + 1
                if (f instanceof Not) {
                    Formula child = ((Not) f).getRight();
                    int ch = initFormulaHeights(child);
                    addFormulaByHeight(ch + 1, f);
                    addUsedByLevel(ch, f.id);
                    return ch + 1;
                }
                // f1 or f2 is level max(level(f1),level(f2))+1
                if ((f instanceof And) | (f instanceof Or)) {
                    BinaryConnective bc = (BinaryConnective) f;
                    int hl = initFormulaHeights(bc.getLeft());
                    addUsedByLevel(hl, f.id);
                    int hr = initFormulaHeights(bc.getRight());
                    addUsedByLevel(hr, f.id);
                    int h = Math.max(hl, hr) + 1;
                    addFormulaByHeight(h, f);
                    return h;
                }
            }
            // CTL formulas
            // EX(phi)
            // EX(phi) is level level(phi) + 1
            if (f instanceof ExistNeXt) {
                Formula child = ((ExistNeXt) f).getLeft();
                int h = initFormulaHeights(child);
                addFormulaByHeight(h + 1, f);
                addUsedByLevel(h, f.id);
                return h + 1;
            }
            // E(phi U psi)
            // E(phi U psi) is level max(level(phi),level(psi)) + 1
            if (f instanceof ExistUntil) {
                Formula first = ((ExistUntil) f).getLeft();
                Formula second = ((ExistUntil) f).getRight();
                int hf = initFormulaHeights(first);
                addUsedByLevel(hf, f.id);
                int hs = initFormulaHeights(second);
                addUsedByLevel(hs, f.id);
                int h = Math.max(hf, hs) + 1;
                addFormulaByHeight(h, f);
                return h;
            }
            // EG
            // EG(phi) is level level(phi) + 1
            if (f instanceof ExistGenerally) {
//                System.out.println("EG");
                // recursive call to label states with phi
                Formula child = ((ExistGenerally) f).getLeft();
                int h = initFormulaHeights(child);
                addFormulaByHeight(h + 1, f);
                addUsedByLevel(h, f.id);
                return h + 1;
            }

            System.out.println("    *****" + f.getClass().getSimpleName() + " is not yet handled");
        }
        return fFact.getHeight();
    }


    // return the id of formulas whose levels are in levelToRemove
    private HashSet<Integer> formulaIdToRemove(HashSet<Integer> levelToRemove) {
        if (Out.verb() >= 1)
            System.out.println("levels to remove " + levelToRemove);
//        System.out.println("levelUsedBy " + levelUsedBy);
        HashSet<Integer> idToRemove = new HashSet<>();
        for (int l : levelToRemove) {
            for (Formula f : formulaByHeight.get(l))
                idToRemove.add(f.id);
        }
        if (Out.verb() >= 1)
            System.out.println("formulas to remove " + idToRemove);
        return idToRemove;
    }

    //////////////////////////////////////////////
    // printing utilities
    //////////////////////////////////////////////
    public String formulaByHeightString() {
        String res = "";
        for (int i = 0; i < formulaByHeight.keySet().size(); i++) {
            res += i + " " + print(formulaByHeight.get(i)) + "\n";
        }
        return res;
    }

    private String print(HashSet<Formula> list) {
        String res = "";
        for (Formula f : list) {
            res += f.toStringWithId() + " ";
        }
        return res;
    }

    public String levelUsedByString() {
        String res = "";
        for (int i = 0; i < levelUsedBy.keySet().size(); i++) {
            res += i + " " + levelUsedBy.get(i) + "\n";
        }
        return res;
    }

    // remove id from all lists in levelUsedBy
    // PRECOND: formula of ident id has been model checked
    // if a level is is not more used by any formula, then it is added
    // to the list of levels to be removed
    private HashSet<Integer> removeFromLevelUsed(int id) {
        HashSet<Integer> toRemove = new HashSet<>();
        for (int level : levelUsedBy.keySet()) {
            HashSet<Integer> formulas = levelUsedBy.get(level);
            if (!formulas.isEmpty()) {
                formulas.remove(id);
                if (formulas.isEmpty()) {
                    toRemove.add(level);
                }
            }
        }
        return toRemove;
    }

    ///////////////////////////////////
    // methods on network and parameters
    ///////////////////////////////////

    // to browse all states
    private void firstState() {
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g))
                g.firstLevel();
        }
    }

    private boolean nextState() {
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g) & g.nextLevel())
                return true;
        }
        return false;
    }

    /**
     * @param g : a gene
     * @return : the applicable parameter for this gene on state s
     */
    private Para getApplicablePara(Gene g, String s) {
        // values of variables
        HashMap<String, Integer> valValues = makeMapVarValues(s);

        // check which regulation is effective on this state
        OrderedArrayListStringID<Reg> paraRegs = new OrderedArrayListStringID<Reg>();
        for (Reg r : g.getRegs()) {
            if (r.isEffective(valValues)) {
                paraRegs.addAlpha(r);
            }
        }
        // build the name of the parameter and get the associated Para object
        String paraNuSMVName = "K" + g.name;
        for (int i = 0; i < paraRegs.size(); i++) {
            paraNuSMVName += "_" + paraRegs.get(i);
        }
        //On regarde si le param�tre existe d�j�
        //System.out.println(g.getParas());
        Para p = g.getParas().getWithStringId(paraNuSMVName);
        return p;
    }

    /**
     * tool for translating a vector of int into a long
     * NOTA: assume that upper bounds of variables are less or equal to 9
     */
    private Long vectorToLong(ArrayList<Integer> list) {
        long r = 0;
        for (Integer i : list) {
            r = r * 10 + i;
        }
        return r;
    }

    private String vectorToString(ArrayList<Integer> list) {
        String r = "";
        for (Integer i : list) {
            r += i;
        }
        return r;
    }

    ///////////////////////////////////////////////////////
    // methods for model checking
    ///////////////////////////////////////////////////////

    // starting point of model checking
    //////////////////////////////////////
    // Model check uses verbose level for printing information:
    //   1: only print
    //   2: print sol and print labeled states for each step for debuging (cf labelExistEU)
    //   3: print solution + use yices to remove redundant constraints (cf toSolve)

    // model check the formula ctlToModelCheck
    // sub-formula are treated by deep level
    // when a subformula has been model-check and is no more needed,
    // information about this subformula it is removed from the labeled states
    public void modelCheck() {
        // init the data structure that store the subformulas by levels
        initFormulaHeights(ctlToModelCheck);
        if (Out.verb() == 2) {
            System.out.println("  height \n" + formulaByHeightString());
            System.out.println("  used by \n" + levelUsedByString());
        }
        if (Out.verb() >= 1)
            System.out.println();
        long t1 = System.currentTimeMillis();
        // Check formulas starting by level 0
        // NOTA: good! keySet are in the right order !
        for (Integer level : formulaByHeight.keySet()) {
            if (Out.verb() >= 1)
                System.out.println("Checking formulas of level " + level);
            HashSet<Integer> levelToRemove = new HashSet<>();
            // model check the subformulas of current level "level"
            for (Formula f : formulaByHeight.get(level)) {
                modelCheck(f);
                levelToRemove.addAll(removeFromLevelUsed(f.id));
            }
            // remove subformulas which are no more necessary
            if (!levelToRemove.isEmpty()) {
                if (Out.verb() >= 1)
                    System.out.println("       removing unused levels ...");
                int n = states.remove(formulaIdToRemove(levelToRemove));
                if (Out.verb() >= 1) {
                    System.out.println("       " + n + " labels have been removed for level " + level);
                    System.out.println("              /// Total number of atoms " + this.nbAtoms());
                }
//                System.out.println("state after remove " + this);
            }
//            System.out.println("State after checking formulas of height " + level);
//            System.out.println(this);
        }
        long t2 = System.currentTimeMillis();
        MCC_TIME = t2 - t1;
        if (Out.verb() == 2) {
            System.out.println("       ****** end of model-checking");
            System.out.println(this);
        }
        System.out.println("\n--- Time for model-checking:" + Clock.print(MCC_TIME));
        System.out.println("    including " + Clock.print(YICES_SEMANTIC_EQ_TOTAL_TIME) + " to call Yices for loop termination.\n");
//        System.out.println("number of atoms in factory " + AtomFactory.ID + " number of used atoms "  + states.nbAtoms());
//        System.out.println("final state " + this);
    }

    // model check the formula f
    // POST: states where f is true have been labeld with f and the constraint on parameters
    //       which ensures that f is true
    private void modelCheck(Formula f) {
        if (Out.verb() >= 1)
            System.out.println("   Handling formula " + f);
        try {
            if (f.isAtom())
                labelAtom(f);
            else {
                if (!f.isCTL())
                    handleSimpleLogic(f);
                else
                    handleCTL(f);
            }
        } catch (ConcurrentModificationException e) {
            System.err.println("there was a concurrent modification");
            e.printStackTrace();
        }
        if (Out.verb() >= 1)
            System.out.println("       ****** end for " + f);
        if (Out.verb() == 2) {
            System.out.println("       ****** Total number of atoms " + this.nbAtoms());
            System.out.println(this);
        }
    }

    ///////////////////////////////////////////////////////
    // labeling for non temporal connectors
    ///////////////////////////////////////////////////////

    // state s is labeled with the atom if the atom evaluates to true on the state
    private void labelAtom(Formula f) {
//        System.out.println("Handling atom " + f + " id " + f.id);
        for (String s : statesName) {
            HashMap<String, Integer> vals = makeMapVarValues(s);
            if (f.eval(vals) == 1) {
                states.add(s, f, TRUE);
            }
        }
    }

    // label the state with a simple formula (i.e. not temporal)
    private void handleSimpleLogic(Formula f) {
        if (f instanceof Not) {
            Formula child = ((Not) f).getRight();
            labelNot(f, child);
        } else {
            if ((f instanceof And) | (f instanceof Or)) {
                BinaryConnective bc = (BinaryConnective) f;
                Formula left = bc.getLeft();
                Formula right = bc.getRight();
                labelJunction(f, left, right, (f instanceof And));
            } else {
                if (f.isTrueFormula())
                    labelTrue();
                else {
                    System.out.println("    *****" + f.getClass().getSimpleName() + " is not yet handled");
                }
            }
        }
    }

    // handle formula true
    // all states are labeled with TRUE
    private void labelTrue() {
        for (String st : statesName)
            states.addNewLabel(st, Formula.TRUE, TRUE);
    }

    // handle not(child) => label states that are not already labeled with child
    private void labelNot(Formula f, Formula child) {
        Label couple = new Label(f, TRUE);
        for (String st : statesName) {
            ArrayList<Label> lb = states.getLabels(st);
            // no label at all, thus not(child) is TRUE
            if (lb.size() == 0) {
                lb.add(couple);
            } else {
                // st is not labeled with child, thus not(child) is TRUE
                if (!states.contains(st, child)) {
                    lb.add(couple);
                }
                // state is labeled with child, so if it is labeled under constraint cst
                // it is labeled with !child under constraint !cst
                else {
                    Disjunction cst = states.getConstraint(st, child);
                    // st is labeled with child under no constraint
                    // thus it is not labeled with !child
//                       long t = System.currentTimeMillis();
                    Disjunction neg = cst.negation();
//                        System.out.println("     ... end negation" + (System.currentTimeMillis()-t) + "ms)");
                    if (!neg.isInconsistent()) {
                        lb.add(new Label(f, neg));
                    }
                }
            }
        }
    }

    // handle phi1 op phi2 : op is either and or or
    private void labelJunction(Formula f, Formula left, Formula right, boolean isAnd) {
        for (String st : statesName) {
            // constraint of left
            int idL = left.id;
            Disjunction cstL = null;
            // constraint of right
            int idR = right.id;
            Disjunction cstR = null;
            // get the labels on st
            ArrayList<Label> lb = states.getLabels(st);
            for (Label lab : lb) {
                if (lab.ctl.id == idL) {
                    cstL = lab.constraint;
                }
                if (lab.ctl.id == idR) {
                    cstR = lab.constraint;
                }
            }
            // for AND, cstL and cstR must exist
            if (cstL != null & cstR != null & isAnd) {
                Disjunction and = cstL.andWithDisjunctionClone(cstR);
//                System.out.println("L " + cstL + " R " + cstR + " res " + and);
                if (!and.isInconsistent()) {
                    Label couple = new Label(f, and);
                    lb.add(couple);
                }
            }
            // for OR one of cstL or cstR must exist
            if ((cstL != null | cstR != null) & !isAnd) {
                Disjunction or = null;
                if (cstL != null & cstR != null)
                    or = cstL.orWithDisjunctionClone(cstR);
                else if (cstL != null)
                    or = (Disjunction) cstL.clone();
                else
                    or = (Disjunction) cstR.clone();
                if (!or.isInconsistent()) {
                    Label couple = new Label(f, or);
                    lb.add(couple);
                }
            }
        }
    }

    ///////////////////////////////////////////////////////
    // Temporal connectors
    ///////////////////////////////////////////////////////

    // handle temporal logic formulas
    // The input CTL formula has been translated using EX, EG and EU connectives
    // NOTA: since formulas are handled by levels, their children have already been model-checked
    private void handleCTL(Formula f) {
        // EX(phi)
        if (f instanceof ExistNeXt) {
            Formula phi = ((ExistNeXt) f).getLeft();
            // loop over states to label with EX(phi)
            labelExist(f, phi);
        } else {
            // E(phi U psi)
            if (f instanceof ExistUntil) {
                Formula phi = ((ExistUntil) f).getLeft();
                Formula psi = ((ExistUntil) f).getRight();
                // loop over states to label with E(phi U psi)
                labelExistUntil(f, phi, psi);
            } else {
                // EG(phi)
                if (f instanceof ExistGenerally) {
                    Formula phi = ((ExistGenerally) f).getLeft();
                    // loop over states to label with E(phi U psi)
                    labelExistGenerally(f, phi);
                } else {
                    System.out.println("    *****" + f.getClass().getSimpleName() + " is not yet handled");

                }
            }
        }
    }


    ///////////////////////////////////////////////////////
    // EX
    ///////////////////////////////////////////////////////

    // to label EX formulas f= EX(child)
    private void labelExist(Formula f, Formula child) {
        for (String from : statesName) {
            // constraints in construction
            // selfloop
            Conjunction selfLoop = new Conjunction();
            // disjunction for all possible successors
            Disjunction disjunction = new Disjunction();
            // from is already labeled with child under a consistent constraint
            // thus the edge (to,to) can be useful
            boolean selfIsUseful = states.contains(from, child);

            // seeking possible next edges in asynchronous way
            // only one variable can increase or decrease
            // look at each digit of the state name to get each variable

            // exploration of the constrained Kripke structure (cf section 6.3 of the article)
            for (int i = 0; i < from.length(); i++) {
                Para p = getApplicablePara(genes.get(i), from);
                byte appParaVar = VariableFactory.createVar(p.getSMBname(), (byte) p.minLevel(), (byte) p.maxLevel());
                byte digit = Byte.valueOf(from.substring(i, i + 1));
                String pre = from.substring(0, i);
                String post = from.substring(i + 1);
                // increase
                if (digit + 1 <= p.maxLevel()) {
                    String to = pre + (digit + 1) + post;
                    disjunction = handleEdgeEX(to, child, appParaVar, digit, disjunction, true);
                }
                // decrease
                if (digit - 1 >= p.minLevel()) {
                    String to = pre + (digit - 1) + post;
                    disjunction = handleEdgeEX(to, child, appParaVar, digit, disjunction, false);
                }
                // equal
                if (selfIsUseful) {
//                    System.out.println("from " + from + " cst " + AtomFactory.makeEqual(appParaVar, digit));
                    selfLoop = selfLoop.andWithAtom(AtomFactory.makeEqual(appParaVar, digit));
//                    selfLoop = selfLoop.makeAndAtom(AtomFactory.makeEqual(appParaVar, digit));
                }
            }
            // add the constraints of self loop if from is labeled with child
            if (selfIsUseful && !selfLoop.isInconsistent() & !selfLoop.isEmpty()) {
                disjunction = handleLoopEX(from, child, disjunction, selfLoop);
            }
            // add the overall disjunction to the constraints of state from for formula f
            // NOTA: it is sure that from was not already labeled with f
            if (!disjunction.isEmpty() && !disjunction.isInconsistent()) {
                //addDisjunction(from,f,disjunction);
                states.addNewLabel(from, f, disjunction);
            }
        }
    }

    // EX connective
    // handle edge (from,to) by adding constraint in the disjunction associated with state from
    // (from,to) is either increasing or decreasing
    private Disjunction handleEdgeEX(String to, Formula child, byte appParaVar, byte valI, Disjunction disjunction, boolean increase) {
        Disjunction toCst = states.getConstraint(to, child);
        // toCst is null if state to has not been labeled with child
        if (toCst != null) {
            Atom cst = (increase) ? AtomFactory.makeGreat(appParaVar, valI) : AtomFactory.makeLess(appParaVar, valI);
            Disjunction d = toCst.andWithAtomClone(cst);
            disjunction = disjunction.orWithDisjunction(d);
        }
        return disjunction;
    }


    // f = EX(child)
    // selfLoop is the conjunction of atoms to stay on st
    // disjunction is the disjunction under construction for f
    // PRE: state st is labeled with child under a consistent constraint
    private Disjunction handleLoopEX(String st, Formula child, Disjunction disjunction, Conjunction selfLoop) {
        Disjunction toCst = states.getConstraint(st, child);
        Disjunction d = toCst.andWithConjunctionClone(selfLoop);
        disjunction = disjunction.orWithDisjunction(d);
        return disjunction;
    }


    ///////////////////////////////////////////////////////
    // EU

    // to label Exist until formulas
    // phi = E(first U second)
    private void labelExistUntil(Formula phi, Formula first, Formula second) {
        YICES_SEMANTIC_EQ_TIME = 0;
//        Timer timer = new Timer();
//        timer.schedule(new Banner(true,phi.toString()), 0, 1000);
        // label states where second is true with E(first U second)
        initEUG(phi, second);
        nbStepsEU = 0;
//        System.out.println("after init \n" + states);
        boolean hasChanged = true;
        // continue while at least one label for a state has changed
        //    => a new state is labeled with f
        //    => a new transition satisfying f has been found for a state already labeled with f
        while (hasChanged) {// && nbStepsEU<3) {
            hasChanged = false;
            //System.out.println("step " + step);
            for (String from : statesName) {
                // check only states which are labeled with first
                // and seek if there is a successor labeled with E(first U second)
                if (states.contains(from, first)) {
                    // get constraint of first on this state
                    Disjunction firstFormula = states.getConstraint(from, first);
//                    System.out.println("from " + from + " constr " + firstFormula);
//                    System.out.println("from " + from);
                    // self-loop (from,from) could be useful if from is labeled with first
                    boolean selfIsUseful = states.contains(from, phi);
                    // constraints in construction
                    // selfloop
                    Conjunction selfLoop = new Conjunction();
                    // disjunction for all possible successors
                    Disjunction newDisj = new Disjunction();
                    // adding possible next edge in asynchronous way
                    // only one variable can increase or decrease
                    // look at each digit of the state name to get each variable
                    for (int i = 0; i < from.length(); i++) {
                        Para p = getApplicablePara(genes.get(i), from);
//                          System.out.println( "from " + from + " app para :" + p + " " + p.minLevel() + ".." + p.maxLevel());
//                         if (p.fixe)
//                             System.out.println( "from " + from + " app para :" + p + " " + p.minLevel()+ " " + p.maxLevel()) ;
                        byte appParaVar = VariableFactory.createVar(p.getSMBname(), (byte) p.minLevel(), (byte) p.maxLevel());
                        byte valI = Byte.valueOf(from.substring(i, i + 1));
                        String pre = from.substring(0, i);
                        String post = from.substring(i + 1);
                        // increase
                        if (!newDisj.isTrue() & valI + 1 <= p.maxLevel()) {
                            //System.out.println("increase");
                            String to = pre + (valI + 1) + post;
//                            if (from.equals("11101"))
//                                System.out.println(" (from,to) increase " + from + " " + to  + " new Disj bef " + newDisj );
                            newDisj = handleEdgeEU(to, phi, firstFormula, appParaVar, valI, newDisj, true);
//                           if (from.equals("11101"))
//                            System.out.println("                   after  " + newDisj);
                        }
                        // decrease
                        if (!newDisj.isTrue() & valI - 1 >= p.minLevel()) {
                            //System.out.println("decrease");
                            String to = pre + Integer.valueOf(valI - 1) + post;
//                            if (from.equals("11101"))
//                                System.out.println(" (from,to) decrease " + from + " " + to + " new Disj bef " + newDisj);
                            newDisj = handleEdgeEU(to, phi, firstFormula, appParaVar, valI, newDisj, false);
//                            if (from.equals("11101"))
//                                System.out.println("                   after  " + newDisj);
                        }
                        // equal
                        if (!newDisj.isTrue() & selfIsUseful) {
//                            System.out.println(" (from,from) " + from + " "  + " self bef " + selfLoop );
//                            if (p.fixe)
//                                System.out.println("   equal " + valI + " " + AtomFactory.makeEqual(appParaVar, valI));
                            selfLoop = selfLoop.andWithAtom(AtomFactory.makeEqual(appParaVar, valI));
//                            if (p.fixe)
//                                System.out.println("                   after  " + selfLoop + " " + selfLoop.isInconsistent());
                        }
                    }
                    // add the constraints of self loop
                    if (!newDisj.isTrue() & selfIsUseful && !selfLoop.isInconsistent() & !selfLoop.isEmpty())
                        newDisj = handleLoopEU(from, phi, firstFormula, selfLoop, newDisj);
                    // add the overall disjunction to the constraint of state from for formula f
                    boolean changed = addAndTestDisjunction(from, phi, newDisj);
//                    if (changed) System.out.print(from + " ");
                    hasChanged = hasChanged | changed;

//                    else
//                        System.out.println("disjunction is empty");
//                    System.out.println("              /// end label state " + phiDisj);
                }
//                System.out.println("end states");
            }
            if (Out.verb() >= 1) {
                System.out.println("              /// EU end step " + nbStepsEU);
                System.out.println("              /// Yices time: " + YICES_SEMANTIC_EQ_TIME + "ms");
            }
            if (Out.verb() == 3)
                System.out.println("              " + this);
            nbStepsEU++;
        }
        YICES_SEMANTIC_EQ_TOTAL_TIME += YICES_SEMANTIC_EQ_TIME;
//        System.out.println();
//        timer.cancel();
    }

    // f is E (first U second)
    // or EG(second)
    // initialization step : each state already labeled with second is also labeled with f
    private void initEUG(Formula f, Formula second) {
        for (String st : statesName) {
            if (states.contains(st, second) & !states.contains(st, f)) {
                states.addNewLabel(st, f, states.getConstraint(st, second));
            }
        }
        if (Out.verb() >= 2) {
            System.out.println("States after init EU");
            System.out.println(this);
        }
    }

    // f = EG(child)
    // label with (f,INIT_EG) all states which are labeled with child
    private void initEG(Formula f, Formula child) {
        for (String st : statesName) {
            if (states.contains(st, child) & !states.contains(st, f)) {
                states.addNewLabel(st, f, states.getConstraint(st, child));
            }
        }
        if (Out.verb() >= 2) {
            System.out.println("States after init EG");
            System.out.println(this);
        }
    }


    //  Add to disjunction newDisj the label phi = E(first U second) where (from,to) is an edge s.t
    //      - first is true on from according firstCst constraint
    //      - (from,to) is an in(de)creasing edge (i.e. variable I in(de)creased between from and to)
    //  Nota: during init phase, states where second is true have been labeled with phi
    private Disjunction handleEdgeEU(String stateTo, Formula phi, Disjunction firstCst, byte appParaVar, byte valI, Disjunction newPhi, boolean increase) {
//        System.out.println(" edge EU  to " + stateTo + " phi " + phi);
        // state to must be labeled with phi, otherwise, edge (from,to) is not useful
        Disjunction cstForPhiOnTo = states.getConstraint(stateTo, phi);
        if (cstForPhiOnTo != null) {
            // the constraint on the edge
            Atom edgeFormula = (increase) ? AtomFactory.makeGreat(appParaVar, valI) : AtomFactory.makeLess(appParaVar, valI);
            // when the applicable para is fixed, the atom may be inconsistent
            //    => ignore this edge
//            System.out.println("       edge formule " + edgeFormula + " " + increase + " " + VariableFactory.getName(appParaVar) + " " + valI);

            if (edgeFormula.isInconsistent()) {
                return newPhi;
            }
            // to take edge (from,to) the conjunction of the constraint firstCst on from, and the
            // constraint on the edge must be true
            Disjunction arcAndFirstFormula = firstCst.andWithAtomClone(edgeFormula);
//            if (firstCst == null)
//                arcAndFirstFormula = new Disjunction(edgeFormula);
//            else

//            System.out.println("arc and first " + arcAndFirstFormula);
            // and this must also be compatible with the constraint phi on state to
            Disjunction res = arcAndFirstFormula.andWithDisjunctionClone(cstForPhiOnTo);
//            if (stateTo.equals("11") ) {
//                System.out.println("   Arc cst " + edgeFormula);
//                System.out.println("   edge cst " + res);
//                System.out.println("   newPhi " + newPhi);
//                System.out.println("   to cst " + states.getLabels(stateTo));
//            }
            // this is added to the disjunction under construction
//            System.out.println("    res " + res + " newPhi before " + newPhi);

            newPhi = newPhi.orWithDisjunction(res);
//            System.out.println("    newPhi after " + newPhi);

//            System.out.println("   to " + stateTo + " increase " + " phiFrom " + cstForPhiOnTo);
//            System.out.println("   end handleEU " +res);
        }
        return newPhi;
    }


    // PRECOND : from contains phi
    private Disjunction handleLoopEU(String from, Formula phi, Disjunction firstFormula, Conjunction self, Disjunction newPhi) {
        Disjunction phiFormula = states.getConstraint(from, phi);
        // to take edge (from,from) the conjunction of the constraint firstFormula on from, and the
        // constraint on the edge must be true
        Disjunction arcAndFirstFormula = firstFormula.andWithConjunctionClone(self);
        // and this must also be compatible with the constraint phi on state from
        Disjunction res = arcAndFirstFormula.andWithDisjunctionClone(phiFormula);
        // this is added to the disjunction under construction
        newPhi = newPhi.orWithDisjunction(res);
//            System.out.println("handleLoopEU  adding " + res + " to state " + from);
        return newPhi;
    }


//    private void handleLoopEU(String st, Formula f, Disjunction disjunction, Conjunction selfLoop){
////
////                        System.out.println("st " + st + "self " + selfLoop);
//        Disjunction toCst = states.getConstraint(st, f);
//         // toCst is null if state "st" has not been labeled with formula
//        if (toCst != null) {
//            Disjunction d = toCst.makeAndConjunction(selfLoop);
//            System.out.println("to " + st + " " +toCst + " self " + selfLoop + " d " + d);
//            disjunction.addOrDisjunction(d);
//        }
//    }


    ///////////////////////////////////////////////////////
    // EG
    // label EG formulas f = EG(child)
    // Principle:
    //    - start by assuming that states labeled with child are also labeled with EG(child),
    //      with constraint EG_CST_INIT
    //    - fixed point algo so must stop when nothing change. Thus states can be searched in any order.
    //    - to know if EG is valid, one only needs to look at successors, prefixes have no impact.
    //    - build the disjunction for all the successors "to" of state "from". Edge (from,to) gives a new option if
    //      the conjunction of constraint of child on "from", constraint on the edge (from,to) and constraint of
    //      EG(child) on "to" is satisfiable
    //    - at each step of the fixed point algo, if a state must be labeled with an inconsistent disjunction,
    //      that means that each possible option is false and that this state was supposed to be labeled with EG
    //      but in fact, it is not true => remove the EG label for this state
    // NOTA: if a disjunction is unsatisfiable, that means that each conjunction it contains is unsatisfiable
    //       => no successor is OK => EG is false
    // NOTA: in the parametric state transition graph, each state has at least one successor, itself (self-loop)
    //       or another state. So if a state remains labeled with (EG(child), EG_CST_INIT) that means that
    //       no successor was OK to label it with EG. In other words, there is no leaf that would have been labeled with
    //       (EG (child), EG_CST_INIT) during the initialization step and should remain tagged with it
    //       after the fixed point has been reached
    ///////////////////////////////////////////////////////
    private void labelExistGenerally(Formula f, Formula child) {
        YICES_SEMANTIC_EQ_TIME = 0;
        // label with (f,TRUE) all states which are labeled with child
        initEG(f, child);
        nbStepsEG = 0;
//        System.out.println("after init EG " + states);
        ////////////////////////////////////////
        // label with EG until nothing change
        ///////////////////////////////////////
        boolean hasChanged = true;
        // continue while at least one label for a state has changed
        //    => a new state is labeled with f
        //    => a new transition satisfying f has been found for a state already labeled with f
        while (hasChanged) {// && nbStepsEU<3) {
            hasChanged = false;
            //System.out.println("step " + step);
            for (String from : statesName) {
                // check only states which are labeled with f
                // after init, there are states where child is true
                // after loop step i>0, there are states s.t.
                //     - child is true and nothing has changed => labeled with (EG(child),fromChildCst)
                //     - child is true and a new possibility has been found => labeled with (EG(child),newDisj_i)
                // states that were labeled with (EG(child),fromChildCst) or with (EG(child),newDisj_i) at step i
                // s.t. all outgoing edges do not statisfy EG(child) at step (i+1)
                // (i.e newDisj_(i+1) is inconsistent) are no more labeled with EG(child)
//                System.out.println("from " + from);
                if (states.contains(from, f)) {
                    //                System.out.println(" first " + first.toStringWithId());
                    // get constraint of child on this state
                    Disjunction fromChildCst = states.getConstraint(from, child);
//                    System.out.println("from " + from + " constr " + firstFormula);
//                    System.out.print( from + "  ");
                    // constraints in construction
                    // selfloop
                    // NOTA: the self-loop is always potentially useful
                    Conjunction selfLoop = new Conjunction();
                    // disjunction for all possible successors
                    Disjunction newDisj = new Disjunction();
                    // adding possible next edge in asynchronous way
                    // only one variable can increase or decrease
                    // look at each digit of the state name to get each variable
                    for (int i = 0; i < from.length(); i++) {
                        Para p = getApplicablePara(genes.get(i), from);
//                        System.out.println( "from " + from + " app para :" + p + " " + p.minLevel() + ".." + p.maxLevel());
                        byte appParaVar = VariableFactory.createVar(p.getSMBname(), (byte) p.minLevel(), (byte) p.maxLevel());
                        byte valI = Byte.valueOf(from.substring(i, i + 1));
                        String pre = from.substring(0, i);
                        String post = from.substring(i + 1);
                        // increase
                        if (!newDisj.isTrue() & valI + 1 <= p.maxLevel()) {
                            //System.out.println("increase");
                            String to = pre + (valI + 1) + post;
//                            System.out.println(" (from,to) increase " + from + " " + to  + " new Disj bef " + newDisj );
                            newDisj = handleEdgeEG(to, f, fromChildCst, appParaVar, valI, newDisj, true);
//                            System.out.println("                   after  " + newDisj);
                        }
                        // decrease
                        if (!newDisj.isTrue() & valI - 1 >= p.minLevel()) {
                            //System.out.println("decrease");
                            String to = pre + Integer.valueOf(valI - 1) + post;
//                            System.out.println(" (from,to) decrease " + from + " " + to  + " new Disj bef " + newDisj );
                            newDisj = handleEdgeEG(to, f, fromChildCst, appParaVar, valI, newDisj, false);
//                            System.out.println("                   after  " + newDisj);
                        }
                        // equal
                        if (!newDisj.isTrue())
                            selfLoop = selfLoop.andWithAtom(AtomFactory.makeEqual(appParaVar, valI));

                        //System.out.println("end");
                    }
                    // add the constraints of self loop
                    if (!newDisj.isTrue() & !selfLoop.isInconsistent() & !selfLoop.isEmpty()) {
//                        System.out.println(" (from,from) " + from + " "  + " self bef " + selfLoop );
                        newDisj = handleLoopEG(fromChildCst, selfLoop, newDisj);
//                        System.out.println("                   after  " + newDisj);
                    }

                    boolean changed = removeOrReplace(from, f, newDisj);
//                    if (changed) System.out.print(from + " ");
                    hasChanged = hasChanged | changed;

//                    else
//                        System.out.println("disjunction is empty");
//                    System.out.println("              /// end label state " + phiDisj);
                }
//                System.out.println("end states");
            }
            if (Out.verb() >= 1) {
                System.out.println("\n            /// EG end step " + nbStepsEG);
                System.out.println("              /// Yices time: " + YICES_SEMANTIC_EQ_TIME + "ms");
//                System.out.println("              /// nb atoms: " + nbAtoms());
//                System.out.println("              /// nb product: " + Disjunction.product_nb + " time for product " + Clock.print(Disjunction.product_time));
            }
            //            System.out.println("              /// Labeled states:\n" + states);
            nbStepsEG++;
        }
        YICES_SEMANTIC_EQ_TOTAL_TIME += YICES_SEMANTIC_EQ_TIME;

        /////////////////////////////////////////
        // Remove EG(child) from states st where initial constraint (i.e. built by init step)
        // has not changed
        // That means that there was no outgoing edge from st that reaches a state to where EG(child)
        // is true and the conjunction of
        //         - constraints of child on st,
        //         - constraint on the edge (st,to)
        //         - constraint of EG(child) on to is satisfiable
        /////////////////////////////////////////
        //states.removeInitEG(f.id);
    }


    // from is a state, f the formula EG(child) and newDisj is the new way
    // to label from with (f,newDisj) at one step of the fixed point algo
    // if newDisj is inconsistent, that means that the initialization was false
    // (i.e. when considering the constraints of edges and states, the conjunctions are false)
    // NOTA: if a disjunction is unsatisfiable, that means that each conjunction it contains is unsatisfiable
    //       => no successor is OK => EG is false
    private boolean removeOrReplace(String from, Formula f, Disjunction newDisj) {
        // the new possibility is not satisfiable (syntactically)
        if (newDisj.isInconsistent() || newDisj.isEmpty()) {
//            System.out.println("  *** EG remove : inconsistent " + newDisj);
            if (Out.verb() == 2)
                System.out.println("  *** EG remove : inconsistent " + newDisj);
            states.remove(from, f.id);
            return true;
        }
        // test if it is satisfiable semantically
        if (!yices.testSatisfiability(newDisj)) {
//            System.out.println("   *** EG remove : semantically unsatisfiable " + newDisj);
            if (Out.verb() == 2)
                System.out.println("   *** EG remove : semantically unsatisfiable " + newDisj);
            states.remove(from, f.id);
            return true;
        }
        // there is at least one edge (from,to) which validates EG(child) so the label EG must be kept on from
        // add the overall disjunction to the constraint of state from for formula f
        return replaceDisjunctionForEG(from, f, newDisj);
    }


    // phiEG = EG(child)
    // look at an edge (from,to) where:
    //      - child is true on from according to constraint childCst
    //      - phiEG is true on to according to constraint cstForEGOnTo
    // add the option (childCst and edgeFormula and cstForEGOnTo) if it is syntactically satisfiable
    private Disjunction handleEdgeEG(String stateTo, Formula phiEG, Disjunction childCst, byte appParaVar, byte valI, Disjunction newPhi, boolean increase) {

        //        System.out.println(" edge EG  to " + stateTo + " child cst " + childCst);
        // state to must be labeled with phiEG, otherwise, edge (from,to) is not useful
        Disjunction cstForEGOnTo = states.getConstraint(stateTo, phiEG);
        if (cstForEGOnTo != null) {
            // the constraint on the edge
            Atom edgeFormula = (increase) ? AtomFactory.makeGreat(appParaVar, valI) : AtomFactory.makeLess(appParaVar, valI);
            // when the applicable para is fixed, the atom may be inconsistent
            //    => ignore this edge
            if (edgeFormula.isInconsistent())
                return newPhi;
            // to take edge (from,to) the conjunction of the constraint phiEG on from, and the
            // constraint on the edge must be true
            Disjunction arcAndPhiFormula = childCst.andWithAtomClone(edgeFormula);
//            if (!arcAndPhiFormula.isInconsistent()) {
//            if (firstFormula == null)
//                arcAndFirstFormula = new Disjunction(edgeFormula);
//            else
//            arcAndFirstFormula = firstFormula.makeAndAtom(edgeFormula);
//            System.out.println("arc and first " + arcAndFirstFormula);
            // and this must also be compatible with the constraint phi on state to
            Disjunction res = arcAndPhiFormula.andWithDisjunctionClone(cstForEGOnTo);
//            if (stateTo.equals("11") ) {
//                System.out.println("   Arc cst " + edgeFormula);
//                System.out.println("   edge cst " + res);
//                System.out.println("   newPhi " + newPhi);
//                System.out.println("   to cst " + states.getLabels(stateTo));
//            }
            // this is added to the disjunction under construction
            newPhi = newPhi.orWithDisjunction(res);

//                System.out.println("   to " + stateTo + " increase " + " phiFrom " + cstForSecondOnTo);
//                System.out.println("   end handleEG " + res);
//            }
//            else System.out.println(" edge    to " + stateTo + " impossible for EG");
        }
        return newPhi;
    }


    // loop for EG(child)
    // PRECOND : from contains child
    private Disjunction handleLoopEG(Disjunction childCst, Conjunction selfEdgeCst, Disjunction newPhi) {
//        System.out.println("handleLoop child " + childCst + " self " + selfEdgeCst);

        Disjunction arcAndChildCst = childCst.andWithConjunctionClone(selfEdgeCst);
        // this is added to the disjunction under construction
//        if (!arcAndChildCst.isInconsistent() && !arcAndChildCst.isEmpty()) {
        newPhi = newPhi.orWithDisjunction(arcAndChildCst);
//            System.out.println("handleLoopEU  adding " + res + " to state " + from);
//        }
        return newPhi;
    }


    ////////////////////////////////////////////////////////////////////////////////
    // methods for testing termination of the loops for EG and EU
    // cf 4.5 and 4.6 in the article
    // first syntactic tests are done and only if needed, semantic test using Yices are done
    ////////////////////////////////////////////////////////////////////////////////


    // stop condition for EU labeling
    /////////////////////////////////
    // phi is EU(f1,f2), newDisj is a new option computed when seeking states
    // add (or create) the disjunction (oldDisj or newDisj) where
    //     oldDisj is the current constraint associated with phi for state s
    //     newDisj is a new possible option discovered when seeking the states
    // return true if (oldDisj or newDisj) is semantically different from oldDisj
    //        => label of state s has changed
    //        => loop of labeling for EU must be done one more time
    public boolean addAndTestDisjunction(String s, Formula phi, Disjunction newDisj) {

        // newDisj is not a useful option
        if (newDisj.isEmpty() || newDisj.isInconsistent()) {
            return false;
        }

        // s was not already labeled with phi => add the new disjunction
        Label lab = states.getLabel(s, phi);
        if (lab == null) {
            states.addNewLabel(s, phi, newDisj);
            return true;
        }

        ///////////////////////////////////////////////////////////////////////////
        // if s was already labeled with f check if this possibility gives new values
        // if yes, add it as a disjunction

        // constraint for phi
        Disjunction oldf = lab.constraint;

        // if s has already been labeled with phi under constraint TRUE, it is not necessary to
        // do something, keep TRUE
        if (oldf.isTrue()) {
            return false;
        }

        // syntactic equality between the old constraint and the new option
        /////////////////////
        if (oldf.equals(newDisj)) {
            if (Out.verb() == 2) {
                System.out.println("   !!! Constraints old and newDisj are syntactically equals");
                System.out.println("       old " + oldf + " new " + newDisj);
            }
            return false;
        }

        // compute the new disjunction by adding the new possibility newDisj
        Disjunction newf = oldf.orWithDisjunctionClone(newDisj);

        // after simplification, oldf and newf are syntactically equals
        if (oldf.equals(newf)) {
            if (Out.verb() == 2) {
                System.out.println("   !!! Constraints oldf and (oldf | newDisj) are syntactically equals");
                System.out.println("       old " + oldf + " new " + newf);
            }
            return false;
        }

        // newf may have been simplified to true
        if (newf == TRUE) {
            states.addNewLabel(s, phi, TRUE);
            return true;
        }

        //////////////////////////////////////
        // here test of semantic equality is necessary
        // test the semantic inclusion of the solutions of newf and oldf
        //    * if newf is included in oldf and oldf is included in newf they are semantically equal => keep oldf
        //    * else :
        //         if newf is included in oldf, keep oldf
        //         if oldf is included in newf, change oldf by newf
        //////////////////////////////////////

        ///////////////// si on veut la version inclusion avec MDDs
        long t2 = System.currentTimeMillis();
        HashMap<String, int[]> usedVars = oldf.usedVarsAndDomains();
        usedVars.putAll(newDisj.usedVarsAndDomains());
        MDDSolver mddSolver = new MDDSolver(usedVars);
        int inclus = mddSolver.testInclusion(oldf, newDisj);


//////////////// si on veut la version avec test inclusion old et new via yices. Semble bien marcher
//        long t2 = System.currentTimeMillis();
//        int inclus = yices.testInclusion(oldf, newDisj);
        YICES_SEMANTIC_EQ_TIME += (System.currentTimeMillis() - t2);
//        System.out.println("inclusion " + inclus);// + " " + (System.currentTimeMillis() - t3) + "ms");
//
//        if (inclus==OLD_IN_NEW)
//            System.out.println("       old " + oldf + " new " + newDisj);
//


        // oldf is included in newf => keep newDisj
        if (inclus == OLD_IN_NEW) {
            lab.constraint = newDisj;
            if (Out.verb() == 2) {
                System.out.println("   !!! addAndTest: use new");
                System.out.println("       old " + oldf + " new " + newDisj);
            }
            return true;
        }
        // there is no inclusion => keep the disjunction
        if (inclus == NEW_DIFF_OLD) {
            if (Out.verb() == 2) {
                System.out.println("   !!! addAndTest: make the disjunction");
                System.out.println("       old " + oldf + " new " + newDisj + " disjunction " + newf);
            }
            lab.constraint = newf;
            return true;
        }

        // here new is included in old, or semantically equals to old, no change
        if (Out.verb() == 2) {
            System.out.println("   !!! addAndTest: keep old, don't change");
            System.out.println("       old " + oldf + " new " + newDisj);
        }
        return false;
    }

    // stop condition for EG labeling
    /////////////////////////////////
    // phi is EG(f), newDisj is a new possibility computed when seeking the states
    // on state s, replace the constraint associated with phi by constraint newDisj
    // PRE: s is already labeled with phi and newDisj is consistent
    // POST: if newDisj is semantically different than the current constraint for phi,
    //       replace the current constraint by newDisj
    // return : true if the constraint for phi on s has changed, false otherwise
    public boolean replaceDisjunctionForEG(String s, Formula phi, Disjunction newDisj) {

        // get the label associated with phi
        Label lab = states.getLabel(s, phi);

        // get the old constraint associated with phi
        Disjunction oldPhiCst = lab.constraint;

        // syntactic equality => don't change
        /////////////////////
        if (oldPhiCst.equals(newDisj)) {
            if (Out.verb() == 2)
                System.out.println("   *** Constraints are syntactically equals");
            return false;
        }

        // test of semantic equality is needed
        //////////////////////////////////////
        long t1 = System.currentTimeMillis();
        boolean test = yices.testEquality(oldPhiCst, newDisj);
        long t2 = System.currentTimeMillis();
        YICES_SEMANTIC_EQ_TIME += t2 - t1;
        // if they are not semantically equal, change the label
        if (!test) {
            lab.constraint = newDisj;
            if (Out.verb() == 2)
                System.out.println("   ***  EG changed, new label  " + newDisj);
            return true;
        }

        if (Out.verb() == 2)
            System.out.println("   *** EG state " + s + " NOT changed (semantic)");
        return false;
    }


    //////////////////////////////////////////////////////
    // TOOLS
    /////////////////////////////////////////////////////

    // to build a Formula Or (or And) from a list of formulas
    private Formula makeJunction(ArrayList<Formula> d, boolean isDisjunct) {
        if (d.size() == 1)
            return d.get(0);
        Formula junc;
        int i;
        if (d.size() % 2 == 0) {
            junc = (isDisjunct) ? new Or(d.get(0), d.get(1)) : new And(d.get(0), d.get(1));
            i = 2;
        } else {
            junc = d.get(0);
            i = 1;
        }
        while (i < d.size()) {
            junc = (isDisjunct) ? new Or(junc, d.get(i)) : new And(junc, d.get(i));
            i++;
        }
        return junc;
    }

    private Formula makeConjunction(ArrayList<Formula> d) {
        return makeJunction(d, false);
    }

    private Formula makeDisjunction(ArrayList<Formula> d) {
        return makeJunction(d, true);
    }


    // to build the map that associates to each Gene name its value on state s
    // used by eval in class Formula
    private HashMap<String, Integer> makeMapVarValues(String s) {
        HashMap<String, Integer> vals = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            vals.put(mapIndexVar.get(i), Integer.valueOf(s.substring(i, i + 1)));
        }
        for (Var v : net.getEnvVar()) {
            vals.put(v.name, v.level);
        }
        return vals;

    }


    /////////////////////////////////////////////
    // constraint solving
    // use MDDs to represent the solutions associated with a constraint
    /////////////////////////////////////////////


    // to know if the whole system is SAT
    // use to check coherence between MDDs set and Yices
    public boolean isSat() throws Exception {
        HashMap<String, Disjunction> toSolve = states.getStatesAndConstraints(ctlToModelCheck);
        if (toSolve.isEmpty()) {
            System.out.println("No state is labeled with " + ctl);
            return false;
        }
        if (toSolve.size() != statesName.size()) {
            System.out.println("The formula is not true on each initial state");
            System.out.println(toSolve.size() + " states among " + statesName.size() +
                    " have been labeled with " + ctl);
            return false;
        }
        YicesLauncher yl = new YicesLauncher(path, net);
        yl.writeHeader();
        for (String st : toSolve.keySet()) {
            Disjunction cst = toSolve.get(st);
            if (!cst.isEmpty()) {
                yl.writeOneAssert(cst);
            } else {
            }
        }
        yl.close();
        return yl.yicesIsSAT();
    }


    // to print the solutions associated with the constraints of all the states
    public void printSolutions(boolean inCSV) throws Exception {
        // number of states which have been labeled with ctlToModelCheck
        int nbLabeledStates = states.nbStatesLabeledWith(ctlToModelCheck);
        // a ack to be able to print csv even if there is no solution
        boolean hasSolution = true;
//        System.out.println("\n***  Parameter settings which validate " + ctl + "  ***\n");
        // all the states must have been labeled with ctlToModelCheck
        if (nbLabeledStates == 0) {
            System.out.println("*** No states are labeled with the property, " +
                    "there is no setting such that the property is true on one of the states of " +
                    "the induced Kripke structure");
            hasSolution = false;
        } else {
            System.out.println("*** " + nbLabeledStates + " states among " + statesName.size() +
                    " have been labeled with the formula");
            if (nbLabeledStates != statesName.size()) {
                System.out.println("*** Some states are not labeled, " +
                        "there is no setting such that the property is true on each of the states of " +
                        "the induced Kripke structure");
                hasSolution = false;
            }
        }
        // all states have been labeled with ctlToModelCheck, solve the constraint system
        // the list of disjunction to solve
        ArrayList<Disjunction> toSolve = toSolve(hasSolution);
//                printSolutionIsSat(toSolve);
        // build the MDDsolver in charge of computing the solutions
        // here only variables used in the constraint to solve are used
        HashMap<String, int[]> allDomains = allParaDomains();
        MDDSolver mddSolver = new MDDSolver(usedVarsAndDomain(allDomains));
        if (inCSV)
            mddSolver.printSolutionsInCsv(allDomains, toSolve, unused());
        else
            if (hasSolution)
                mddSolver.printSolutions(toSolve);

    }


    // to get the variables which are effectively used in the labels of ctlToModelCheck
    private HashMap<String,int[]> usedVarsAndDomain(HashMap<String,int[]> allDomains) {
        HashMap<String,int[]> usedVarsAndDomains = new HashMap<String,int[]>();
        for (String s : usedVarsToSolve) {
            usedVarsAndDomains.put(s,allDomains.get(s));
        }
        return usedVarsAndDomains;
    }

    // to get the variables which are not involved in the constraints to solve
    private HashSet<String> unused() {
        HashSet<String> paras = new HashSet<>();
        // parameters which are candidate to be used : not fixed and more than one possible value
        for (Gene g : this.net.genes()) {
            for (Para p : g.getParas()) {
                paras.add(p.getSMBname());
            }
        }
        HashSet<String> unused = new HashSet();
        for (String p : paras) {
            if (!usedVarsToSolve.contains(p))
                unused.add(p);
        }
        return unused;
    }

    // tool for checking coherence vs Yices
    private void printSolutionIsSat(ArrayList<Disjunction> toSolve) {
        System.out.println("//////////// check if solution is SAT //////////");
        System.out.println("constraints to solve " + toSolve);
        Disjunction res= new Disjunction();
        for (Disjunction d : toSolve) {
            if (!yices.testSatisfiability(d))
                System.out.println("////////// " + d + " is UNSAT");
            res = res.andWithDisjunctionClone(d);
            System.out.println("d " + d + "RES " + res);
        }
        System.out.println("Conjunction of constraint of all states to be solved: " + res);
        System.out.println("is SAT ? " + yices.testSatisfiability(res));

    }

    // to get all the paras with their domains
    // useful to be able to compare the csv with other csv obtained by calling TotemBioNet
    private HashMap<String,int[]> allParaDomains() {
        HashMap<String,int[]> allParaNames = new HashMap<>();
        for (Gene g : genes) {
            if (!net.isEnvVar(g)) {
                for (Para p : g.getParas())
                    allParaNames.put(p.getSMBname(),new int[] {p.minLevel(),p.maxLevel()});
            }
        }
        return allParaNames;
    }

    // return the list of non empty, non true and non redundant constraints that must be checked
    private ArrayList<Disjunction> toSolve(boolean hasSolution) {
        if (!hasSolution)
            return null;

        ArrayList<Disjunction> res = new ArrayList<>();

        if (Out.verb()==3) {
            System.out.println("    --- Using Yices to remove redundant constraints");
        }

        for (String st : states.getStates()) {
            Disjunction disj = states.getConstraint(st, ctlToModelCheck);
            if (!disj.isEmpty() && !disj.isTrue()) {
                if (!containsDisj(res, disj)) {
                    res.add(disj);
                    usedVarsToSolve.addAll(disj.varNames());
                }
            }
            else
                NB_REMOVE_TRUE++;

        }
        System.out.println("    " + NB_REMOVE_TRUE + " disjunctions are labeled with TRUE.");
        System.out.println("    " + (NB_REMOVE_EQUALS + NB_REMOVE_YICES) + " disjunctions have been removed because they are redundant.");
        if (Out.verb()==3) {
            System.out.println("    " + NB_REMOVE_YICES + " over " + NB_EQ_YICES + " calls to yices removed some disjunctions.");
            System.out.println("--- Total time for yices " + Clock.print(TIME_EQ_YICES));
        }
        return res;
    }

    // to add d in the list list only if it is not yet inside
    // with verbose level 3, semantic equality is used
    private boolean containsDisj(ArrayList<Disjunction> list, Disjunction d) {
        for (Disjunction disj : list) {
            if (disj.equals(d)) {
                NB_REMOVE_EQUALS++;
                return true;
            }
            if (Out.verb()==3) {
                long t1 = System.currentTimeMillis();
                NB_EQ_YICES++;
                if (yices.testEquality(d, disj)) {
                    NB_REMOVE_YICES++;
                    return true;
                }
                TIME_EQ_YICES += (System.currentTimeMillis() - t1);
            }
        }
        return false;
    }


    //////////////////////////////////////
    // printing
    /////////////////////////////////////


    public String toString() {
        String s = "States : \n" + states + "\n";
        return s;
    }

    public int nbAtoms() {
        return states.nbAtoms();
    }

}

