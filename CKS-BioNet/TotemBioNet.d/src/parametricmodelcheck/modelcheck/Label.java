package parametricmodelcheck.modelcheck;

import back.logic.Formula;
import parametricmodelcheck.solver.Disjunction;

/**
 * to label state with a couple (formula,constraint)
 *
 * a list of Labels are associated with states in class LabeledStates
 * @author Helene Collavizza
 */
public class Label implements Comparable {
    // the ctl formula which is satisfied on the state
    Formula ctl;
    // constraint on the parameters to satisfy formula
    Disjunction constraint;

    public Label(Formula ctl,Disjunction cst) {
        this.ctl = ctl;
        this.constraint = cst;
    }

    boolean isLabeledWith(int id) {
        return ctl.id==id;
    }

    void setConstraint(Disjunction c) {
        constraint=c;
    }

    public String toString() {
        return "label {" + ctl.toStringWithId() + "," + constraint + "}";
    }

    @Override
    public int compareTo(Object o) {
        Label that = (Label)o;
        if (this.ctl==null) {
            if (that.ctl == null)
                return 0;
            return -1;
        }
        return this.ctl.compareTo(that.ctl);
    }
}
