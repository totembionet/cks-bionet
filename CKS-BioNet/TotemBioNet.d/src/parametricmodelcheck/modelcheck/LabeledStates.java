package parametricmodelcheck.modelcheck;

import back.logic.Formula;
import parametricmodelcheck.solver.Disjunction;
import parametricmodelcheck.solver.FormulaFactory;
import util.Out;
import util.jlist.OrderedArrayList;

import java.util.*;

/**
 * Class to associate a list of couples (CTL formula,constraint) to a state
 *
 * Nota : CTL formulas and constraints are Formula => an array is enough to represent the couple
 *
 * @author Helene Collavizza
 */
public class LabeledStates {

    HashMap<String,ArrayList<Label>> states;

    public LabeledStates() {
        this.states=new HashMap<>();
    }

    void init(String s) {
        states.put(s,new OrderedArrayList<Label>());
    }

    // to be able to transverse all states
    public Set<String> getStates() {
        return states.keySet();
    }

    // add formula f to formulas of state s if it doesn't already exist
    void add(String s, Formula f, Disjunction cst) {
        ArrayList<Label> labels = states.get(s);
        Label lb = new Label(f, cst);
        if (labels.size() == 0)
            labels.add(lb);
        else {
            boolean add = true;
            for (Label lab : labels) {
                if (lab.isLabeledWith(f.id)) {
                    add = false;
                }
            }
            if (add)
                labels.add(lb);
        }
    }

    // add a formula to labels of state s
    // PRECOND: it is sure that s has not already been labeled with f
    void addNewLabel(String s, Formula f, Disjunction cst) {
        ArrayList<Label> labels  = states.get(s);
        Label lb = new Label(f, cst);
        labels.add(lb);
    }

    ArrayList<Label> getLabels(String s) {
        return states.get(s);
    }

    // to know if state s is already labeled with f
    boolean contains(String s,Formula f){
        ArrayList<Label> labels = states.get(s);
        Iterator iter = labels.iterator();
       while (iter.hasNext()){
            Label l = (Label) iter.next();
            if (l.isLabeledWith(f.id)) {
                return true;
            }
        }
        return false;
    }

    // to get a formula. Return null if doesn't exist
    // used to get the constraint of the formula of a successor of a node during CTL model-checking
    // return null if not found to avoid to transverse twice the list of formulas
    Disjunction getConstraint(String s,Formula f){
        ArrayList<Label> labels  = states.get(s);
        Iterator iter = labels.iterator();
        while (iter.hasNext()){
            Label l = (Label) iter.next();
            if (l.isLabeledWith(f.id))
                return l.constraint;
        }
        return null;
    }

    // return the label (f,constraint) is s is labeled with f
    // return null otherwise
   Label getLabel(String s, Formula f){
        ArrayList<Label> labels = states.get(s);
        Iterator iter = labels.iterator();
        while (iter.hasNext()){
            Label l = (Label) iter.next();
            if (l.isLabeledWith(f.id))
                return l;
        }
        return null;
    }

    // to get a map <state,constraints> for all states which are labeled with f
    public HashMap<String,Disjunction> getStatesAndConstraints(Formula f) {
        HashMap<String, Disjunction> cst = new HashMap<>();
        for (String st : states.keySet()) {
            Disjunction c = getConstraint(st, f);
            if (c!=null &&! c.isInconsistent())
                cst.put(st, c);
        }
        return cst;
    }

    // to get the number of states which are labeled with f
    public int nbStatesLabeledWith(Formula f) {
        int nb=0;
        for (String st : states.keySet()) {
            Disjunction c = getConstraint(st, f);
            if (c!=null &&! c.isInconsistent())
                nb++;
            if (c==null)
                System.out.println("   " + st + " is not labeled");
        }
        return nb;
    }

    // remove the formulas with id in toRemove from each states
    public int remove(HashSet<Integer> idToRemove) {
        int nbRemoved=0;
        for (int id : idToRemove) {
            nbRemoved += remove(id);
        }
        return nbRemoved;
    }

    // remove the formula with identifier id from state st
    public void remove(String st,int id) {
        ArrayList<Label> labels = states.get(st);
        Iterator<Label> iter = labels.iterator();
        while (iter.hasNext()) {
            Label lab = iter.next();
            if (lab.isLabeledWith(id)) {
                iter.remove();
                // if found, stop because there is at most one label by id
                return;
            }
        }
    }

    // remove formulas with ident id to all the states
    public int remove(int id) {
        if (Out.verb()==2)
            System.out.println("     Removing formula " + FormulaFactory.getFormula(id));
        int nbRemoved=0;
        for (String s : getStates()) {
            ArrayList<Label> labels = states.get(s);
            Iterator<Label> iter = labels.iterator();
            while (iter.hasNext()) {
                Label lab = iter.next();
                if (lab.isLabeledWith(id)) {
                    iter.remove();
                    nbRemoved++;
                }
            }
        }
        if (Out.verb()==2)
            System.out.println("          ... has been removed " + nbRemoved + " times");
        return nbRemoved;
    }

    // remove label id from all states which are in toKeep with value false
    void remove(HashMap<String, Boolean> toKeep,int id) {
        for (String st : toKeep.keySet()) {
            if (!toKeep.get(st)) {
                ArrayList<Label> labels = states.get(st);
                Iterator<Label> iter = labels.iterator();
                while (iter.hasNext()) {
                    Label lab = iter.next();
                    if (lab.isLabeledWith(id)) {
                        System.out.println("removing " + lab.constraint + " from state " + st);
                        iter.remove();
                    }
                }
            }
        }
    }


    public int nbAtoms() {
        int nb=0;
        for (String st : states.keySet()) {
            ArrayList<Label> labs = getLabels(st);
            for (Label l : labs) {
                nb += l.constraint.nbAtoms();
            }
        }
        return nb;
    }


    public String toString() {
        StringBuilder s = new StringBuilder("[");
        for (String st : states.keySet()) {
            s.append("["+ st + ":");
            ArrayList<Label> flist = states.get(st);
            if (flist.isEmpty())
                s.append(flist);
            else
                s.append("\n ");
            for (Label f:flist) {
                s.append("{["+ f.ctl.id + "]" +f.ctl + "," + f.constraint+"}\n ");
            }
            s.append("]\n");
        }
        s.append("]\n");
        return s.toString();
    }


    //////////////////////////////////////////////////////////////
    //// NOT USED
    void clean(Formula f,String s){
        ArrayList<Label> labels  = states.get(s);
        Iterator iter = labels.iterator();
        while (iter.hasNext()){
            Label l = (Label) iter.next();
            if (l.isLabeledWith(f.id) && l.constraint.isEmpty())
                iter.remove();
        }
    }

}

