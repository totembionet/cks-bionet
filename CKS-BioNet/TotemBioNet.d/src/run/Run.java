package run;

import back.hoare.HoareTriplets;
import back.search.*;
import parametricmodelcheck.modelcheck.ParametricModelChecker;
import parametricmodelcheck.modelcheck.YicesLauncher;
import util.jclock.Clock;
import back.net.Gene;
import back.net.Net;
import back.net.Para;

import front.smbParser.SMBParser;
import front.smbParser.Visitor;

import util.Out;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.*;
import java.util.Timer;
import java.util.TimerTask;


public class Run {

    public enum SearchType {
        ALL, FORMAL, FROM_PARA_SET, FROM_PARA_HOARE,HOARE, INSTANCIATED;
    }

    enum Command {
        MODELS,PARAS,SIMU,JSON,DEBUG,STG,PSTG,CKS;
    }

    //Pour l'affichage toutes les secondes
    protected static class Banner extends TimerTask {

        SearchType searchType;

        public Banner(SearchType st) {
            this.searchType=st;
        }
        public void run() {
            String searchStat="";
            switch (searchType) {
                case FORMAL:{searchStat= FormalSearchEngine.stats();break;}
                case HOARE:{searchStat= InstanciatedSearchEngineWithHoare.stats();break;}
                case INSTANCIATED:{searchStat= InstanciatedSearchEngine.stats();break;}
                case FROM_PARA_SET: {searchStat= FromFileSearchEngine.stats();break;}
            }
            Out.pr("> (" + c + ") " + searchStat);
        }
    }

    private static Clock c;
    private static Clock cNet;
    private int verbose;
    private Command command;
    private Net net;
    private boolean stable;
    private String filePrefix;
    MainInfo info;

    public Run(boolean stable,MainInfo info) throws Exception {
        this.info = info;
        this.stable = stable;
        this.filePrefix = info.path + info.inputRoot;
        this.verbose = getOption("-verbose", 0);
        Out.setVerb(verbose);

        setCommand();

        if (command.equals(Command.MODELS) || command.equals(Command.JSON)) {
            String outputFile = (optionExists("formal")) ? filePrefix + "_formal" : filePrefix;
            Out.setOutputFile(getOption("-o", outputFile + ".out"));
        }
        cNet = new Clock();
        cNet.start();
        try {
            SMBParser smbParser = new SMBParser(info.path, info.inputRoot, verbose);
            Visitor SMBVisitor = smbParser.parse();
            this.net = new Net(SMBVisitor, command == Command.PARAS);
        }finally {
            System.out.println("\nTime for building the net (including Hoare constraint generation): " + cNet + "\n*******\n");
        }
    }

    // set command according to list of options
    private void setCommand() {
        this.command = Command.MODELS;
        if (optionExists("-paras"))
            this.command = Command.PARAS;
        else if (optionExists("-json"))
            this.command = Command.JSON;
        else if (optionExists("-simu"))
            this.command = Command.SIMU;
        else if (optionExists("-debug"))
            this.command = Command.DEBUG;
        else if (optionExists("-stg"))
            this.command = Command.STG;
        else if (optionExists("-pstg"))
            this.command = Command.PSTG;
        else if (optionExists("-cks"))
            this.command = Command.CKS;
        //System.out.println("run command " + command);
    }

    // the search according to net and options
    private SearchType getSearchType() {
        if (info.opts.contains("-from")) {
            if (net.needDynamicHoare())
                return SearchType.FROM_PARA_HOARE;
            return SearchType.FROM_PARA_SET;
        }
        if (net.needDynamicHoare())
            return SearchType.HOARE;
        if (info.opts.contains("-formal"))
            return SearchType.FORMAL;
        return SearchType.INSTANCIATED;
    }

    // utilisé pour énumérer les modèles quand il n'y a pas de propriétés
    // inutile de passer par NuSMV
    // ici on écrit dans un csv
    private void writeModelsInCSV() throws IOException {
        BufferedWriter w = new BufferedWriter(new FileWriter(new File(filePrefix +".csv")));
        String s = "n°;valid;"; // entête
        String d = ";;";  // domaines
        // paramètres
        for (Gene g : net.genes()) {
            if ((!net.isEnvVar(g))) {
                for (Para p : g.getParas()) {
                    s += p.getSMBname() + ";";
                    d += p.minLevel() + ".." + p.maxLevel() + ";";
                }
            }
        }
        w.write(s+"\n");
        w.write(d+"\n");
        int n = 1;
        ParasAsArray.init(net);
        net.firstParameterization();
        byte[] model = ParasAsArray.currentModelAsArray();
        w.write( + n+";OK;"+ParasAsArray.getParasForCSV(model)+"\n");
        n+=1;
        while (net.nextParameterization()) {
            model = ParasAsArray.currentModelAsArray();
            w.write(n+";OK;"+ParasAsArray.getParasForCSV(model)+"\n");
            n++;
        }
        w.close();
    }

    // quand il n'y a pas de formules CTL, on affiche seulement
    // les paramétrisations possibles donc en accord avec les domaines,
    // le bloc PARA et le bloc HOARE s'il existe.
    public void enumerateModels(boolean csv) throws Exception{
        Out.psfln();
        Out.psfln("###########     No CTL formula     ###########");
        Out.psfln("# Total number of models: " + net.NB_PARAMETERIZATIONS.add(HoareTriplets.nbRemovedParam));
        if (HoareTriplets.nbRemovedParam.compareTo(BigInteger.ZERO)!=0)
            Out.psfln("# Number of models removed using Hoare: " + HoareTriplets.nbRemovedParam);
        Out.psfln("# Number of selected models: " + net.NB_PARAMETERIZATIONS);
        Out.psfln("# Computation time: " + c);
        Out.psfln("Models have been written in " + info.path+ info.inputRoot + ".out");
        Out.psfln("###############################################");
        if (csv) {
            Out.psfln("Models have been written in " + info.path+ info.inputRoot + ".csv");
            writeModelsInCSV();
        }
        else {
            ParasAsArray.init(net);
            net.firstParameterization();
            byte[] model = ParasAsArray.currentModelAsArray();
            int n=1;
            ParasAsArray.writeDetailedModel(model,true,n++);
            while (net.nextParameterization()) {
                model = ParasAsArray.currentModelAsArray();
                ParasAsArray.writeDetailedModel(model,true,n++);
            }
        }
        deleteFiles(info.path, info.inputRoot);
    }


    public void run() throws Exception {
        if (!stable)
            runUnstable();
        else {
            if (Net.inconsistentHoare != 0) {
                writeHoareMessage();
            } else {
                switch (command) {
                    case PARAS: {
                        net.printForbiddenParas();
                        System.out.println("######################");
                        net.printParameterSet();
                        break;
                    }
                    case SIMU: {
                        net.print();
                        Simu.run(net);
                        break;
                    }
                    case JSON: {
                        //TODO : modifié pour ajouter les effective param. A améliorer
                        net.print();
                        SMBParser smbParser = new SMBParser(info.path, info.inputRoot, verbose);
                        Visitor SMBVisitor = smbParser.parse();
                        String result = "{\"result\":" + SMBVisitor.toJson() + ",\n";
                        result += "\"effectiveParas\":{" + net.effectiveParametersToJson() + "}";
                        result += "}";
                        String outFile = filePrefix + ".json";
                        BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
                        out.write(result);
                        out.close();
                        System.out.println("JSON file has been generated in " + outFile + "in " + c.toString() + "ms");
                        break;
                    }
                    case DEBUG: {
                        NuSMV.runDebug(net);
                        break;
                    }
                    default: {
                        net.print();
                        NuSMV.setNuSMVPath();
                        boolean dynamic = info.opts.contains("-dynamic");

                        c = new Clock();

                        boolean writeCSV = optionExists("-csv");
//    System.out.println("root " + info.inputRoot);
//                    if (writeCSV && info.opts.contains("-from")&&info.fromFile.startsWith(info.inputRoot))
//                        throw new TotemBionetException("Impossible to generate csv report in the same file as from file");

                        if (net.hasTemporalLogicProp()) {
                            Timer timer = new Timer();
                            timer.schedule(new Banner(getSearchType()), 0, 1000);
                            NuSMV.run(net, filePrefix, dynamic, getSearchType(), writeCSV);
                            deleteFiles(info.path, info.inputRoot);
                        } else
                            enumerateModels(writeCSV);
                        Out.close();
                        System.exit(0);
                    }
                }
            }
        }
    }

    private void writeHoareMessage() throws Exception {
        Out.psfln("########### Hoare triple unsatisfied ###########");
        if (Net.inconsistentHoare==2)
            Out.psfln("# Hoare weakest precondition is inconsistent with parameter domains");
        else
            Out.psfln("# Hoare weakest precondition is False");
        Out.psfln("# Selected models: " + 0);
        Out.psfln("# Computation time: " + cNet);
        Out.psfln("################################################\n");
    }

    public void runUnstable() throws Exception {
        switch (command) {
            case STG: {
                String outFile = filePrefix + "-stg.json";
                BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
                out.write((new StateGraphGenerator(net)).toJson());
                out.close();
                System.out.println("   *** Success - " + outFile + " file has been generated ***");
                break;
            }
            case PSTG: {
                String outFile = filePrefix + "-pstg.json";
                BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
                out.write((new StateGraphGenerator(net, true)).toJson());
                out.close();
                System.out.println("   *** Success - " + outFile + " file has been generated ***");
                break;
            }
            case CKS: {
                YicesLauncher.setYicesPath();
                long l1 = System.currentTimeMillis();
                ParametricModelChecker pm = new ParametricModelChecker(Main.getPath(), net);
                pm.modelCheck();
                pm.printSolutions(optionExists("-csv"));
                long l2 = System.currentTimeMillis();
                System.out.println("\n************************");
                System.out.println("--- Total time: " + Clock.print(l2 - l1));
                System.out.println("************************");
                break;
            }
        }
    }


    // sans le mode debug, on efface les fichiers intermédiaires
    private void deleteFiles(String path, String name) {
        File dir = new File(path);
        if (verbose<2) { // on efface tout
            System.out.println("\n\nDeleting unnecessary files ...");
            //Delete all files except inputPrefix.out
            for (File file : dir.listFiles()) {
                String fileName = file.getName();
                boolean delete = false;
                Path p= FileSystems.getDefault().getPath(path,fileName);
                if ("hoare".equals(fileName) && file.isDirectory()) {
                    deleteHoareDir(file,path+"/hoare");
                } else if (fileName.equals(name + ".smv")) {
                    System.out.println("     Deleting file " + path + fileName);
                    file.delete();
                }
            }
        }
    }

    // to delete files into Hoare directory
    private void deleteHoareDir(File file,String dirPath) {
        System.out.println("     Deleting directory " + dirPath);
        File dir = new File(dirPath);
        for (File f : dir.listFiles()) {
            System.out.println("     Deleting file " + f.getName());
            f.delete();
        }
        file.delete();
    }

    //Option s avec un entier en argument
    private int getOption(String s, int def) throws Exception {
        int i = info.opts.indexOf(s);
        if (i >= 0) {
            if (i + 1 < info.opts.size())
                return Integer.valueOf(info.opts.get(i + 1));
            throw new Exception("option " + s + " needs an int as arg");
        }
        return def;
    }

    //Option avec une chaine de caract�res en argument
    private String getOption(String s, String def) throws Exception {
        int i = info.opts.indexOf(s);
        if (i >= 0) {
            if (i + 1 < info.opts.size() && !info.opts.get(i + 1).startsWith("-"))
                return info.opts.get(i + 1);
            throw new Exception("option " + s + " needs a string as arg");
        }
        return def;
    }

    //Présence d'une option
    private boolean optionExists(String s) {
        return info.opts.contains(s);
    }


}
