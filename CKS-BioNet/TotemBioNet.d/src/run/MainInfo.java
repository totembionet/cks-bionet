package run;

import java.util.ArrayList;

/***
 * information about the main
 * @author helen
 */
public class MainInfo {

    String fromFile;
    String compareFile;
    String inputFile;
    String path;
    String inputRoot;
    ArrayList<String> opts;

    //static public long nuSMVTime = 0;


    MainInfo() {
        opts = new ArrayList<>();
    }

    boolean isUnstableSimpleOption() {
        return opts.contains("-test") || opts.contains("-cfc")|| opts.contains("-circuit");
    }

    boolean isUnstableRunOption() {
        return opts.contains("-stg")|| opts.contains("-pstg") || opts.contains("-cks");
    }

    boolean isNotNetNeeded() {
        return opts.contains("-test") ||opts.contains("-yed") ||opts.contains("-help")||opts.contains("-intersection")||opts.contains("-union")||opts.contains("-compare");
    }

    boolean isCombinationOp() {
        return opts.contains("-intersection")||opts.contains("-union");
    }

    int verboseLevel() {
        int i = opts.indexOf("-verbose");
        if (i < 0)
            return 0;
        return Integer.valueOf(opts.get(i+1));
    }


}
