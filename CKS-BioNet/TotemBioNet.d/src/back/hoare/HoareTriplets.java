package back.hoare;

import back.logic.Formula;
import back.net.Net;
import back.search.InstanciatedSearchEngine;
import back.search.InstanciatedSearchEngineWithHoare;
import util.Out;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class to handle one or more Hoare triplets
 *
 */
public class HoareTriplets {

    List<OneHoareTriplet> triplets;

    public static BigInteger nbRemovedParam= BigInteger.ZERO;;

    //vrai si conjonction de conditions de la forme x op_comp val
    // (où op_comp opérateur de comparaison)
    public boolean isOnlyConjunct;


    public HoareTriplets() {
        triplets = new ArrayList<OneHoareTriplet>();
        isOnlyConjunct = true;
    }

    void addTriplet(OneHoareTriplet t){
        triplets.add(t);
        if (!t.isOnlyConjunct)
            isOnlyConjunct=false;
    }

    public boolean isEmpty() {
        return triplets.isEmpty();
    }

    // propagate the conjunctive formula to reduce bounds
    public void propagateHoareConstraints(Net net) throws Exception {
        System.out.println("\n* Using Hoare formulas to reduce parameter domains...");
        for (OneHoareTriplet oht : triplets){
            oht.propagateHoareConstraints(net);
        }
    }

    // affiche les informations liées à Hoare
    public void writeHoareInfos() throws Exception {
        Out.psfln("\nHOARE");
        Out.psfln("Result of Hoare triple is:");
        Out.psfln("    " + this.toString());
        if (this.isOnlyConjunct) {
            Out.psfln("    Hoare result is a conjunction of conditions on K, it has been used to reduced parameters bounds");
            Out.psfln("    " + nbRemovedParam + " models have been removed");
        }
        // sinon, il faut vérifier les conditions de hoare
        // pour chaque modèle
        else {
            Out.psfln("    Hoare result contains a disjunction. Only models that satify the hoare triple are generated");
        }
    }

    public void setNbRemovedParam(BigInteger nbp) {
        nbRemovedParam=nbp;
    }

    // to know if all the formulas are valid for a particular parameterization
    public boolean valid(HashMap<String, Integer> model) {
        for (OneHoareTriplet oht : triplets){
            if (!oht.valid(model)) {
                nbRemovedParam=nbRemovedParam.add(BigInteger.ONE);
                return false;
            }
        }
        return true;
    }

    public String toString() {
        return ""+triplets;
    }
}
