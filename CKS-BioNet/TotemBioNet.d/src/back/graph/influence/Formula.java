package back.graph.influence;

/**
 * @author Hugo Bricard
 */
public class Formula implements Comparable<Formula> {

    private int threshold;
    private boolean activation;
    private String multiplexId;

    Formula(int threshold, boolean activation) {
        super();
        this.threshold = threshold;
        this.activation = activation;
        this.multiplexId = "_";
    }

    Formula(int threshold, boolean activation, String multiplexId) {
        super();
        this.threshold = threshold;
        this.activation = activation;
        this.multiplexId = multiplexId;
    }

    int getThreshold() {
        return threshold;
    }

    boolean getActivation() {
        return activation;
    }

    String getMultiplexId() {
        return multiplexId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Formula formula = (Formula) o;
        return multiplexId.equals(formula.multiplexId);
    }

    @Override
    public int compareTo(Formula formula) {
        if (this.multiplexId.compareTo(formula.multiplexId) != 0) {
            return this.multiplexId.compareTo(formula.multiplexId);
        }
        if (Boolean.compare(this.activation, formula.activation) != 0) {
            return Boolean.compare(this.activation, formula.activation);
        }
        return Integer.compare(this.threshold, formula.threshold);
    }

    @Override
    public String toString() {
        String str = "";
        if (activation) {
            str += ">= " + threshold;
        } else {
            str += "<= " + threshold;
        }
        if (multiplexId != null) {
            return str + ", m: " + multiplexId;
        }
        return str;
    }
}
