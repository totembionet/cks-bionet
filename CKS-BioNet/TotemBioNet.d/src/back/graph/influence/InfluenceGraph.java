package back.graph.influence;

import back.graph.data.Graph;

import java.util.*;

/**
 * @author Hugo Bricard
 */
public class InfluenceGraph extends Graph {

    private final ArrayList<InfluenceVertex> vertices;
    private TreeMap<InfluenceVertex, AdjacencyList> adjacencyLists;
    private ArrayList<YicesMultiplex> multiplexes;
    private String yicesVars;
    private Map<InfluenceVertex, ArrayList<Para>> paraLists;

    public InfluenceGraph() {
        this.vertices = new ArrayList<>();
        this.adjacencyLists = new TreeMap<>();
        this.multiplexes = new ArrayList<>();
        this.paraLists = new TreeMap<>();
    }

    ArrayList<InfluenceVertex> getVertices() {
        return vertices;
    }

    TreeMap<InfluenceVertex, AdjacencyList> getAdjacencyLists() {
        return adjacencyLists;
    }

    void setAdjacencyLists(TreeMap<InfluenceVertex, AdjacencyList> adjacencyLists) {
        this.adjacencyLists = adjacencyLists;
    }

    void addVertex(InfluenceVertex influenceVertex) {
        this.vertices.add(influenceVertex);
        this.adjacencyLists.put(influenceVertex, new AdjacencyList(influenceVertex.getName()));
        this.paraLists.put(influenceVertex, new ArrayList<>());
    }

    InfluenceVertex getVertexById(String vertexId) {
        for (InfluenceVertex influenceVertex : vertices) {
            if (influenceVertex.getName().equals(vertexId)) {
                return influenceVertex;
            }
        }
        System.out.println(vertexId + ": vertex not found");
        return null;
    }

    ArrayList<YicesMultiplex> getMultiplexes() {
        return multiplexes;
    }

    YicesMultiplex getMultiplexById(String id) {
        for (YicesMultiplex multiplex : this.multiplexes) {
            if (multiplex.getMultiplexId() == id) return multiplex;
        }
        System.out.println("multiplex not find");
        return null;
    }

    void setYicesVars(String yicesVars) {
        this.yicesVars = yicesVars;
    }

    String getYicesVars() {
        return yicesVars;
    }

    Map<InfluenceVertex, ArrayList<Para>> getParaLists() {
        return paraLists;
    }

    void addArc(InfluenceVertex influenceVertex, Arc arc) {
        adjacencyLists.get(influenceVertex).add(arc);
    }

    ArrayList<InfluenceVertex> dfs(InfluenceVertex influenceVertex, ArrayList<InfluenceVertex> visited) {
        visited.add(influenceVertex);
        for (Arc arc : this.adjacencyLists.get(influenceVertex).getOutgoingArcs()) {
            if (!visited.contains(arc.getSuccessor())) {
                dfs(arc.getSuccessor(), visited);
            }
        }
        return visited;
    }

    void addYicesMultiplex(YicesMultiplex yicesMultiplex) {
        this.multiplexes.add(yicesMultiplex);
    }

    ArrayList<String> getMultiplexPredecessors(InfluenceVertex vertex) {
        ArrayList<String> multiplexIds = new ArrayList<>();
        for (InfluenceVertex v : this.vertices) {
            for (Arc arc : this.adjacencyLists.get(v).getOutgoingArcs()) {
                if (arc.getSuccessor().equals(vertex) && !(multiplexIds.contains(arc.getFormulaMultiplexId()))) {
                    multiplexIds.add(arc.getFormulaMultiplexId());
                }
            }
        }
        return multiplexIds;
    }

    ArrayList<Para> getParasWithoutMultiplex(InfluenceVertex x, String m) {
        ArrayList<Para> paralist = this.paraLists.get(x);
        ArrayList<Para> parasWithoutm = new ArrayList<>();
        for (Para p : paralist) if (!p.getMultiplexes().contains(m)) parasWithoutm.add(p);
        return parasWithoutm;
    }

    Para getParaWithMultiplex(InfluenceVertex x, Para p, String m) {
        TreeSet<String> copy = new TreeSet<>(p.getMultiplexes());
        copy.add(m);
        for (Para para : this.paraLists.get(x)) {
            if (copy.equals(para.getMultiplexes())) {
                return para;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        Collections.sort(vertices);
        String str = "number of vertices: " + vertices.size() + "\n";
        for (InfluenceVertex v : vertices) {
            str += v.toString() + ": ";
            for (Arc arc : adjacencyLists.get(v).getOutgoingArcs()) {
                InfluenceVertex successor = arc.getSuccessor();
                Formula formula = arc.getFormula();
                str += "[" + successor.getName() + ", " + formula.toString() + "], ";
            }
            str = (String) str.subSequence(0, str.length() - 2);
            str += "\n";
        }
        return str;
    }
}
