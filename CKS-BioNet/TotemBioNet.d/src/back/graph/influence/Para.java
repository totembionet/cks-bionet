package back.graph.influence;

import java.util.ArrayList;
import java.util.TreeSet;

public class Para {

    private String name;
    private int min;
    private int max;
    private TreeSet<String> multiplexes;
    private String yices;

    public Para(String name, int min, int max, TreeSet<String> multiplexes) {
        this.name = name.replaceAll(":", "-");
        this.min = min;
        this.max = max;
        this.multiplexes = multiplexes;
        this.yices = "(define " + this.name + "::int)\n";
        if (min == max) this.yices += "(assert (=  " + this.name + " " + min + "))\n";
        else
            this.yices += "(assert (>=  " + this.name + " " + min + "))\n(assert (<=  " + this.name + " " + max + "))\n";
    }

    String getName() {
        return name;
    }

    int getMin() {
        return min;
    }

    int getMax() {
        return max;
    }

    TreeSet<String> getMultiplexes() {
        return multiplexes;
    }

    String getYices() {
        return yices;
    }

    void addMultiplex(String m) {
        this.multiplexes.add(m);
    }

    void removeMultiplex(String m) {
        this.multiplexes.remove(m);
    }

    boolean equalsMultiplexes(Para p) {
        int size1 = this.multiplexes.size();
        int size2 = p.multiplexes.size();
        if (size1 != size2) return false;
        else {
            ArrayList<String> multiplexesList1 = new ArrayList<>(this.multiplexes);
            ArrayList<String> multiplexesList2 = new ArrayList<>(p.multiplexes);
            for (int i = 0; i < size1; i++) {
                if (!multiplexesList1.get(i).equals(multiplexesList2.get(i))) return false;
            }
            return true;
        }
    }
}
