package back.graph.influence;

import back.graph.data.Vertex;

/**
 * @author Hugo Bricard
 */
public class InfluenceVertex extends Vertex implements Comparable<InfluenceVertex> {

    private int min;
    private int max;

    public InfluenceVertex(String name, int min, int max) {
        super(name);
        this.min = min;
        this.max = max;
    }

    int getMin() {
        return min;
    }

    int getMax() {
        return max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InfluenceVertex that = (InfluenceVertex) o;
        return this.getName().equals(that.getName());
    }

    @Override
    public String toString() {
        return "[" + super.getName() + ", " + min + "-" + max + "]";
    }

    @Override
    public int compareTo(InfluenceVertex v) {
        return this.getName().compareTo(v.getName());
    }
}
