package back.graph.influence;

public class Arc implements Comparable<Arc> {

    private InfluenceVertex successor;
    private Formula formula;

    Arc(InfluenceVertex successor, Formula formula) {
        this.successor = successor;
        this.formula = formula;
    }

    InfluenceVertex getSuccessor() {
        return successor;
    }

    Formula getFormula() {
        return formula;
    }

    String getFormulaMultiplexId() {
        return this.formula.getMultiplexId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Arc arc = (Arc) o;
        return successor.equals(arc.successor) &&
                formula.equals(arc.formula);
    }

    @Override
    public int compareTo(Arc arc) {
        if (this.successor.compareTo(arc.successor) != 0) return this.successor.compareTo(arc.successor);
        return this.formula.compareTo(arc.formula);
    }
}
