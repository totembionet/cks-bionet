package back.graph.influence;

public class YicesMultiplex {

    private String multiplexId;
    private String yices;

    public YicesMultiplex(String multiplexId, String yices) {
        this.multiplexId = multiplexId;
        this.yices = yices;
    }

    public String getMultiplexId() {
        return multiplexId;
    }

    String getYices() {
        return yices;
    }
}
