package back.graph.influence;

import org.junit.Test;

import java.io.IOException;
import java.util.TreeMap;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * @author Hugo Bricard
 */
public class CircuitFinderTest {

    private InfluenceGraph buildCompleteGraph(int n) {
        InfluenceGraph graph = new InfluenceGraph();
        for (int i = 0; i < n; i++) {
            graph.addVertex(new InfluenceVertex(((Integer) (i + 1)).toString(), 0, 1));
        }
        TreeMap<InfluenceVertex, AdjacencyList> adjLists = new TreeMap<>();
        for (InfluenceVertex v : graph.getVertices()) {
            TreeSet<Arc> arcs = new TreeSet<>();
            for (InfluenceVertex w : graph.getVertices()) {
                arcs.add(new Arc(w, new Formula(1, true)));
            }
            AdjacencyList adjList = new AdjacencyList(v.getName(), arcs);
            adjLists.put(v, adjList);
        }
        graph.setAdjacencyLists(adjLists);
        return graph;
    }

    InfluenceGraph graph4 = buildCompleteGraph(4);
    InfluenceGraph graph5 = buildCompleteGraph(5);
    InfluenceGraph graph6 = buildCompleteGraph(6);
    InfluenceGraph graph7 = buildCompleteGraph(7);
    InfluenceGraph graph8 = buildCompleteGraph(8);
    InfluenceGraph graph9 = buildCompleteGraph(9);
    InfluenceGraph graph10 = buildCompleteGraph(10);

    CircuitFinder circuitFinder = new CircuitFinder();

    @Test
    public void completeGraph4Test() throws IOException {
        System.out.println("completeGraph4Test:");
        circuitFinder.findCircuits(graph4);
        assertEquals(24, circuitFinder.getCircuitCounter());
    }

    @Test
    public void completeGraph5Test() {
        System.out.println("completeGraph5Test:");
        circuitFinder.findCircuits(graph5);
        assertEquals(89, circuitFinder.getCircuitCounter());
    }

    @Test
    public void completeGraph6Test() {
        System.out.println("completeGraph6Test:");
        circuitFinder.findCircuits(graph6);
        assertEquals(415, circuitFinder.getCircuitCounter());
    }

    @Test
    public void completeGraph7Test() {
        System.out.println("completeGraph7Test:");
        circuitFinder.findCircuits(graph7);
        assertEquals(2372, circuitFinder.getCircuitCounter());
    }

    @Test
    public void completeGraph8Test() {
        System.out.println("completeGraph8Test:");
        circuitFinder.findCircuits(graph8);
        assertEquals(16072, circuitFinder.getCircuitCounter());
    }

    @Test
    public void completeGraph9Test() {
        System.out.println("completeGraph9Test:");
        circuitFinder.findCircuits(graph9);
        assertEquals(125673, circuitFinder.getCircuitCounter());
    }

    @Test
    public void completeGraph10Test() {
        System.out.println("completeGraph10Test:");
        circuitFinder.findCircuits(graph10);
        assertEquals(1112083, circuitFinder.getCircuitCounter());
    }
}