package back.graph.influence;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

/**
 * @author Hugo Bricard
 */

public class CircuitFinder {

    private InfluenceGraph graph;

    private ArrayList<InfluenceVertex> blocked;
    private ArrayList<InfluenceVertex> path;
    private ArrayList<Formula> pathFormulas;
    private TreeMap<InfluenceVertex, ArrayList<InfluenceVertex>> memory;
    private InfluenceVertex root;
    private ArrayList<InfluenceVertex> processed;

    private FileWriter fileWriter;
    private String yicesFileWriterPath;

    private int circuitCounter;
    private StringBuilder positiveCircuits;
    private int positiveCircuitsCounter;
    private StringBuilder negativeCircuits;
    private int negativeCircuitsCounter;

    private boolean functional;

    CircuitFinder() {
        blocked = new ArrayList<>();
        path = new ArrayList<>();
        pathFormulas = new ArrayList<>();
        memory = new TreeMap<>();
    }

    private void clear() {
        path.clear();
        pathFormulas.clear();
        processed = new ArrayList<>();
        circuitCounter = 0;
        positiveCircuits = new StringBuilder();
        positiveCircuitsCounter = 0;
        negativeCircuits = new StringBuilder();
        negativeCircuitsCounter = 0;
        yicesFileWriterPath = null;
    }

    void initYicesFileWriterPath(String fileWriterPath) {
        String str = "";
        String[] strSplit = fileWriterPath.split("/");
        for (int i = 0; i < strSplit.length - 1; i++) {
            str += strSplit[i] + "/";
        }
        str += strSplit[strSplit.length - 1].split(".out")[0] + ".ys";
        this.yicesFileWriterPath = str;
    }

    int getCircuitCounter() {
        return circuitCounter;
    }

    void findCircuits(InfluenceGraph graph) {
        findCircuits(graph, null);
    }

    void findCircuits(InfluenceGraph graph, String fileWriterPath) {
        clear();
        if (fileWriterPath != null) {
            initYicesFileWriterPath(fileWriterPath);
            try {
                this.fileWriter = new FileWriter(fileWriterPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.graph = graph;
        ArrayList<InfluenceVertex> vertices = new ArrayList<>(graph.getVertices());
        for (InfluenceVertex v : vertices) {
            memory.put(v, new ArrayList<>());
        }
        Collections.sort(vertices);
        int s = 0;
        while (s < vertices.size()) {
            root = vertices.get(s);
            for (int i = s; i < vertices.size(); i++) {
                blocked.remove(vertices.get(i));
                memory.get(vertices.get(i)).clear();
            }
            circuit(root, null);
            processed.add(root);
            s++;

        }
        if (fileWriterPath != null) {
            try {
                fileWriter.write(circuitCounter + " circuits\n\n");
                fileWriter.write(positiveCircuitsCounter + " positive circuits\n");
                fileWriter.write(positiveCircuits.toString() + "\n");
                fileWriter.write(negativeCircuitsCounter + " negative circuits\n");
                fileWriter.write(negativeCircuits.toString());
                fileWriter.close();
                Files.deleteIfExists(Paths.get(yicesFileWriterPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println(circuitCounter + " circuits  found");
    }

    private Boolean circuit(InfluenceVertex v, Formula formula) {
        boolean f = false;
        if (processed.contains(v)) return f;
        path.add(v);
        if (formula != null) pathFormulas.add(formula);
        blocked.add(v);
        for (Arc arc : this.graph.getAdjacencyLists().get(v).getOutgoingArcs()) {
            if (arc.getSuccessor().compareTo(root) == 0) {
                pathFormulas.add(arc.getFormula());
                output();
                pathFormulas.remove(pathFormulas.size() - 1);
                f = true;
            } else if (!blocked.contains(arc.getSuccessor()) && circuit(arc.getSuccessor(), arc.getFormula())) {
                f = true;
            }
        }
        if (f) {
            unblock(v);
        } else {
            for (Arc arc : this.graph.getAdjacencyLists().get(v).getOutgoingArcs()) {
                if (!memory.get(arc.getSuccessor()).contains(v)) {
                    memory.get(arc.getSuccessor()).add(v);
                }
            }
        }
        path.remove(path.size() - 1);
        if (!pathFormulas.isEmpty()) pathFormulas.remove(pathFormulas.size() - 1);
        return f;
    }

    private void unblock(InfluenceVertex v) {
        blocked.remove(v);
        while (!memory.get(v).isEmpty()) {
            InfluenceVertex w = memory.get(v).get(0);
            memory.get(v).remove(w);
            if (blocked.contains(w)) {
                unblock(w);
            }
        }
    }

    private void output() {
        circuitCounter++;
        Formula formula;
        boolean negative = false;
        StringBuilder circuit = new StringBuilder();
        circuit.append(circuitCounter + ": ");

        String multiplexesCircuit = "";
        String functCircuit = "";
        functional = true;
        for (int i = 0; i < path.size() - 1; i++) {
            InfluenceVertex v = path.get(i);
            formula = pathFormulas.get(i);
            if (!formula.getActivation()) negative = !(negative);
            circuit.append(v.getName() + "--[" + formula + "]->");
            if (yicesFileWriterPath != null) {
                YicesMultiplex multiplex = graph.getMultiplexById(formula.getMultiplexId());
                multiplexesCircuit += multiplex.getYices();

                InfluenceVertex vertex = path.get(i + 1);
                int threshold = pathFormulas.get(i + 1).getThreshold();
                String m = formula.getMultiplexId();
                functCircuit += ";" + m + "\n" + addInequality(vertex, threshold, m);
            }
        }
        formula = pathFormulas.get(pathFormulas.size() - 1);
        if (!formula.getActivation()) negative = !(negative);
        if (yicesFileWriterPath != null) {
            YicesMultiplex multiplex = graph.getMultiplexById(formula.getMultiplexId());
            multiplexesCircuit += multiplex.getYices();

            InfluenceVertex vertex = path.get(0);
            int threshold = pathFormulas.get(0).getThreshold();
            String m = formula.getMultiplexId();
            functCircuit += ";" + m + "\n" + addInequality(vertex, threshold, m);
        }
        String yicesResult = executeYicesCommand(multiplexesCircuit);
        String functResult = "";
        if (functional) functResult = executeYicesCommand(functCircuit);
        else functResult = "missing parmaters";
        if (negative) {
            circuit.append(path.get(path.size() - 1).getName() + "--[" + formula + "]->" + path.get(0).getName() + " negative\n");
            circuit.append(yicesResult + "\n");
            circuit.append(multiplexesCircuit + "\n");
            circuit.append("functional: " + functResult + "\n");
            circuit.append(functCircuit + "\n");
            negativeCircuitsCounter++;
            negativeCircuits.append(circuit);
        } else {
            circuit.append(path.get(path.size() - 1).getName() + "--[" + formula + "]->" + path.get(0).getName() + " positive\n");
            circuit.append(yicesResult + "\n");
            circuit.append(multiplexesCircuit + "\n");
            circuit.append("functional: " + functResult + "\n");
            circuit.append(functCircuit + "\n");
            positiveCircuitsCounter++;
            positiveCircuits.append(circuit);
        }
    }

    private String executeYicesCommand(String yices) {
        if (yicesFileWriterPath != null) {
            Process p;
            try {
                yices += "(check)\n(show-model)";
                yices = graph.getYicesVars() + yices;
                FileWriter yicesFileWriter = new FileWriter(yicesFileWriterPath);
                yicesFileWriter.write(yices);
                yicesFileWriter.close();
                p = Runtime.getRuntime().exec("yices " + yicesFileWriterPath);
                p.waitFor();
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line = "";
                if ((line = reader.readLine()) != null) return line;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "error";
    }

    private String addInequality(InfluenceVertex x, int threshold, String m) {
        String str = "";
        String vars = "";
        int counter = 0;
        ArrayList<Para> parasWithoutm = graph.getParasWithoutMultiplex(x, m);
        if (!parasWithoutm.isEmpty()) {
            for (Para p : parasWithoutm) {
                Para pWithm = graph.getParaWithMultiplex(x, p, m);
                if (pWithm != null) {
                    vars += p.getYices() + pWithm.getYices();
                    str += "(and (< " + p.getName() + " " + threshold + ")(<= " + threshold + " " + pWithm.getName() + "))";
                    counter++;
                }
            }
            if (counter == 0) {
                functional = false;
                return "no parameters\n";
            }
            if (counter == 1) return vars + "(assert " + str + ")\n";
            else return vars + "(assert (or " + str + "))\n";
        } else {
            functional = false;
            return "no parameters\n";
        }
    }
}

