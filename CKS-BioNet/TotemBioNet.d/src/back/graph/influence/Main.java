package back.graph.influence;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * @author Hugo Bricard
 */
public class Main {

    public static void main(String... args) {

        if (args.length > 0) {
            GraphBuilder graphBuilder = new GraphBuilder();
            InfluenceGraph influenceGraph = graphBuilder.generateGraphFromJSON(args[0]);
            System.out.println(influenceGraph);
            CircuitFinder circuitFinder = new CircuitFinder();
            String filePath = args[0].split(".json")[0] + "-circuits.out";
            circuitFinder.findCircuits(influenceGraph, filePath);
            System.out.println("result saved in file " + filePath);
        } else {
            Result result = JUnitCore.runClasses(CircuitFinderTest.class);
            for (Failure failure : result.getFailures()) System.out.println(failure.toString());
            System.out.println("run time: " + result.getRunTime() + " ms");
        }
    }
}
