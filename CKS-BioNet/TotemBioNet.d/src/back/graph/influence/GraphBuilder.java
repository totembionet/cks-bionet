package back.graph.influence;

import back.graph.data.Parser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeSet;

/**
 * @author Hugo Bricard
 */
public class GraphBuilder {

    private String multiplexId;
    private String yices;

    private Object[] traverseFormulaTree(JSONObject formulaTree, ArrayList<String> vertexIds,
                                         ArrayList<Formula> formulas, Boolean ope) {
        if (!formulaTree.get("expr1").toString().equals("null")) {
            JSONObject expr1 = (JSONObject) formulaTree.get("expr1");
            ope = processExpr(vertexIds, formulas, ope, expr1);
        }
        if (!formulaTree.get("expr2").toString().equals("null")) {
            JSONObject expr2 = (JSONObject) formulaTree.get("expr2");
            ope = processExpr(vertexIds, formulas, ope, expr2);
        }
        return new Object[]{vertexIds, formulas};
    }

    private Boolean processExpr(ArrayList<String> vertexIds, ArrayList<Formula> formulas, Boolean ope, JSONObject expr) {
        if (expr.get("type").equals("EXPR")) {
            String opeStr = (String) expr.get("operateur");
            if (opeStr.equals("&")) yices += "(and ";
            if (opeStr.equals("|")) yices += "(or ";
            if (opeStr.equals("!")) {
                ope = (!ope);
                yices += "(not ";
            }
            traverseFormulaTree(expr, vertexIds, formulas, ope);
            yices += ")";
        } else {
            String varId1 = (String) expr.get("var");
            vertexIds.add(varId1);
            int threshold = ((Long) expr.get("seuil")).intValue();
            formulas.add(new Formula(threshold, ope, multiplexId));
            yices += "(>= " + varId1 + " " + threshold + ")";
        }
        return ope;
    }

    @SuppressWarnings("unchecked")
    InfluenceGraph generateGraphFromJSON(String path) {

        Parser parser = new Parser();
        JSONArray result = parser.parseResult(path, "result");

        InfluenceGraph graph = new InfluenceGraph();
        String yicesVars = "";

        for (Object item : result) {
            JSONObject block = (JSONObject) item;
            if (block.get("blocktype").equals("env_var")) {
                JSONArray data = (JSONArray) ((JSONObject) block.get("value")).get("data");
                for (Object datum : data) {
                    JSONObject var = (JSONObject) datum;
                    String varId = (String) var.get("id");
                    int val = ((Long) var.get("val")).intValue();
                    graph.addVertex(new InfluenceVertex(varId, val, val));

                    yicesVars += "(define " + varId + "::int)\n";
                    yicesVars += "(assert (=  " + varId + " " + val + "))\n";
                }
            }
            if (block.get("blocktype").equals("var")) {
                JSONArray data = (JSONArray) ((JSONObject) block.get("value")).get("data");
                for (Object datum : data) {
                    JSONObject var = (JSONObject) datum;
                    String varId = (String) var.get("id");
                    int min = ((Long) var.get("min")).intValue();
                    int max = ((Long) var.get("max")).intValue();
                    graph.addVertex(new InfluenceVertex(varId, min, max));

                    yicesVars += "(define " + varId + "::int)\n";
                    yicesVars += "(assert (>=  " + varId + " " + min + "))\n";
                    yicesVars += "(assert (<=  " + varId + " " + max + "))\n";
                }
            }
            yicesVars += "\n";
            graph.setYicesVars(yicesVars);

            if (block.get("blocktype").equals("reg")) {
                JSONArray value = (JSONArray) block.get("value");
                for (Object o : value) {
                    String cibleId = (String) ((JSONObject) o).get("cible");
                    InfluenceVertex cible = graph.getVertexById(cibleId);
                    JSONArray regs = (JSONArray) ((JSONObject) o).get("regs");
                    for (Object reg : regs) {
                        multiplexId = (String) ((JSONObject) reg).get("id");
                        yices = ";" + multiplexId + "\n";
                        JSONObject formulaTree = (JSONObject) ((JSONObject) reg).get("formulaTree");
                        if (formulaTree.get("type").equals("EXPR")) {

                            yices += "(assert (";
                            if (formulaTree.get("operateur").equals("!")) yices += "not ";
                            if (formulaTree.get("operateur").equals("&")) yices += "and ";
                            if (formulaTree.get("operateur").equals("|")) yices += "or ";

                            Boolean ope = (!formulaTree.get("operateur").equals("!"));
                            Object[] arcs = traverseFormulaTree(formulaTree, new ArrayList<>(),
                                    new ArrayList<>(), ope);

                            yices += "))\n";

                            for (int l = 0; l < ((ArrayList<String>) arcs[0]).size(); l++) {
                                InfluenceVertex influenceVertex = graph.getVertexById(((ArrayList<String>) arcs[0]).get(l));
                                graph.addArc(influenceVertex, new Arc(cible, ((ArrayList<Formula>) arcs[1]).get(l)));
                            }
                        } else {
                            String varId = (String) formulaTree.get("var");
                            InfluenceVertex influenceVertex = graph.getVertexById(varId);
                            int threshold = ((Long) formulaTree.get("seuil")).intValue();
                            graph.addArc(influenceVertex, new Arc(cible, new Formula(threshold, true, multiplexId)));

                            yices += "(assert (>=  " + varId + " " + threshold + "))\n";
                        }
                        graph.addYicesMultiplex(new YicesMultiplex(multiplexId, yices));
                    }
                }
            }
            if (block.get("blocktype").equals("para")) {
                JSONArray value = (JSONArray) block.get("value");
                for (Object val : value) {
                    JSONObject valObject = (JSONObject) val;
                    String gene = (String) valObject.get("gene");
                    InfluenceVertex v = graph.getVertexById(gene);
                    JSONArray paras = (JSONArray) valObject.get("paras");
                    for (Object p : paras) {
                        String name = (String) ((JSONObject) p).get("id");
                        TreeSet<String> multiplexes = getParaMultiplexes(name);
                        int min = ((Long) ((JSONObject) p).get("min")).intValue();
                        int max = ((Long) ((JSONObject) p).get("max")).intValue();
                        graph.getParaLists().get(v).add(new Para(name, min, max, multiplexes));
                    }
                }
            }
        }
        return graph;
    }

    private TreeSet<String> getParaMultiplexes(String name) {
        TreeSet<String> multiplexes = new TreeSet<>();
        String[] nameSplit = name.split(":");
        if (nameSplit.length > 1) multiplexes.addAll(Arrays.asList(nameSplit).subList(1, nameSplit.length));
        return multiplexes;
    }

}
