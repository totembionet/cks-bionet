package back.graph.influence;

import java.util.TreeSet;

/**
 * @author Hugo Bricard
 */
public class AdjacencyList {

    private String vertexId;
    private TreeSet<Arc> outgoingArcs;

    public AdjacencyList(String vertexId, TreeSet<Arc> outgoingArcs) {
        super();
        this.vertexId = vertexId;
        this.outgoingArcs = outgoingArcs;
    }

    public AdjacencyList(String vertexId) {
        this.vertexId = vertexId;
        this.outgoingArcs = new TreeSet<>();
    }

    TreeSet<Arc> getOutgoingArcs() {
        return outgoingArcs;
    }

    void add(Arc arc) {
        this.outgoingArcs.add(arc);
    }
}
