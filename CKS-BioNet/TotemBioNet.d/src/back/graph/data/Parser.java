package back.graph.data;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

/**
 * @author Yanis Benabdelouahed | Hugo Bricard
 * Stage SI4 19-20
 */

public class Parser {

    public Parser(){}

    public JSONArray parseResult(String path, String object) {
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(path));

            // A JSON object. Key value pairs are unordered. JSONObject supports
            // java.util.Map interface.
            JSONObject jsonObject = (JSONObject) obj;

            // A JSON array. JSONObject supports java.util.List interface.
            return (JSONArray) jsonObject.get(object);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
