package back.graph.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * @author Yanis Benabdelouahed
 * Stage SI4 19-20
 */

public class Vertex {

    private String name;
    private int id;
    private boolean visited;
    private ArrayList<Vertex> previousVertices;
    private boolean commitmentSet = false;
    private HashSet<Vertex> adjencyList =  new HashSet<>();
    private int scc = -1;
    private HashSet<Vertex> attractorReached = new HashSet<>();

    public Vertex(String name){
        this.name = name;
        this.visited = false;
        this.previousVertices = new ArrayList<>();
    }

    public Vertex(String name, int id){
        this.name = name;
        this.id = id;
        this.visited = false;
        this.previousVertices = new ArrayList<>();
    }

    public Vertex(){
        this.visited = false;
        this.previousVertices = new ArrayList<>();
    }

    public HashSet<Vertex> getAdjencyList() {
        return adjencyList;
    }

    public ArrayList<Vertex> getPreviousVertices() {
        return previousVertices;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public int getScc() {
        return scc;
    }

    public void setScc(int scc) {
        this.scc = scc;
    }

    public boolean isCommitmentSet(){
        return this.commitmentSet;
    }

    public void setCommitmentSet(boolean commitmentSet) {
        this.commitmentSet = commitmentSet;
    }

    public void setPreviousVertices(ArrayList<Vertex> previousVertices) {
        this.previousVertices = previousVertices;
    }

    public void addPreviousVertex(Vertex vertex){
        if(vertex.getPreviousVertices().size()>0){
            this.previousVertices.addAll(vertex.getPreviousVertices());
        }
        this.previousVertices.add(vertex);
    }

    public void removePreviousVertex(Vertex vertex){
        this.previousVertices.remove(vertex);
    }

    public void addEdge(Vertex to){
        this.adjencyList.add(to);
    }

    public void addAttractorReached(Vertex vertex){
        this.attractorReached.add(vertex);
    }

    public HashSet<Vertex> getAttractorReached(){
        return this.attractorReached;
    }

    public String getName(){
        return this.name;
    }

    public void setVisited(){
        this.visited = true;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void concatenateName(String name){
        if(this.name == null){
            this.name = "";
        }
        this.name = this.name + " " + name;
    }

    public boolean inTheSameVertex(Vertex vertex){
        ArrayList<String> names = new ArrayList<>();
        Collections.addAll(names, this.name.split(" "));
        return names.contains(vertex.getName());
    }

    @Override
    public String toString() {
        return this.name;
    }

    public int getId() {
        return id;
    }

    public boolean isVisited(){
        return this.visited;
    }
}
