package back.graph.state;

import back.graph.data.Vertex;

import java.util.ArrayList;

/**
 * @author Yanis Benabdelouahed
 * Stage SI4 19-20
 */

public class StronglyConnectedComponent {

    private int id;
    private ArrayList<Vertex> vertices;
    private ArrayList<Vertex> adjencyList = new ArrayList<>();

    public StronglyConnectedComponent(){
        this.vertices = new ArrayList<>();
        this.id = -1;
    }

    public StronglyConnectedComponent(ArrayList<Vertex> vertices){
        this.vertices = vertices;
        this.id = -1;
    }

    public StronglyConnectedComponent(ArrayList<Vertex> vertices, int id){
        this.vertices = vertices;
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(ArrayList<Vertex> vertices) {
        this.vertices = vertices;
    }

    public void addVertex(Vertex vertex){
        this.vertices.add(vertex);
        vertex.setScc(this.id);
    }

    public void addEdge(StronglyConnectedComponent to){
        for(Vertex vertex: this.vertices){
            for(Vertex vertex1: vertex.getAdjencyList()){
                if(!inTheSameSCC(vertex1)){

                }
            }
        }
    }

    public boolean inTheSameSCC(Vertex vertex){
        return this.vertices.contains(vertex);
    }

    public String toString(){
        StringBuilder str = new StringBuilder("Composante fortement connexe " + this.id + " : [ ");
        for(Vertex vertex : this.vertices){
            str.append(vertex.getName());
            str.append(" ");
        }
        str.append("]");
        return str.toString();
    }
}
