package back.graph.state;

import back.graph.data.Vertex;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;

import static java.lang.Math.min;

/**
 * @author Yanis Benabdelouahed
 * Stage SI4 19-20
 */

public class TarjanCFC {

    private final int n;
    private final TransitionGraph graph;
    private boolean solved;
    private int cfccount, id;
    private boolean[] onStack;
    private ArrayList<Integer> ids, low;
    private Deque<Integer> stack;
    private Deque<Integer> stackCommitment;
    private boolean[] onStackCommitment;
    private HashMap<String, ArrayList<Vertex>> commitmentSet = new HashMap<>();

    private final int UNVISITED = -1;

    public TarjanCFC(TransitionGraph graph) {
        if (graph == null) {
            throw new IllegalArgumentException("Le graph ne peut pas être null");
        }
        n = graph.getSize();
        this.graph = graph;
    }

    /**
     * @return number of CFC
     */
    int CFCcount() {
        if (!solved) solve();
        return cfccount;
    }

    // Get the connected components of this graph. If two indexes
    // have the same value then they're in the same SCC.

    /**
     * @return the low list (if 2 vertex got the same number, they are in the same CFC
     */
    ArrayList<Integer> getCFC() {
        if (!solved) solve();
        return low;
    }

    /**
     * Apply Tarjan algorithm
     */
    public void solve() {
        if (solved) return;

        ids = new ArrayList<>();
        low = new ArrayList<>();
        fillIds();
        fillArray(n, UNVISITED, low);
        onStack = new boolean[n];
        stack = new ArrayDeque<>();
        stackCommitment = new ArrayDeque<>();
        onStackCommitment = new boolean[n];

        for (int i = 0; i < n; i++){
            if(low.get(i) == UNVISITED){
                dfs(i);
            }
        }

        for(int i = 0; i<n; i++){
            dfsCommitmentSet(i);
        }

        solved = true;
        stackCommitment.clear();
    }

    /**
     * Fill all ids in the "ids" list
     */
    private void fillIds() {
        for(int i = 0; i<graph.getSize(); i++){
            ids.add(graph.getVertex(i).getId());
        }
    }

    public ArrayList<Integer> getLow(){
        return this.low;
    }

    /**
     * Depth first search applied with Tarjan algorithm
     * @param at vertex id where we apply the dfs
     */
    private void dfs(int at) {
        stack.push(at); //Put the vertex on the stack
        onStack[at] = true; //On met à true onStack
        low.set(at, ids.indexOf(at));
        if(graph.getAdjencyIds(at).size() == 0 || (graph.getAdjencyIds(at).size() == 1 && graph.getAdjencyIds(at).get(0) == at)){
            stack.iterator().forEachRemaining(x -> {
                graph.getVertexById(x).addAttractorReached(graph.getVertexById(at));
            });
            graph.getVertexById(at).setCommitmentSet(true);
        }
        for (int to : graph.getAdjencyIds(at)) { //itere sur toutes les aretes adjacentes
            if (low.get(to) == UNVISITED) {
                dfs(to); //Si cette arete n'a pas été visité on applique dfs dessus
            }
            if (onStack[to]) {
                low.set(at, min(low.get(at), low.get(to))); //Si l'arete to est dans la pile, low prend la valeur min entre low[at|to]
            }
        }

        // On recursive callback, if we're at the root node (start of SCC)
        // empty the seen stack until back to root.
        if (ids.get(at).equals(low.get(at))) {
            for (int node = stack.pop(); ; node = stack.pop()) {
                Vertex vertexReached = graph.getVertexById(node);
                if(vertexReached.isCommitmentSet()){
                    graph.getVertexById(at).addAttractorReached(vertexReached);
                }
                onStack[node] = false;
                low.set(node, ids.get(at));
                if (node == at) break;
            }
            cfccount++; //incrémente
        }
    }

    public void dfsCommitmentSet(int at){
        stackCommitment.push(at); //Put the vertex on the stack
        onStackCommitment[at] = true; //On met à true onStack
        int first = stackCommitment.getLast();

        for(int to : graph.getAdjencyIds(at)){
            if(!onStackCommitment[to]){
                dfsCommitmentSet(to);
            }
        }
        if(graph.getVertexById(at).isCommitmentSet()){
            for(int node = stackCommitment.pop(); ; node=stackCommitment.pop()){
                Vertex attractorReached = graph.getVertexById(at);
                Vertex actualNode = graph.getVertexById(node);
                actualNode.addAttractorReached(attractorReached);
                if(!commitmentSet.containsKey(attractorReached.getName())){
                    commitmentSet.put(attractorReached.getName(), new ArrayList<>());
                }
                boolean differentkey = false;
                for(String key: commitmentSet.keySet()){
                    if(commitmentSet.get(key).contains(actualNode) && !key.equals(attractorReached.getName())){
                        commitmentSet.get(key).remove(actualNode);
                        commitmentSet.put(key + " " + attractorReached.getName(), new ArrayList<>());
                        commitmentSet.get(key + " " + attractorReached.getName()).add(actualNode);
                        differentkey = true;
                        break;
                    }
                }
                if(!differentkey && !commitmentSet.get(attractorReached.getName()).contains(actualNode)){
                    commitmentSet.get(attractorReached.getName()).add(actualNode);
                }
                onStackCommitment[node] = false;
                if(node == first) {
                    break;
                }
            }
        }
    }

    /**
     *
     * @param n number of vertex
     * @param value value that we fill with
     * @param arrayList list to fill
     */
    private void fillArray(int n, int value, ArrayList<Integer> arrayList){
        for(int i = 0; i<n; i++){
            arrayList.add(value);
        }
    }

    public HashMap<String, ArrayList<Vertex>> getCommitmentSet() {
        return commitmentSet;
    }
}
