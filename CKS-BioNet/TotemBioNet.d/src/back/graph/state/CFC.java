package back.graph.state;

import back.graph.data.Parser;
import back.graph.data.Vertex;
import back.graph.influence.GraphBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * @author Yanis Benabdelouahed
 * Stage SI4 19-20
 */

public class CFC {

    private Map<Integer, List<Integer>> edgesMap;
    private ArrayList<StronglyConnectedComponent> sccs = new ArrayList<>();
    private TransitionGraph graph;
    private ArrayList<Vertex> sccList = new ArrayList<>();

    private CFC(){
        this.edgesMap = new HashMap<>();
    }


    /**
     * Parth the JSON and convert it to a Transition graph
     * @param path Where is the JSON
     * @param sync If we choose the sync or the async graph
     * @return
     */
    private TransitionGraph generateTransGraphFromJSON(String path, boolean sync){
        Parser parser = new Parser();
        JSONArray result;
        if(sync){
            result = parser.parseResult(path, "sync-graph");
        } else {
            result = parser.parseResult(path, "async-graph");
        }
        TransitionGraph graph = new TransitionGraph();
        GraphBuilder graphBuilder = new GraphBuilder();
        if(result == null){
            return null;
        }
        //Get all the node and create each one with his adjency list
        for (Object o : result) {
            JSONObject link = (JSONObject) o;
            String name = link.get("node").toString();
            Vertex node = graph.getVertexByName(name);
            if (node == null) {
                node = new Vertex(name);
                graph.createVertex(node);
            }
            JSONArray jsonArray = (JSONArray) link.get("succs");
            int succsSize = jsonArray.size();
            for (int j = 0; j<succsSize; j++) {
                String succsName = ((JSONArray) link.get("succs")).get(j).toString();
                Vertex succsNode = graph.getVertexByName(succsName);
                if(succsNode == null){
                    succsNode = new Vertex(succsName);
                    graph.createVertex(succsNode);
                }
                graph.addEdge(node, succsNode);
            }
        }
        return graph;
    }

    /**
     * Init all ids in the graph (0 at the beggining)
     * @param transitionGraph graph where we apply it
     */
    private void initGraphIds(TransitionGraph transitionGraph){
        for(int i = 0; i<transitionGraph.getSize(); i++){
            transitionGraph.getVertex(i).setId(i);
        }
    }

    /**
     *
     * @param transitionGraph graph that we solve
     * @param fileWriter file where we write the output
     * @throws Exception if the graph is null, throw an Exception
     */
    private void resolve(TransitionGraph transitionGraph, FileWriter fileWriter) throws Exception {
        //On crée le graph
        if(transitionGraph == null){
            throw new Exception("Graph is null : Please verify the path");
        } else {
            graph = transitionGraph;
        }

        initGraphIds(graph);

        TarjanCFC solver = new TarjanCFC(graph);

        ArrayList<Integer> cfcs = solver.getCFC();
        Map<Integer, List<Integer>> multimap = new HashMap<>();
        for (int i = 0; i < graph.getSize(); i++){
            if (!multimap.containsKey(cfcs.get(i))){
                multimap.put(cfcs.get(i), new ArrayList<>());
            }
            multimap.get(cfcs.get(i)).add(i);
        }

        System.out.println("La multimap : " + multimap);
        TransitionGraph graphCFC = new TransitionGraph();
        createCFC(multimap, graph, graphCFC);
        createEdgesFromGraph(graph, graphCFC);

        int cfcCount = solver.CFCcount();

        TarjanCFC solverCFC = new TarjanCFC(graphCFC);

        this.edgesMap = multimap;

        System.out.println("####################################");
        System.out.printf("Number of SCC: %d\n", cfcCount);
        System.out.println("####################################");
        if (fileWriter != null) {
            try {
                fileWriter.write(cfcCount + " SCC  found\n");
                for (List<Integer> scc : multimap.values()) {
                    if(scc.size()<=1) {
                        fileWriter.write("The node: [ ");
                        fileWriter.write(graph.getVertexById(scc.get(0)).getName());
                        fileWriter.write(" ] form the SCC : ");
                        fileWriter.write(scc.get(0) + "\n");
                    } else {
                        fileWriter.write("The node : [ ");
                        for(Integer i : scc){
                            Vertex vertex = graph.getVertexById(i);
                            fileWriter.write(vertex.getName() + " ");
                            if(vertex.getPreviousVertices().size()>0){
                                for(Vertex vertex1 : vertex.getPreviousVertices()){
                                    fileWriter.write(vertex1.getName() + " ");
                                }
                            }
                        }
                        fileWriter.write("] form the SCC : ");
                        fileWriter.write(scc.get(0) + "\n");
                    }
                }
                fileWriter.write("\n\n#################################\n\n " + graphCFC);
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("GraphCFC :\n" + graphCFC);
        System.out.println(solver.getCommitmentSet());
    }

    /**
     * Create all the SCCs from the first graph
     * @param edgesMap Map of all CFC
     * @param graph First graph
     * @param graphCFC New graph with only CFC
     */
    private void createCFC(Map<Integer, List<Integer>> edgesMap, TransitionGraph graph, TransitionGraph graphCFC){
        ArrayList<Vertex> sccs = new ArrayList<>();
        for(Integer i: edgesMap.keySet()){
            Vertex scc = new Vertex();
            scc.setId(i);
            scc.setCommitmentSet(false);
            graphCFC.createVertex(scc);
            for(Integer k: edgesMap.get(i)){
                Vertex vertex = graph.getVertexById(k);
                if(vertex.isCommitmentSet()){
                    scc.setCommitmentSet(true);
                }
                scc.concatenateName(vertex.getName());
                scc.getAttractorReached().addAll(vertex.getAttractorReached());
            }
            sccs.add(scc);
        }
        this.sccList = sccs;
    }

    /**
     * Get the SCCs from the Vertex are and create the link between them in the new graph
     * @param from Vertex which has a link
     * @param to Vertex which is linked
     * @param graph SCC graph when we need to apply this link
     */
    private void createLinkSCC(Vertex from, Vertex to, TransitionGraph graph){
        Vertex scc1 = graph.getSCCByName(from.getName());
        Vertex scc2 = graph.getSCCByName(to.getName());

        graph.addEdge(scc1, scc2);
    }

    /**
     * Check all the link, if there is a link between 2 Vertex that are not in the same SCC, create edge in the graph
     * @param from First graph
     * @param to SCC graph
     */
    private void createEdgesFromGraph(TransitionGraph from, TransitionGraph to){
        for(Vertex vertex: from.getVertices()){
            for(Vertex vertex1: from.getAdjencyVertex(vertex)){
                if(!to.areInTheSameVertex(vertex, vertex1)){
                    createLinkSCC(vertex, vertex1, to);
                }
            }
        }
    }


    /**
     * Apply algorithm to the both graphs (sync and async)
     * @param args Path to the json file
     * @throws Exception If the graph is null (due to path for exemple)
     */
    public static void main(String... args) throws Exception {
        CFC cfc = new CFC();
        TransitionGraph transitionGraph;
        System.out.println("Async graph : ");
        transitionGraph = cfc.generateTransGraphFromJSON(args[0], false);
        String filePath = args[0].split(".json")[0] + "-cfc-async.out";
        FileWriter fileWriter = new FileWriter(filePath);
        fileWriter.write("Async graph : \n");
        cfc.resolve(transitionGraph, fileWriter);
        System.out.println("result saved in file " + filePath);

        transitionGraph.cleanUpGraph();

        System.out.println("\n\nSync Graph");
        transitionGraph = cfc.generateTransGraphFromJSON(args[0], true);
        filePath = filePath.split("-async")[0] + ".out";
        fileWriter = new FileWriter(filePath);
        fileWriter.write("Sync graph : \n");
        cfc.resolve(transitionGraph, fileWriter);
        System.out.println("result saved in file " + filePath);
    }
}