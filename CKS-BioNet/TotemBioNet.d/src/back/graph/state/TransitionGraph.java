package back.graph.state;

import back.graph.data.Graph;
import back.graph.data.Vertex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * @author Yanis Benabdelouahed
 * Stage SI4 19-20
 */

public class TransitionGraph extends Graph {

    private ArrayList<Vertex> vertices;
    private HashMap<Vertex, ArrayList<Vertex>> adjencyLists;
    private ArrayList<StronglyConnectedComponent> sccList = new ArrayList<>();


    public TransitionGraph(ArrayList<Vertex> vertices, HashMap<Vertex, ArrayList<Vertex>> adjencyLists){
        this.vertices = vertices;
        this.adjencyLists = adjencyLists;
    }

    public TransitionGraph(){
        this.vertices = new ArrayList<>();
        this.adjencyLists = new HashMap<>();
    }

    public TransitionGraph(TransitionGraph graph){
        this.vertices = graph.vertices;
        this.adjencyLists = graph.adjencyLists;
    }

    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    public HashMap<Vertex, ArrayList<Vertex>> getAdjencyLists() {
        return adjencyLists;
    }

    public void createVertex(Vertex vertex){
        this.vertices.add(vertex);
        this.adjencyLists.put(vertex, new ArrayList<>());
    }

    public void addSCC(StronglyConnectedComponent scc){
        this.sccList.add(scc);
    }

    public void addEdge(Vertex from, Vertex to){
        this.adjencyLists.get(from).add(to);
    }

    public void removeEdge(Vertex from, Vertex to){
        this.adjencyLists.get(from).remove(to);
    }

    public void cleanUpGraph(){
        this.vertices = new ArrayList<>();
        this.adjencyLists = new HashMap<>();
    }

    public int getSize(){
        return this.vertices.size();
    }

    public Vertex getVertex(int index){
        return this.vertices.get(index);
    }

    public ArrayList<Integer> getAdjencyIds(int vertexId){
        ArrayList<Integer> adjencyList = new ArrayList<>();
        Vertex vertex = new Vertex();
        for(Vertex vert : this.vertices){
            if(vert.getId() == vertexId){
                vertex = vert;
                break;
            }
        }
        if (vertex.getName() != null) {
            for(Vertex vertex2: this.adjencyLists.get(vertex)){
                adjencyList.add(vertex2.getId());
            }
        }
        return adjencyList;
    }

    public ArrayList<Vertex> getAdjencyVertex(Vertex vertex){
        return this.adjencyLists.get(vertex);
    }

    public Vertex getVertexById(int id){
        for(Vertex vertex: this.vertices){
            if(vertex.getId() == id){
                return vertex;
            }
        }
        return null;
    }

    public Vertex getVertexByName(String name){
        for(Vertex vertex: this.vertices){
            if(vertex.getName().equals(name)){
                return vertex;
            }
        }
        return null;
    }

    public Vertex getSCCByName(String name){
        for(Vertex vertex: this.vertices){
            ArrayList<String> names = new ArrayList<>();
            Collections.addAll(names, vertex.getName().split(" "));
            if(names.contains(name)){
                return vertex;
            }
        }
        return null;
    }

    public boolean areInTheSameVertex(Vertex vertex1, Vertex vertex2){
        return getSCCByName(vertex1.getName()).equals(getSCCByName(vertex2.getName()));
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder("The graph is composed of : {\n");
        for(Vertex vertex: vertices){
            str.append("Vertex ");
            str.append(vertex.getId());
            str.append(" ");
            str.append(vertex.isCommitmentSet());
            str.append(" : linked to vertex : ");
            for(Vertex vertex1: this.getAdjencyLists().get(vertex)){
                str.append(vertex1.getId());
                str.append(" - ");
            }
            for(Vertex vertex1 : vertex.getAttractorReached()){
                str.append("[").append(vertex1.getId()).append("]");
            }
            str.append("\n");

        }
        str.append("}");
        return str.toString();
    }
}
