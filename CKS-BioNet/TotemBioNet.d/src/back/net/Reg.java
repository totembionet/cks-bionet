package back.net;

import util.jlist.ObjectWithStringID;
import back.logic.*;
import back.logic.Var;

import java.util.HashMap;

/**
 * @author Adrien Richard
 */


public class Reg implements ObjectWithStringID{

    //Nom de la r�gulation
    String name;
    //Formule bool�enne (pr�sence/absence de la r�gulation)
    Formula formula;

    //Constructeur
    public Reg(String name, Formula formula) throws Exception {
        this.name = name;
        this.formula = formula;
        if (formula instanceof Var)
            throw new Exception("The formula connot be reduced to a variable");
    }

    //La description
    public String toString() {
        return name;
    }

    // description format SMB sans la cible
    public String toSMBString() {
        return name + " [" + formula + "]=>";
    }

    //Indique si la formule est pr�sente
    public boolean isEffective() {
        return formula.eval() > 0;
    }

    // to check if the regulation is effective for some values of variables
    public boolean isEffective(HashMap<String, Integer> varValues) {
        return formula.eval(varValues) > 0;
    }

    /////////////////////////////////////////////////////
    // utilisé pour les OrderedArrayListStringID

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Reg reg = (Reg) o;
//        return Objects.equals(name, reg.name) &&
//                Objects.equals(formula, reg.formula);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(name, formula);
//    }

    @Override
    public String stringId() {
        return name;
    }
}