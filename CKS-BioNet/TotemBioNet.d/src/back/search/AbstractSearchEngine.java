package back.search;

/**
 * This class is used to search for valid models, either by using partly-intanciated patterns
 * or constant models.
 * Patterns are used to negate the CTL property to refute many intanciated models.
 *
 * @author Hélène Collavizza
 */

import back.net.Gene;
import back.net.Net;
import back.net.Para;
import mddapi.apitotem.CSVParaWriter;
import mddapi.apitotem.MDDBuilder;
import mddapi.apitotem.MDDParaSet;
import run.MainInfo;
import run.NuSMV;
import util.Out;
import util.StdoutReader;
import util.TotemBionetException;
import util.jclock.Clock;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import static back.search.ParasAsArray.*;


public abstract class AbstractSearchEngine implements Search {
    // le réseau qu'on est en train de traiter
    protected Net net;

    // la commande NuSMV
    protected String nuSMVcommand;

    // nb de modèles OK
    protected static int nbSelected;
    // nb de modèles KO
    protected static int nbKO;
    // nb de modèles soumis à NuSMV
    protected static int nbSolvedModel;

    // sauvegarde des modèles ou pattern résolus
    protected ArrayList<byte[]> OKModels;
    protected ArrayList<byte[]> KOModels;
    // explication des modèles KO
    protected ArrayList<ArrayList<String>> errorExplanation;

    // used if verbose = 2
    protected MDDParaSet OKmdd;

    long nuSMVTime = 0;

    public AbstractSearchEngine(Net n, String nuSMVcommand) {
        init(n);
        this.net = n;
        OKModels = new ArrayList<>();
        KOModels = new ArrayList<byte[]>();
        errorExplanation = new ArrayList<ArrayList<String>>();
        this.nuSMVcommand = nuSMVcommand;
        if (Out.verb()==2)
            this.OKmdd = (new MDDBuilder()).createEmptyFromGene("OKModels",n.genes(),"OK");
    }

    /////////////////////////////////////////////
    // gestion de la recherche
    // resoud un modèle s'il y en a un
    public void search(String name, byte[] param, boolean csv, boolean fromFile) throws Exception {
        Process proc;

        //back.net.printAllCurrentIntervals();
        NuSMV.writeNet(net, name, param, csv,fromFile);

        //System.out.println(".smv file has been generated, launching NuSMV");
        //EXECUTION DE NUSMV
        proc = nuSMVCall(name);
        nbSolvedModel++;
        Iterator<String> iter = StdoutReader.getIteratorStream(proc);
        String line = StdoutReader.readNuSMVHeader(iter);

        // if there is a INIT block, check if the set of initial states is empty
        if (net.hasInitBlock()) {
            StdoutReader.readWarning(proc);
        }
        //StdoutReader.readNuSMVHeader(iter, NuSMV.HEADER_SIZE);
        // si ça s'arrête là, il y a un problème d'execution et on affiche le
        //message d'erreur de NuSMV
        if (!line.startsWith("-- specification")&&!iter.hasNext()) {
            StdoutReader.readNuSMVError(proc);
        } else {
            byte[] model = currentModelAsArray();
            analyseModel(line,iter, model);
        }
        proc.destroy();
    }
    protected Process nuSMVCall(String name) throws IOException {
        long t1= System.currentTimeMillis();
        Process proc = Runtime.getRuntime().exec(nuSMVcommand + " " + name + ".smv");
        long t2= System.currentTimeMillis();
//        MainInfo.nuSMVTime+=t2-t1;
        nuSMVTime+=t2-t1;
        return proc;
    }

    protected boolean falseSpecification(String line) {
        if (line.startsWith("-- specification")) {
            return line.endsWith("is false");
        }
        return false;
    }

    private boolean specificationIsFalse(String line) {
        return line.endsWith("is false");
    }

    private boolean isSpecification(String line) {
        return line.startsWith("-- specification");
    }

    // iter est le stream résultat d'appel de NuSMV
    // analyse ce stream pour gérer les modèles OK ou KO
    protected void analyseModel(String l,Iterator<String> iter, byte[] model) throws Exception {

        int specNumber = 0;
        boolean ok = true;

        // l is the last line
        if (isSpecification(l)) {
            specNumber++;
            if (specificationIsFalse(l)) {
                ok = false;
            }
        }
        String line = "";
        // on part à la recherche d'une spécification fausse
        // dans le cas où il y a plusieurs formules CTL
        while (ok && iter.hasNext()) {
            line = iter.next();
            if (isSpecification(line)) {
                specNumber++;
                if (specificationIsFalse(line))
                    ok = false;
            }
        }
        saveModel(model, ok);
        // le modèle est KO, on explique pourquoi
        // ATTENTION : tout repose sur le fait que NuSMV traite les formules dans
        //             le même ordre que le stockage dans les listes
        //             ctl et fairctl de Net
        if (!ok) {
            newExplanation(nbKO-1, specNumber-1);
            while (iter.hasNext()) {
                line = iter.next();
                if (isSpecification(line)) {
                    specNumber++;
                    if (specificationIsFalse(line)) {
                        addExplanation(nbKO-1, specNumber-1);
                    }
                }
            }
        }
    }
    // créé une explication d'erreur pour le modèle courant et la spec courante
    private void newExplanation(int modelNumber, int specNumber) {
        errorExplanation.add(modelNumber, new ArrayList<String>());
        addExplanation( modelNumber, specNumber);
    }

    // ajoute une explication pour un modèle KO
    private void addExplanation(int modelNumber, int specNumber){
        int nbCtl = net.getCtl().size();
        String spec;
        if (specNumber<nbCtl)
            spec = net.getCtl().get(specNumber).toExplanationString();
        else
            spec = net.getFairCtl().get(specNumber-nbCtl).toExplanationString();
        errorExplanation.get(modelNumber).add(spec);
    }

    // ajoute un modèle aux listes de modèles déjà résolus
    // NOTA : ATTENTION cette méthode est redéfinie dans les FromFileSearch
    protected void saveModel(byte[] model, boolean ok) {
        if (ok) {
            OKModels.add(model);
            //System.out.println(Arrays.toString(model));
            // utilisé seulement en verbose 2 : pas assez efficace et résultat compact pas exploité
            // sur cellCycle: ajout dans MDD 39390 ms, arrayList 15 ms
            if (Out.verb()==2)
                addMDDRow(model,ok);
            nbSelected++;
        } else {
            KOModels.add(model);
            nbKO++;
        }
    }

    // add a row in the MDD => convert from byte to int and discard the first byte which
    // is an information about formal or not
    protected void addMDDRow(byte[] row, boolean ok) {
        int[] intRow = new int[row.length-1];
        int k = 1;
        for (int i=0;i<intRow.length;i++)
            intRow[i]= row[k++];
        if (ok) {
            try {
                OKmdd.addRow(intRow);
            } catch (TotemBionetException e) {
                e.printStackTrace();
            }
        }
        //TODO : same for KOmdd
    }

    protected String getCSVHead() {
        //System.out.println("array " + t1 + " mdd " + t2);
        // n° du modèle, OK ou KO
        //String s = "n°;" + ((Out.verb()==2)?"model id;":"")+"valid;";
        String s = "n°;valid;";
        int i = 0; // n° du gène
        // paramètres
        for (Gene g : net.genes()) {
            if ((!net.isEnvVar(g))) {
                for (Para p : g.getParas()) {
                    s += p.getSMBname() + ";";
                }
                // si csv et verbose 2 alors on met aussi les param non effectifs
                if (Out.verb()>=2) {
                    ArrayList<String> forb = net.forbiddenParasForGene(g);
                    setNonEffective(i,forb.size());
                    for (String f : forb)
                        s += f + ";";
                }
            }
            i++;
        }
        // colonne d'explication d'erreur
        s += "Error explanation";
        if (Out.verb()==3)
            s += ";model id";
        s+="\n";
        return s;
    }

    // utilisé pour ajouter une ligne avec le domaine des paramètres
    // utile pour les intersections
    // appelé seulement en mode -csv -verbose 2
    protected String getParaDomains() {
        // n° du modèle, OK ou KO
        String s = ";;";
        // paramètres
        for (Gene g : net.genes()) {
            if ((!net.isEnvVar(g))) {
                // on écrit le domaine fournit par l'utilisateur dans le bloc PARA
                // s'il s'agissait d'un intervalle
                // si le para est fixe, on met le domaine de la variable
                for (Para p : g.getParas()) {
                    s += p.minLevel() + ".." + p.maxLevel() + ";";
                }
                if (Out.verb()>=2) {
                    // on écrit le domaine de la variable pour les paras non effectifs
                    String varDomain = g.min() + ".." + g.max() + ";";
                    ArrayList<String> forb = net.forbiddenParasForGene(g);
                    for (int i = 0; i < forb.size(); i++)
                        s += varDomain;
                }
            }
        }
        // colonne d'explication d'erreur et éventuellement model id
        s += (Out.verb()==3)?";;\n":";\n";
        return s;
    }

    // search type dependent information
    protected abstract void writeInfoModels(Clock c) throws Exception;

    public void writeSearch(Clock c) throws Exception {
        //System.out.println("Order of parameters when printing");
        //System.out.println(ParasAsArray.printGeneParam());
        writeInfoModels(c);
        //Out.pfln("Order of parameters when printing");
        //Out.pfln(ParasAsArray.printGeneParam());
        Out.psfln("################################################\n");
        for (int i = 0; i < OKModels.size(); i++) {
            writeDetailedModel(OKModels.get(i), true, i);
            Out.pfln("");
        }
        Out.pfln("\n########## KO models #########\n");
        for (int i = 0; i < KOModels.size(); i++) {
            writeDetailedModel(KOModels.get(i), false, i);
            Out.pf("   Error explanation: " + errorExplanation.get(i).toString());
            Out.pfln("");
        }
    }


    // write the OK and KO models in the csv file
   public void writeSearchInCSV(Clock c, String file) throws Exception {
       //System.out.println("Order of parameters when printing");
       //System.out.println(ParasAsArray.printGeneParam());
       writeInfoModels(c);
       String name = file + ".csv";
       String where = (Out.verb()==2)?name + " and " + file+"-MDD.csv":name;
       Out.psfln("Models have been written in " + where);
       Out.psfln("################################################");
       Out.psfln();
       BufferedWriter w = new BufferedWriter(new FileWriter(new File(name)));
       w.write(getCSVHead());
       w.write(getParaDomains());
       int n = 1;
       for (byte[] b : OKModels) {
           String line = getCSV(n++, b, true);
           if (Out.verb() == 3) {
               line += ";" + getId(b);
           }
           w.write(line + "\n");
       }
       //w.write(separationLine(back.net.nbParams()));
       int i = 0;
       while (i < KOModels.size()) {
           String line = getCSV(n++, KOModels.get(i), false);
           // écriture de l'erreur
           line += "\"" + errorExplanation.get(i).toString() + "\"";
           if (Out.verb() == 3) {
               line += ";" + getId(KOModels.get(i));
           }
           w.write(line + "\n");
           i++;
       }
       // fermeture du fichier
       w.close();
       if (Out.verb()==2) {
            writeMDD(file);
       }
   }

   private void writeMDD(String f) throws IOException, TotemBionetException {
        CSVParaWriter pw = new CSVParaWriter(f+"-MDD.csv",OKmdd.unusedVariables());
        pw.writeCSV(OKmdd);

   }

    //     renvoie une String où chaque caractère correspond à la valeur d'un des paramètres
//     # devant car sinon excel transforme 00001 en 1 par exemple (format nombre au lieu de texte)
    private static String getId(byte[] pattern){
        String res = "#";
        for (int i=1;i<pattern.length;i++){
            res +=pattern[i]+"";
        }
        return res;
    }

}
