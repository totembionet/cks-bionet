package back.search;

import java.io.IOException;

public interface ParaEnumerator {

    void firstParameterization();

    boolean nextParameterization();
}
