package back.search;

/**
 * This class is used to search for valid models from a set of models in a .csv file
 * <psf>
 * All models are enumerated but only models that validate Hoare constraints are
 * generated and checked with NuSMV
 *
 * @author Hélène Collavizza
 */

import back.hoare.HoareTriplets;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import run.Main;
import util.Out;
import util.TotemBionetException;
import util.jclock.Clock;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;


public class FromFileSearchEngineWithHoare extends AbstractSearchEngine {

    // nb modèles non compatibles avec les contraintes de Hoare
    static long nbHoareRemovedModel = 0;

    // les formules de Hoare à valider
    private HoareTriplets hoare;

    // the enumerator built from the csv file
    static FromFileParaEnumerator ffParaEnum;

    public FromFileSearchEngineWithHoare(Net n, String nuSMVcommand,String path,String type) throws TotemBionetException, IOException {
        super(n, nuSMVcommand);
        int ind = path.lastIndexOf('/');
        String fileName = (ind==-1)?path:path.substring(ind+1);
        ffParaEnum = new FromFileParaEnumerator(type,path,net.genes());
        hoare = n.getHoare();
    }

    /////////////////////////////////////////////
    // gestion de la recherche

    // information on search process
    // statistiques affichées toutes les secondes
    public static String stats() {
        String s1 = " [#Checked Models : " + nbSolvedModel + "] ";
        String h = " [#Hoare Removed Models : " + nbHoareRemovedModel + "] ";
        String s2 = "[#Selected  Models : " + nbSelected + "] ";
        return s1 + h + s2;
    }

    ///////////////////////////////
    // méthodes de recherche de l'interface Search

    public void initSearch(String name) throws Exception {
        // on calcule le 1er modèle
        ffParaEnum.firstParameterization();
    }

    public boolean hasNext() {
        return ffParaEnum.nextParameterization();
    }

    // valeur actuelle de chaque paramètre
    private HashMap<String, Integer> currentModelValue(byte[] m) {
        HashMap<String, Integer> model = new HashMap<String, Integer>();
        int i=1;
        for (Gene g : net.genes()) {
            for (Para p : g.getParas()) {
                 model.put(p.getSMBname(), (int) m[i++]);
            }
        }
        return model;
    }

    @Override
    // resoud un modèle valide
    public void doSearch(String name, boolean csv) throws Exception {
        if (!ffParaEnum.stop) {
            byte[] m = ffParaEnum.getCurrentModel();
            HashMap<String, Integer> model = currentModelValue(m);
            if (hoare.valid(model))
                super.search(name, m, csv, true);
            else {
                if (Out.verb() > 0)
                    Out.psfln("Hoare removed model: " + model);
                nbHoareRemovedModel++;
            }
        }
    }

       // ajoute un modèle aux listes de modèles déjà résolus
    @Override
    protected void saveModel(byte[] model, boolean ok) {
        if (ok) {
            if (!ffParaEnum.needCheck()||(ffParaEnum.needCheck() && !contains(model,true)))
                OKModels.add(model);
            if (Out.verb()==2)
                super.addMDDRow(model,ok);
            nbSelected++;
        } else {
            KOModels.add(model);
            nbKO++;
        }
    }

    // pour savoir si le modèle existe déjà (possible à cause des - ou min..max dans .csv
    private boolean contains(byte[] model, boolean ok) {
        for (byte[] b : OKModels) {
            if (Arrays.equals(model, b))
                return true;
        }
        return false;
    }

    ////////////////////
    public void writeInfoModels(Clock c) throws Exception {
        ffParaEnum.close();
        Out.psfln();
        Out.psfln();
        Out.psfln("########### Result of model checking ###########");
        Out.psfln("# Total number of models: " + super.net.NB_PARAMETERIZATIONS);
        Out.psfln("# Number of " + Main.fromType() + " models in csv file " + ffParaEnum.nbModels);
        Out.psfln("# Number of models removed using Hoare: " + HoareTriplets.nbRemovedParam);
        Out.psfln("# Selected models: " + super.OKModels.size());
        Out.psfln("# Computation time: " + c);

    }


       // sauvegarde des résultats
    public void writeSearchInCSV(Clock c, String file) throws Exception {
        super.writeSearchInCSV(c, file+"-from");
    }


}
