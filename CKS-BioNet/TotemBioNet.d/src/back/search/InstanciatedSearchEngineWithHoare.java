package back.search;

/**
 * This class is used to search for valid models.
 * <psf>
 * All models are enumerated but only models that validate Hoare constraints are
 * generated and checked with NuSMV
 *
 * @author Hélène Collavizza
 */

import back.hoare.HoareTriplets;
import back.hoare.OneHoareTriplet;
import run.Run;
import util.jclock.Clock;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import run.NuSMV;
import util.Out;
import util.StdoutReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;


public class InstanciatedSearchEngineWithHoare extends AbstractSearchEngine {

    // nb modèles non compatibles avec les contraintes de Hoare
    static long nbHoareRemovedModel = 0;

    // les formules de Hoare à valider
    private HoareTriplets hoare;

    public InstanciatedSearchEngineWithHoare(Net n, String nuSMVcommand) {
        super(n, nuSMVcommand);
        hoare = n.getHoare();
    }

    /////////////////////////////////////////////
    // gestion de la recherche

    // information on search process
    // statistiques affichées toutes les secondes
    public static String stats() {
        String s1 = " [#Checked Models : " + nbSolvedModel + "] ";
        String h = " [#Hoare Removed Models : " + nbHoareRemovedModel + "] ";
        String s2 = "[#Selected  Models : " + nbSelected + "] ";
        return s1 + h + s2;
    }

    ///////////////////////////////
    // méthodes de recherche de l'interface Search
    public void initSearch(String name) throws Exception {
        // on calcule le 1er modèle
        super.net.firstParameterization();
    }

    public boolean hasNext() {
        return super.net.nextParameterization();
    }

    // valeur actuelle de chaque paramètre
    private HashMap<String, Integer> currentModelValue() {
        HashMap<String, Integer> model = new HashMap<String, Integer>();
        for (Gene g : net.genes()) {
            for (Para p : g.getParas()) {
                model.put(p.getSMBname(), p.currentMin());
            }
        }
        return model;
    }

    // si l'état courant est OK pour Hoare, on vérifie ce modèle sinon on ne fait rien
    // => on passe à l'état suivant
    public void doSearch(String name, boolean csv) throws Exception {
        HashMap<String, Integer> model = currentModelValue();
        //System.out.println(model);
        if (hoare.valid(model))
            super.search(name, null, csv, false);
        else {
            if (Out.verb() > 0)
                Out.psfln("Hoare removed model: " + model);
            nbHoareRemovedModel++;
        }
    }

    ////////////////////
    public void writeInfoModels(Clock c) throws Exception {
        Out.psfln();
        Out.psfln();
        Out.psfln("########### Result of model checking ###########");
        Out.psfln("# Total number of models: " + super.net.NB_PARAMETERIZATIONS.add(HoareTriplets.nbRemovedParam));
        Out.psfln("# Number of models removed using Hoare: " + HoareTriplets.nbRemovedParam);
        Out.psfln("# Remaning models to check: " + super.net.NB_PARAMETERIZATIONS);
        Out.psfln("# Selected models: " + super.OKModels.size());
        Out.psfln("# Computation time: " + c);

    }

    // sauvegarde des résultats
    public void writeSearchInCSV(Clock c, String file) throws Exception {
        //System.out.println("Order of parameters when printing");
        //System.out.println(ParasAsArray.printGeneParam());
        super.writeSearchInCSV(c,file);
    }


}
