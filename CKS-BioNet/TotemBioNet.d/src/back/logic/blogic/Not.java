package back.logic.blogic;

import back.logic.Formula;
import back.logic.Var;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */


public class Not extends Formula {

    private Formula right;

    public Not(Formula right) {
        this.right = right;
    }

    public Formula getRight() {
        return right;
    }

    public int eval() {
        if (right.eval() > 0)
            return 0;
        return 1;
    }

    public int eval(HashMap<String, Integer> state) {
        if (right.eval(state) > 0)
            return 0;
        return 1;
    }

    @Override
    public Formula negate() {
        return right;
    }

    @Override
    public Formula fairBio() {
        // try to simplify not(not(r)) as r
        Formula r = right.fairBio();
        if (r instanceof Not)
            return ((Not)r).right;
        return new Not(r);
    }

    public String toString() {
        return "(" + Formula.NOT + " " + right + ")";
    }

    public boolean insert(Var v) {
        if ((right instanceof Var) && ((Var) right).name.equals(v.name)) {
            right = v;
            return true;
        } else
            return right.insert(v);
    }

    public void set(String name, int level) {
        right.set(name, level);
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Not expression is not a comparator");
    }

    @Override
    public String toJsonString() {
        return "{\"unop\":" + "\"NEG\""  + ",\"right\":" + right.toJsonString() +"}";
    }

    @Override
    public String toYices() {
        return "(not " +  getRight().toYices()+")";
    }

    @Override
    public Formula translateCTL() {
        // try to simplify not(not(r)) as r
        Formula r = right.translateCTL();
        if (r instanceof Not)
            return ((Not)r).right;
        return new Not(r);
    }

    @Override
    // this is partial because we assume that not only apply to comparators,
    public Formula toPartialDNF() {
        throw new RuntimeException("DNF for constraints in model checking should not be applied to NOT");
    }

}