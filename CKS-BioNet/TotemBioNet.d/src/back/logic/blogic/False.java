package back.logic.blogic;

import back.logic.Formula;
import back.logic.Var;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * Boolean constant False
 *
 * Useful for constraints in parametric transition graph
 */

public class False extends Formula {

    private boolean bval;

    public False() {
        bval = false;
    }
    @Override
    public int eval() {
        return 0;
    }

    @Override
    public int eval(HashMap<String, Integer> paraState) {
        return 0;
    }

    @Override
    public Formula negate() {
        return new True();
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        return null;
    }

    @Override
    public Formula fairBio() {
        return this;
    }

    @Override
    public void set(String name, int level) {
    }

    @Override
    public boolean insert(Var v) {
        return false;
    }

    @Override
    public String toJsonString() {
        return "{\"bval\":false}";
    }

    public String toString() {
        return "False";
    }

    @Override
    public String toYices() {
        return "false";
    }

    @Override
    public Formula translateCTL() {
        return this;
    }

    @Override
    public Formula toPartialDNF() {
        if (id==0)
            id=Formula.CNF_COUNTER++;
        return this;
    }

}
