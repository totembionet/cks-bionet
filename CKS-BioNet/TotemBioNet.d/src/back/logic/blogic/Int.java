package back.logic.blogic;

import back.logic.Formula;
import back.logic.Var;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 */


public class Int extends Formula {

    public final int i;

    public Int(int i) {
        this.i = i;
    }

    public int eval() {
        return i;
    }

    public int eval(HashMap<String, Integer> paraState) {
        return i;
    }

    @Override
    public Formula negate() {
        return this;
    }

    @Override
    public Formula fairBio() {
        return this;
    }

    public String toString() {
        return i + "";
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Integer expression is not a comparator");
    }
    @Override
    public String toJsonString() {
        return "{\"val\":" + i + "}";

    }
    public boolean insert(Var v) {
        return false;
    }

    public void set(String name, int level) {
    }

    public void setColor(int c) {
    }

    public boolean colorOfAllVarIs(int c) {
        return true;
    }

    @Override
    public String toYices() {
        if (i<0)
            throw new IllegalArgumentException("AIE!!! entier négatif ");
        return "" + i;
    }

    @Override
    public Formula translateCTL() {
        return this;
    }

    @Override
    public Formula toPartialDNF() {
        if (id==0)
            id=Formula.CNF_COUNTER++;
        return this;
    }

}