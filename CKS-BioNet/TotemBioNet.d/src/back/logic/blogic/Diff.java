package back.logic.blogic;

import back.logic.Formula;
import back.net.Para;
import util.TotemBionetException;

import java.util.HashMap;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public class Diff extends Comparator {

    public Diff(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        if (getLeft().eval() != getRight().eval())
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        if (getLeft().eval(state) != getRight().eval(state))
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.DIFF + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return this;
    }

    // on suppose que c'est de la forme (var == val)
    public void setParaValue(Para p) throws TotemBionetException {
        throw new TotemBionetException("!= is not yet implemented in Hoare" );
    }

    @Override
    public String toJsonString() {
        return "{\"op\":" + "\"NEQ\"" + ",\"left\":" + getLeft().toJsonString() + ",\"right\":" + getRight().toJsonString() +"}";
    }

    @Override
    public String toYices() {
        return "(neq " + getLeft().toYices() + " " + getRight().toYices()+")";
    }
}