package back.logic.blogic;

import back.logic.Formula;
import back.logic.Var;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * Booelan constant True
 *
 * Useful for constraints in parametric transition graph
 */

public class True extends Formula {

    private boolean bval;

    public True() {
        bval = true;
        id =0;
    }
    @Override
    public int eval() {
        return 1;
    }

    @Override
    public int eval(HashMap<String, Integer> paraState) {
        return 1;
    }

    @Override
    public Formula negate() {
        return new False();
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        return null;
    }

    @Override
    public Formula fairBio() {
        return this;
    }

    @Override
    public void set(String name, int level) {

    }

    @Override
    public boolean insert(Var v) {
        return false;
    }

    @Override
    public String toJsonString() {
        return "{\"bval\":true}";
    }

    public String toString() {
        return "True";
    }

    @Override
    public String toYices() {
        return "true";
    }

    @Override
    public Formula translateCTL() {
        return this;
    }

    @Override
    public Formula toPartialDNF() {
        id=Formula.CNF_COUNTER++;return this;
    }

    // true only for formula True
    @Override
    public boolean isTrueFormula() {
        return true;
    }


}
