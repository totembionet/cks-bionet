package back.logic.blogic;

import back.logic.Formula;
import back.logic.Var;
import back.net.Para;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hélène Collavizza
 */
public abstract class Comparator extends BooleanFormula {


    public Comparator(Formula l, Formula r) {
        super(l, r);
    }

    @Override
    public List<Formula> toSetOfConjunct() {
        ArrayList<Formula> f = new ArrayList<Formula>();
        f.add(this);
        return f;
    }

    public abstract void setParaValue(Para p) throws Exception;

    public abstract String toJsonString() ;

    // used in StateGraphGenerator for constraints on edges in parametric stg
//    public String getJsonParaConstraint() {
//        Var var = (Var)getLeft();
//        Int val = (Int)getRight();
//        String op = (this instanceof GreatEq )? "\"geq\"":"\"leq\"";
//        return "{\"op\": " + op + ",\"p\": \"" + var + "\",\"t\":" + val + "}";
//    }

    @Override
    public boolean isAtom() {
        return true;
    }

    public Formula translateCTL() {
        return this;
    }

    @Override
    public Formula toPartialDNF() {
        if (id==0)
            id=Formula.CNF_COUNTER++;
        return this;
    }

}
