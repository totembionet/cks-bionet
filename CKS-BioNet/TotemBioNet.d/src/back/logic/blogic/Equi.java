package back.logic.blogic;

import back.logic.Formula;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public class Equi extends BooleanFormula {

    public Equi(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        int left = getLeft().eval();
        int right = getRight().eval();
        if ((left > 0 && right > 0) || (left < 1 && right < 1))
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        int left = getLeft().eval(state);
        int right = getRight().eval(state);
        if ((left > 0 && right > 0) || (left < 1 && right < 1))
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.EQUI + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return new Equi(getLeft().fairBio(), getRight().fairBio());
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Equivalence is not a comparator");
    }

    @Override
    public String toJsonString() {
        return "{\"op\":" + "\"EQUI\"" + ",\"left\":" + getLeft().toJsonString() + ",\"right\":" + getRight().toJsonString() +"}";
    }

    @Override
    public String toYices() {
        return "(<=> " + getLeft().toYices() + " " + getRight().toYices()+")";
    }

    @Override
    public Formula translateCTL() {
        return new And(
                new Or(new Not(getLeft().translateCTL()),getRight().translateCTL()),
                new Or(new Not(getRight().translateCTL()),getLeft().translateCTL()));
    }

    // this is partial because we assume that not only apply to comparators,
    public Formula toPartialDNF() {
        throw new RuntimeException("DNF for constraints in model checking should not be applied to EQUI");
    }
}