package back.logic.blogic;

import back.logic.Formula;
import util.TotemBionetException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public class And extends BooleanFormula {

    public And(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        if (getLeft().eval() > 0 && getRight().eval() > 0)
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        if (getLeft().eval(state) > 0 && getRight().eval(state) > 0)
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.AND + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return new And(getLeft().fairBio(), getRight().fairBio());
    }

    @Override
    public String toJsonString() {
        return "{\"op\":" + "\"AND\"" + ",\"left\":" + getLeft().toJsonString() + ",\"right\":" + getRight().toJsonString() +"}";
    }

//    @Override
//    public Formula negate() {
//        if (!(getLeft() instanceof Imply) && !(getRight() instanceof Imply))
//            return super.negate();
//        if (getLeft() instanceof Imply)
//            ((Imply) getLeft()).setRoot();
//        if (getRight() instanceof Imply)
//            ((Imply) getRight()).setRoot();
//        return new Or(getLeft().negate(),getRight().negate());
//    }

    // @author Hélène Collavizza
    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        ArrayList<Formula> conjonct = new ArrayList<Formula>();
        conjonct.addAll(getRight().toSetOfConjunct());
        conjonct.addAll(getLeft().toSetOfConjunct());
        return conjonct;
    }

    @Override
    public String toYices() {
        return "(and " + getLeft().toYices() + " " + getRight().toYices() + ")";
    }

    @Override
    public Formula translateCTL() {
        return new And(getLeft().translateCTL(),getRight().translateCTL());
    }

    @Override
    public Formula toPartialDNF() {
        // recursive call to get children in DNF
        Formula l = getLeft().toPartialDNF();
        Formula r = getRight().toPartialDNF();
        // to simplify a&a
        if (l.id==r.id)
            return l;
        // recursive call to distribute on the children
        if (r instanceof Or) {
            Formula a1 = (new And(l,((Or) r).getLeft())).toPartialDNF();
            Formula a2 = (new And(l,((Or) r).getRight())).toPartialDNF();
            // here a1 was created before a2 thus its if is less
            Formula res = new Or(a1,a2);
            res.id = Formula.CNF_COUNTER++;
            return res;
        }
        if (l instanceof Or) {
            Formula a1 = (new And(r,((Or) l).getLeft())).toPartialDNF();
            Formula a2 = (new And(r,((Or) l).getRight())).toPartialDNF();
            Formula res = new Or(a1,a2);
            res.id = Formula.CNF_COUNTER++;
            return res;
        }
        this.id = Formula.CNF_COUNTER++;
        return this;

//        if ((r instanceof Or)&&(l instanceof Or)) {
//            Formula oll = ((Or) l).getLeft();
//            Formula olr = ((Or) l).getRight();
//            Formula orl = ((Or) r).getLeft();
//            Formula orr = ((Or) r).getRight();
//            Formula a1,a2;
//            if (oll.id==orl.id)
//                a1 = oll;
//            else {
//                a1=new And(oll,orl);
//                a1=a1.toPartialDNF();
//            }
//            if (oll.id==orr.id)
//                a2 = oll;
//            else {
//                a2=new And(oll,orr);
//                a2=a2.toPartialDNF();
//            }
//            Formula res1 = new Or(a1,a2);
//            res1.id = Formula.CNF_COUNTER++;
//            Formula a3,a4;
//            if (olr.id==orl.id)
//                a3 = olr;
//            else {
//                a3=new And(olr,orl);
//                a3=a3.toPartialDNF();
//            }
//            if (olr.id==orr.id)
//                a4 = olr;
//            else {
//                a4=new And(olr,orr);
//                a4=a4.toPartialDNF();
//            }
//            Formula res2
//            res = new Or(res, new And(new And(olr,orl),new And(olr,orr)));
//            res.id = Formula.CNF_COUNTER++;
//            return res.toPartialDNF();
//        }
//        if (r instanceof Or) {
//            Formula res = new Or(new And(l,((Or) r).getLeft()),new And(l,((Or) r).getRight()));
//            res.id = Formula.CNF_COUNTER++;
//            return res.toPartialDNF();
//        }
//        if (l instanceof Or) {
//            Formula res = new Or(new And(r,((Or) l).getLeft()),new And(r,((Or) l).getRight()));
//            res.id = Formula.CNF_COUNTER++;
//            return res.toPartialDNF();
//        }
//        this.id = Formula.CNF_COUNTER++;
//        return this;
    }



}