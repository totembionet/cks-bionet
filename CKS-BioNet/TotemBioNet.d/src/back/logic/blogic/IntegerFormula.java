package back.logic.blogic;

import back.logic.Formula;
import util.TotemBionetException;

import java.util.List;


/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */


public abstract class IntegerFormula extends BinaryConnective {

    public IntegerFormula(Formula l, Formula r) {
        super(l, r);
    }

    public Formula negate() {
        return this;
    }

    @Override
    public Formula fairBio() {
        return this;
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Integer expression is not a comparator");
    }

    @Override
    public Formula translateCTL() {
        return this;
    }

    @Override
    public Formula toPartialDNF() {
        if (id==0)
            id=Formula.CNF_COUNTER++;
        return this;
    }
}
