package back.logic;

import back.logic.blogic.Not;
import back.logic.blogic.True;
import util.TotemBionetException;
import java.util.*;

/**
 *
 *
 * @author Hélène Collavizza pour la partie logique temporelle et négation
 *
 *  à partir d'une version d'Adrien Richard
 *  le parsing des expressions d'Adrien Richard a été complètement remplacé
 *  par le parsing antlr4
 *
 * TODO : la négation est not(this) sauf :
 * - pour une formule P=>Q où elle est psf=>notQ
 * - pour une formule P&Q avec P ou Q implication où elle est !P | !Q
 */

abstract public class Formula implements Comparable{

    // used each time a new formula is created when converting into Conjunctive Normal Form
    public static int CNF_COUNTER = 0;

    public String name;

    public int id;

    private int height; // used by parametric model checker

    protected Formula() {
        name = null;
    }

    // NOTA: DANGER
    // je l'avais mis pour modelChecker mais ça casse l'option -paras car ça change le contains !!!
//    @Override
//    public int hashCode() {
//        return id;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        return id == ((Formula)obj).id;
//    }

    public String toExplanationString() {
        if (name!=null)
            return name;
        return toString();
    }

    // unités lexicales utilisées pour générer les opértaeurs NuSMV

    // connecteurs logiques
    protected static final String EQUI = "<->";
    protected static final String IMPLY = "->";
    protected static final String OR = "|";
    protected static final String AND = "&";
    protected static final String NOT = "!";

    // opérateurs de comparaison
    protected static final String LESS_EQ = "<=";
    protected static final String LESS = "<";
    protected static final String GREAT_EQ = ">=";
    protected static final String GREAT = ">";
    protected static final String DIFF = "!=";
    protected static final String EQUA = "=";

    // opérateurs entiers
    protected static final String SOUS = "-";
    protected static final String ADD = "+";
    protected static final String DIV = "/";
    protected static final String MULT = "*";

    // opérateurs temporels
    protected static final String AG = "AG";
    protected static final String EG = "EG";
    protected static final String AX = "AX";
    protected static final String EX = "EX";
    protected static final String AF = "AF";
    protected static final String EF = "EF";


    //METHODES ABSTRAITES

    //�valuation de la formule
    abstract public int eval();

    //�valuation de la formule pour un modèle donné
    // i.e. une instantiation des paramètres
    abstract public int eval(HashMap<String, Integer> paraState);

    // return the negation of f
    abstract public Formula negate();

    // return a list that contains all the formulas of a conjunction
    // throws an exception if it is not a conjunction
    // NOTA: this is an ack to avoid to look over the formula twice :
    //       one to know if it contains only conjuncts and second to compute the list
    // @author Hélène Collavizza
    abstract public List<Formula> toSetOfConjunct() throws TotemBionetException;

    // transforme la formule CTL en formule CTL fair au sens bio
    // de la note d'Adrien Richard
    // @author Hélène Collavizza
    abstract public Formula fairBio();

    //initialisation du niveau de la variable SMBname
    abstract public void set(String name, int level);

    //insertion d'une variable dans la formule
    abstract public boolean insert(Var v);

    // représentation de la formule en format json
    abstract public String toJsonString();

    // to run yices on the formula to know if it is SAT
    public void runYices(String fileName) {
        //TODO: appeler toSetOfConjunct. Si ok, faire des assert pour chaque formule traduite en yices
        //      sinon faire un gros assert
    }

    // true only for BooleanFormulas and Not
    public boolean isAtom() {
        return false;
    }

    // true only for CTL formulas
    public boolean isCTL() {
        return false;
    }

    // true only for formula True
    public boolean isTrueFormula() {
        return false;
    }

    // String representation in yices syntax
    public abstract String toYices();

    // to translate ctl formulas with checkable connectors EX,EU and EG
    public abstract Formula translateCTL();

    // to put the formula into Conjunctive normal form
    public abstract Formula toPartialDNF();

    public String toStringWithId(){
        return toString() + "[" + id + "]";
    }



    // a special equals
    public boolean DNSEquals(Formula f) {
        if (this==f)
            return true;
        if (id==f.id)
            return true;
        return false;
    }


    @Override
    public int compareTo(Object o) {
        return id - ((Formula)o).id;
    }

    // used for parametric model checker
    //////////////////////////////////////

    public void setId(int id) {
        this.id=id;
    }

    public void setHeight(int h) {
        this.height=h;
    }

    public int getHeight() {
        return this.height;
    }

    // used in translateCTL (todo: in fairBio)
    // f is the result of the call of translateCTL on a child
    // if the translation is not, makeNot return f
    public static Formula makeNot(Formula f) {
        if (f instanceof Not)
            return ((Not) f).getRight();
        return new Not(f);
    }

    public static final Formula TRUE=new True();

}
