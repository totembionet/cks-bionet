package back.logic.CTLlogic;

import back.logic.Formula;
import back.logic.blogic.Not;

import java.text.Normalizer;

/**
 * classe pour représenter l'opérateur CTL AX
 *
 * @author Hélène Collavizza
 */

public class AllNeXt extends UnaryCTLFormula {

    public AllNeXt(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.AX + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new ExistNeXt(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        return new AllNeXt(getLeft().fairBio());
    }

    @Override
    public String toJsonString() {
        return "{\"uctl\":" + "\"AX\"" + ",\"left\":" + getLeft() .toJsonString() +"}";
    }

    @Override
    public Formula translateCTL() {
        return new Not(new ExistNeXt(Formula.makeNot(getLeft().translateCTL())));
    }

}
