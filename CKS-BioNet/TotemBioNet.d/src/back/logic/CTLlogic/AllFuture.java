package back.logic.CTLlogic;

import back.logic.Formula;
import back.logic.blogic.Not;

/**
 * classe pour représenter l'opérateur CTL AF
 *
 * @author Hélène Collavizza
 */

public class AllFuture extends UnaryCTLFormula {

    public AllFuture(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.AF + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new ExistFuture(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        Formula f = new ExistGenerally(new Not(getLeft().fairBio()));
        return new Not(f.fairBio());
    }

    @Override
    public String toJsonString() {
        return "{\"uctl\":" + "\"AF\"" + ",\"left\":" + getLeft().toJsonString()  +"}";
    }

    @Override
    public Formula translateCTL() {
        return new Not(new ExistGenerally(Formula.makeNot(getLeft().translateCTL())));
    }
}
