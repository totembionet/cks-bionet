package back.logic.CTLlogic;

import back.logic.Formula;
import back.logic.blogic.Not;

/**
 * classe pour représenter l'opérateur CTL AG
 * *
 *
 * @author Hélène Collavizza
 */


public class AllGenerally extends UnaryCTLFormula {

    public AllGenerally(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.AG + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new ExistGenerally(getLeft().negate());
    }
    */

    @Override
    public Formula fairBio() {
        return new AllGenerally(getLeft().fairBio());
    }

    @Override
    public String toJsonString() {
        return "{\"uctl\":" + "\"AG\"" + ",\"left\":" + getLeft().toJsonString()  +"}";
    }

    @Override
    public Formula translateCTL() {
        return new Not(new ExistUntil(Formula.TRUE,Formula.makeNot(getLeft().translateCTL())));
    }
}
