package back.logic.CTLlogic;

import back.logic.Formula;
import back.logic.Var;
import back.logic.blogic.Not;

/**
 * classe pour représenter les opérateurs CTL unaires
 *
 * @author Hélène Collavizza
 */

abstract public class UnaryCTLFormula extends CTLFormula {

    private Formula left;

    public UnaryCTLFormula(Formula left) {
        this.left = left;
    }

    public Formula getLeft() {
        return left;
    }

    public void set(String name, int level) {
        left.set(name, level);
    }

    public boolean insert(Var v) {
        boolean leftInsert, rightInsert;
        if ((left instanceof Var) && ((Var) left).name.equals(v.name)) {
            left = v;
            leftInsert = true;
        } else
            leftInsert = left.insert(v);
        return leftInsert;
    }

    @Override
    public Formula negate() {
        return new Not(this);
    }
}