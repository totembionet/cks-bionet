package util;

/**
 * @author Hélène Collavizza
 */


public class TotemBionetException extends Exception {

    public TotemBionetException(String m) {
        super("  [TotemBioNet error] " + m);
    }
}
