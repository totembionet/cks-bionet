package util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author Hélène Collavizza
 * Classe pour gérer les messages renvoyés sur stdout ou stderror par NuSMV
 */
public class StdoutReader {

    // renvoie un itérateur sur le flot
    public static Iterator<String> getIteratorStream(Process proc) {
        //LECTURE DU RESULTAT
        InputStream inputstream = proc.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputstream));
        Iterator<String> iter = reader.lines().iterator();
        return iter;
    }

    // saute le message header de NuSMV
    public static void readNuSMVHeader(Iterator<String> iter, int n) {
        String line = "";
        int nbLine = 0;
        //On passe les 26 premières lignes
        while (iter.hasNext() && nbLine < n) {
            line = iter.next();
            nbLine++;
        }
    }

    // saute le message header de NuSMV
    public static String readNuSMVHeader(Iterator<String> iter) {
        String line = "";
        int nbLine = 0;
        //On passe les 26 premières lignes
        while ((line.length()==0 || line.startsWith("***")|| line.startsWith("WARNING ***"))&&iter.hasNext()) {
            line = iter.next();
            nbLine++;
        }
        return line;
    }

    // to read warning message when the init block is unfair
    public static void readWarning(Process proc) throws Exception {
        InputStream errstream = proc.getErrorStream();
        InputStreamReader errstreamreader = new InputStreamReader(errstream);
        BufferedReader reader = new BufferedReader(errstreamreader);

        String message = "";
        String line = "";
        while ((line = reader.readLine()) != null && !line.startsWith("********   WARNING   ********")) {
        }
        if (line!=null) {
            message = "   " +line + "\n";
            while ((line = reader.readLine()) != null && !line.startsWith("******** END WARNING ********")) {
                message+="   " + line+ "\n";
            }
            message+="   " +line+"\n";
            throw new TotemBionetException("INIT block unsatisfiable. NuSMV message is the following: \n" + message);
        }

    }

    // pour ré-afficher les messages d'erreurs de NuSMV
    public static void readNuSMVError(Process proc) throws Exception {
        InputStream errstream = proc.getErrorStream();
        InputStreamReader errstreamreader = new InputStreamReader(errstream);
        BufferedReader reader = new BufferedReader(errstreamreader);

        String message = "";
        String line = "";
        while ((line = reader.readLine()) != null) {
            message += line + "\n";
        }
        throw new Exception(message);
    }
}
