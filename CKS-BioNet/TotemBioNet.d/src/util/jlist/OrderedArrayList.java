package util.jlist;

import java.util.ArrayList;

/**
 * Used in parametric model checking to order atoms in conjunctions
 * @param <E> any Comparable class
 */

public class OrderedArrayList<E extends Comparable> extends ArrayList<E> {

    // add an element only if it is not already in the list
    @Override
    public boolean add(E elt) {
        // list is empty, add elt
        if (isEmpty()) {
            super.add(elt);
            return true;
        }
        // find the position where to insert elt
        int ind =0;
        while (ind < size() && this.get(ind).compareTo(elt)<0) {
            ind++;
        }
        // elt has not been found, add it
        if (ind == size()) {
            this.add(ind,elt);
            return true;
        }
        // elt must be in position ind, add it only if it is not yet in the list
        if (this.get(ind).compareTo(elt)!=0)
            this.add(ind,elt);
        return true;
    }

    @Override
    public boolean contains(Object elt) {
        int ind =0;
        while (ind < size()) {
            if (this.get(ind).compareTo(elt)==0)
                return true;
            if (this.get(ind).compareTo(elt)<0)
                return false;
            ind++;
        }
    return false;
    }

    public boolean isEqual(OrderedArrayList<E> that) {
        if (that==null)
            return false;
        if (this==that)
            return true;
        if (this.size()!=that.size())
            return false;
        for (int i=0;i<size();i++) {
            if (get(i).compareTo(that.get(i))!=0)
                return false;
        }
        return true;
    }
}
