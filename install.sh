#!/bin/bash

# install make if necessary
# sudo apt install make

# install the jdk if necessary
# sudo apt install openjdk-11-jdk-headless

# script to unzip the lib files and to install TotemBioNet

cd CKS-BioNet/lib

echo "unzip yices-2.6.zip"
unzip yices-2.6.zip

echo "unzip NuSMV.zip"
unzip NuSMV.zip

echo "unzip antlr4.zip"
unzip antlr4.zip                                                                

cd ..

echo "installing CKS-BioNet ..."
make
# sudo make install

cd ..


